package tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.DeclarationContext;
import symbolTable.Entry;
import symbolTable.SymbolTable;

public class DeclarationNode extends Node {

	private static final long serialVersionUID = -877470283158958915L;
	
	String type = "";
	//Lists required here because different options possible per var
	List<Boolean> isPointer = new ArrayList<Boolean>();
	List<String> varName = new ArrayList<String>();
	List<Boolean> isArray = new ArrayList<Boolean>();
	Boolean isConst = false;

	public DeclarationNode(DeclarationContext ctx) {
		super(ctx);
		name = "Declaration";
		idShouldExist = false;
	}
	
	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		//useMaxScope possible if in function definition
		Integer thisMaxScope = maxScope;
		Boolean useMaxScope = useMaxScopeList.remove();
		if (useMaxScope) {
			scopes.push(thisMaxScope+1);
		}

		
		DeclTypeNode typeNode = (DeclTypeNode) this.leftmostChild;
		//set some members
		isConst = typeNode.isConst;
		type = typeNode.type;
		name += ": " + type;
		Node node = typeNode.rightSibling;
		while (node != null) {
			//Arbitrary number of children possible
			//Check for each if pointer
			if (node instanceof PtrNode) {
				isPointer.add(true);
				node = node.rightSibling;
			}
			else {
				isPointer.add(false);
			}
			if (node instanceof IDNode) {
				//Get info from the IDNode (and symbol table)
				IDNode idNode = (IDNode) node;
				varName.add(idNode.id);
				Entry entry = symbolTable.getVarEntry(idNode.id, scopes, true);
				if (entry != null) {
					entry.declared = true; //Indicate that var is as of now declared and usable
				}
				if(entry != null && isConst) {
					entry.isConst = true;
				}
				idNode.isDecl = true; //Notify idNode that it's part of a declaration
			}
			else if (node instanceof ArrayDeclNode) {
				//In case of array grab some info too
				ArrayDeclNode arrayNode = (ArrayDeclNode) node;
				varName.add(arrayNode.id);
				Entry entry = symbolTable.getVarEntry(arrayNode.id, scopes, true);
				entry.declared = true; //Indicate that array is as of now declared and usable
			}

			node = node.rightSibling;
		}
		//Indicate if pointers are present in name (for visualisation)
		if (isPointer.contains(true)) {
			name += "\n ptrs: ";
			for (int i = 0; i < isPointer.size(); i++) {
				if (isPointer.get(i)) {
					name+= i + " ";
				}
			}
		}
		return super.setInfo(scopes, maxScope, useMaxScopeList, useMaxScope);
	}
	
	@Override
	public void abstractify(SymbolTable symbolTable) {
		//Grab type from DeclTypeNode and prune it
		this.leftmostChild.prune();
		Node node = this.leftmostChild.rightSibling;
		while (node != null) {
			//prune PtrNodes
			if (node instanceof PtrNode) {
				node.prune();
				node = node.rightSibling;
			}
			node = node.rightSibling;
		}
		super.abstractify(symbolTable);
	}


}
