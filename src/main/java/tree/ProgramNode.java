package tree;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import smallC.smallCParser.ProgContext;

public class ProgramNode extends Node {

	private static final long serialVersionUID = 3864643709560769693L;

	public ProgramNode(ProgContext ctx) {
		super(ctx);
		name = "program";
	}
	
	@Override
	public int dotTree(int id, int parentId, String filename) throws IOException {
		//Only called for root node, takes case of some GraphViz stuff and clears the file initially
		PrintWriter writer = new PrintWriter(filename);
		writer.println("Graph AST {");
		writer.close();
		
		super.dotTree(id, parentId, filename);
		
		PrintWriter writer2 = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
		writer2.write("}");
		writer2.close();
		
		return 0;
	}
}
