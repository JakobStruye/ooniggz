package tree;

import java.io.PrintWriter;

import smallC.smallCParser.ElseStatContext;

public class ElseStatNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8691238458349015589L;

	public ElseStatNode(ElseStatContext ctx) {
		super(ctx);
		name = "else";
	}

	@Override
	public void visitNode(PrintWriter writer) {
		//Add labels
		if (leftmostChild == null) {
			writer.println("label" + ((IfStatNode)parent).elseLabel + ":");
			writer.println("label" + ((IfStatNode)parent).ifLabel + ":");
		}
		super.visitNode(writer);
	}
	

}
