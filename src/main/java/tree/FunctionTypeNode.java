package tree;

import smallC.smallCParser.FunctionTypeContext;

public class FunctionTypeNode extends Node {

	private static final long serialVersionUID = -7415653795590606682L;
	
	String fctTypeName;

	public FunctionTypeNode(FunctionTypeContext ctx) {
		super(ctx);
		name = "function type";
		fctTypeName = ctx.getText();
		name += ": " + fctTypeName;
	}
}
