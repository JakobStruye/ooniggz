package tree;

import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.CompoundContext;
import symbolTable.SymbolTable;

public class CompoundNode extends Node {


	private static final long serialVersionUID = -2998344356321524335L;

	public CompoundNode(CompoundContext ctx) {
		super(ctx);
		name = "compound";
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		//Set the scopes (a scope gets added here!)
		maxScope = maxScope + 1;
		scopes.push(maxScope);
		Integer retVal = super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);

		scopes.pop(); //Remove after setting for children
		return retVal;
	}
	

	@Override
	public void abstractify(SymbolTable symbolTable) {
		//if zero or one stat in compound, remove compound node
		//if there are more, it's kept to keep AST visualisation clear
		if (this.leftmostChild == null || this.leftmostChild.rightSibling == null) {
			this.prune();
		}
		super.abstractify(symbolTable);
	}

}
