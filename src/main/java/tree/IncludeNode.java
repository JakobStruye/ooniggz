package tree;

import smallC.smallCParser.IncludeContext;

public class IncludeNode extends Node {
	
	private static final long serialVersionUID = -7108535041746768894L;

	public IncludeNode(IncludeContext ctx) {
		super(ctx);
		name = "include \n <stdio.h>";
	}
	//Presence is checked in grammar
}
