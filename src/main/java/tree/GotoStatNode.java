package tree;

import java.io.PrintWriter;

import smallC.smallCParser.GotoStatContext;

public class GotoStatNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4471751024881035881L;
	
	String label;

	public GotoStatNode(GotoStatContext ctx) {
		super(ctx);
		label = ctx.ID().getText();
	}

	@Override
	public void visitNode(PrintWriter writer) {
		//unconditional jump to the label
		writer.println("ujp " + label);
		visitRightSiblings(writer);
	}
	
	

}
