package tree;

import java.io.PrintWriter;

import smallC.smallCParser.JumpLabelContext;

public class JumpLabelNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1606247302832670074L;
	
	String label;

	public JumpLabelNode(JumpLabelContext ctx) {
		super(ctx);
		label = ctx.LABEL().getText();
		//Warn user for possible label collision
		if (label.startsWith("label")) {
			System.out.println("label starting with 'label' detected. Please avoid these as they are used internally.");
		}
		if (label.startsWith("func_")) {
			System.out.println("label starting with 'func_' detected. Please avoid these as they are used internally.");
		}
	}

	@Override
	public void visitNode(PrintWriter writer) {
		writer.println(label); //print the label (already has :)
		super.visitRightSiblings(writer);
	}
	
	

}
