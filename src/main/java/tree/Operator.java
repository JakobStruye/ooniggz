package tree;

public enum Operator {
	MULT, DIV, MOD, PLUS, MINUS, GT, LT, GEQ, LEQ, EQ, NEQ, AND, OR
}
