package tree;

import smallC.smallCParser.TypeContext;

public class TypeNode extends Node {

	private static final long serialVersionUID = -4001087552620724731L;
	
	String type;
	Boolean isConst = false;

	public TypeNode(TypeContext ctx) {
		super(ctx);
		name = "type";
		if (ctx.CONST() != null) {
			isConst = true;
			type = ctx.getText().substring(5);
		}
		else {
			type = ctx.getText();
		}
	}

	
	@Override
	void prune() {
		if (this.leftmostChild != null) {
			this.leftmostChild.prune(); //also prune TypedefTypeNode
		}
		prune = true;
	}

}
