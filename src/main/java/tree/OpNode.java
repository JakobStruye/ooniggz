package tree;

import org.antlr.v4.runtime.ParserRuleContext;

public class OpNode extends Node {

	private static final long serialVersionUID = 7317326348572899989L;
	
	public Operator op;

	public OpNode(ParserRuleContext ctx) {
		super(ctx);
		name = ctx.getText();
		//Generate the right operator
		switch (ctx.getText()) {
		case ("+"):
			op = Operator.PLUS;
			break;
		case ("-"):
			op = Operator.MINUS;
			break;
		case ("*"):
			op = Operator.MULT;
			break;
		case ("/"):
			op = Operator.DIV;
			break;
		case ("%"):
			op = Operator.MOD;
			break;
		case (">"):
			op = Operator.GT;
			break;
		case ("<"):
			op = Operator.LT;
			break;
		case (">="):
			op = Operator.GEQ;
			break;
		case ("<="):
			op = Operator.LEQ;
			break;
		case ("=="):
			op = Operator.EQ;
			break;
		case ("!="):
			op = Operator.NEQ;
			break;
		case ("&&"):
			op = Operator.AND;
			break;
		case ("||"):
			op = Operator.OR;
			break;
		default:
			System.out.println("Unrecognized operator!"); //shouldn't happen
			
		}
	}

}
