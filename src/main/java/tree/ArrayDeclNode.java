package tree;

import java.io.PrintWriter;

import smallC.smallCParser.ArrayDeclContext;

public class ArrayDeclNode extends Node {
	
	private static final long serialVersionUID = 8503261087288363204L;
	
	String id;
	
	//Not used for code generation, just useful for clear AST

	public ArrayDeclNode(ArrayDeclContext ctx) {
		super(ctx);
		name = "array declaration";
		idShouldExist = false;
		id = ctx.id().getText();
		

	}


	@Override
	public
	void visitNode(PrintWriter writer) {
		visitRightSiblings(writer); //Don't visit children as they shouldn't generate code

	}

}
