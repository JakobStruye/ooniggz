package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.ArithContext;
import symbolTable.SymbolTable;

public class ArithNode extends Node {

	private static final long serialVersionUID = -9060240025553962737L;
	
	Type type;
	Boolean isPointer = false;
	Boolean isConst = false;
	
	Operator op;
	
	Boolean makeBool = false; //True if the result should be converted to a bool
	
	public Boolean isNegated = false; //True if the result should be negated (! operator)
			
	Integer knownValue = null; //will be set if it's a literal ("magic") number
	
	public ArithNode(ArithContext ctx) {
		super(ctx);
		name = "arith";		
	}
	
	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		Integer thisMaxScope = maxScope;
		Integer retVal;
		
		//If this is part of a for, pass on the max scope to the siblings
		if (parent instanceof ForStatNode) {
			scopes.push(thisMaxScope+1);
			retVal = super.setInfo(scopes, maxScope, useMaxScopeList, true);

		}
		else {
			retVal = super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		}

		
		if(this.leftmostChild instanceof ArithNode && this.leftmostChild.rightSibling == null) {
			// NEGATE arith (only child is arith -> only option is it was negated
			((ArithNode)this.leftmostChild).isNegated = true;

			//Fill in some members
			type = ((ArithNode)(this.leftmostChild)).type;
			isPointer = ((ArithNode)(this.leftmostChild)).isPointer;
			isConst = ((ArithNode)(this.leftmostChild)).isConst;
		}
		else if (this.leftmostChild instanceof AtomNode) {
			//AtomNode, grab members from it
			type = ((AtomNode)(this.leftmostChild)).type;
			isPointer = ((AtomNode)(this.leftmostChild)).isPointer;
			isConst = ((AtomNode)(this.leftmostChild)).isConst;
			
			//If the AtomNode is constant integer or character, save it here (for optimization)
			if (((AtomNode)(this.leftmostChild)).isInt) {
				knownValue = ((AtomNode)this.leftmostChild).intgr;
			}
			else if (((AtomNode)(this.leftmostChild)).isChar) {
				knownValue = (int) ((AtomNode)this.leftmostChild).chr;
			}
		} 
		else {
			//2 ariths as children (and operator)
			type = ((ArithNode)(this.leftmostChild)).type;
			
			//if either of the operands is a pointer, the result is too. This allows for pointer arithmetic
			isPointer = (((ArithNode)(this.leftmostChild)).isPointer || ((ArithNode)(this.leftmostChild.rightSibling.rightSibling)).isPointer);
			
			//Types should be equal
			if (type != ((ArithNode)(this.leftmostChild.rightSibling.rightSibling)).type) {
				throw new SemanticException("Arithmetic operand mismatch!", this.lineNr);
			}
		}
		return retVal;
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		if (this.leftmostChild.rightSibling == null && this.leftmostChild instanceof AtomNode) {
			//just an atom as child, this node is superfluous, pass along info and prune this
			if(makeBool) {
				((AtomNode) this.leftmostChild).makeBool = true;
			}
			
			((AtomNode) this.leftmostChild).isNegated = isNegated;
			
			if(!(parent instanceof IfStatNode || parent instanceof WhileStatNode || parent instanceof ForStatNode)) {
				// only prune if its parent is not an if/while/stat node
				prune();
			}
		}
		else if (this.leftmostChild.rightSibling == null) {
			//just an arith as child, this node is superfluous. isNegated already passed along 
			if(makeBool) {
				((ArithNode) this.leftmostChild).makeBool = true;
			}
			if(!(parent instanceof IfStatNode || parent instanceof WhileStatNode || parent instanceof ForStatNode)) {
				// only prune if its parent is not an if/while/stat node
				prune();
			}
			else {
				//Also store isNegated here if not pruned
				isNegated = true;
			}
		}
		else {
			//Grab the operation type from the OpNode and prune it
			OpNode opNode = (OpNode) this.leftmostChild.rightSibling;
			op = opNode.op;
			name = opNode.name;
			opNode.prune();
			
			//Check for hardcoded division by zero
			if (name.equals("/")) {
				//Before actual pruning, right operand still arith, so check if one child and if that is zero
				if (opNode.rightSibling.leftmostChild.rightSibling == null && opNode.rightSibling.leftmostChild.name.equals("atom: 0")) {
					throw new SemanticException("Division by zero detected", lineNr);
				}
			}
		}
		
		if(op  == Operator.AND || op == Operator.OR) {
			((ArithNode) this.leftmostChild).makeBool = true;
			// right operand will be cast to bool below 
		}
		
		super.abstractify(symbolTable);
		
		if (this.leftmostChild.rightSibling != null && this.leftmostChild instanceof ArithNode) { //right must also be arith
			
			Integer left = ((ArithNode)this.leftmostChild).knownValue;
			Integer right = ((ArithNode)this.leftmostChild.rightSibling.rightSibling).knownValue;
			
			//Optimize case where both operands are known at compile time (==are constants)
			if (left != null && right != null) {
				this.leftmostChild = null; //Prune all children by cutting only connection to them

				//Calculate operation result at compile time
				switch(op) {
				case EQ:
					if (left.equals(right)) {
						knownValue = 1;
					}
					else knownValue = 0;
					break;
				case NEQ:
					if (left.equals(right)) {
						knownValue = 0;
					}
					else knownValue = 1;
					break;
				case GT:
					if (left.intValue() > right.intValue()) {
						knownValue = 1;
					}
					else {
						knownValue = 0;
					}
					break;
				case LT:
					if (left.intValue() < right.intValue()) {
						knownValue = 1;
					}
					else {
						knownValue = 0;
					}
					break;
				case GEQ:
					if (left.intValue() >= right.intValue()) {
						knownValue = 1;
					}
					else {
						knownValue = 0;
					}
					break;
				case LEQ:
					if (left.intValue() <= right.intValue()) {
						knownValue = 1;
					}
					else {
						knownValue = 0;
					}
					break;
				case AND:
					if (left.intValue() != 0 && right.intValue() != 0) {
						knownValue = 1;
					}
					else {
						knownValue = 0;
					}
					break;
				case OR:
					if (left.intValue() != 0 || right.intValue() != 0) {
						knownValue = 1;
					}
					else {
						knownValue = 0;
					}
					break;
				case PLUS:
					knownValue = left + right;
					break;
				case MINUS:
					knownValue = left - right;
					break;
				case MULT:
					knownValue = left * right;
					break;
				case DIV:
					knownValue = left / right;
					break;
				default:
					System.out.println("unknown operation!");
				}
			}
		}
	}
	
	@Override
	public
	void visitNode(PrintWriter writer) {

		//Check if condition of while/for and place labels accordingly
		if(parent instanceof WhileStatNode) {
			writer.println("label" + ((WhileStatNode)parent).loopLabel + ":");
		}
		if(parent instanceof ForStatNode && (rightSibling instanceof ScopedBodyNode)) {
			writer.println("label" + ((ForStatNode)parent).loopLabel + ":");
		}
		
		//Even if visitRight is false, this node's children should visit their right siblings, so temporarily set to true
		if (!visitRight) {
			visitRight = true;
			visitChildren(writer);
			visitRight = false;
		} 
		else {
			visitChildren(writer);
		}
		
		//Value determined at compile time, so load as constant
		//Note that children have been pruned if this is not null
		if (knownValue != null) {
			writer.println("ldc i " + knownValue.intValue());
		}
		
		else {
			//If there's an operator, generate code for it
			//Children will take care of operand code generation
			if(this.leftmostChild.rightSibling != null) {
				switch(op) {
				case EQ:
					writer.println("equ i");
					writer.println("conv b i");
					break;
				case NEQ:
					writer.println("neq i");
					writer.println("conv b i");
					break;
				case GT:
					writer.println("grt i");
					writer.println("conv b i");
					break;
				case LT:
					writer.println("les i");
					writer.println("conv b i");
					break;
				case GEQ:
					writer.println("geq i");
					writer.println("conv b i");
					break;
				case LEQ:
					writer.println("leq i");
					writer.println("conv b i");
					break;
				//if and/or, first convert right operand to boolean
				//left operand will have taken care of conversion by itself
				case AND:
					writer.println("conv i b");
					writer.println("and");
					writer.println("conv b i");
					break;
				case OR:
					writer.println("conv i b");
					writer.println("or");
					writer.println("conv b i");
					break;
				case PLUS:
					writer.println("add i");
					break;
				case MINUS:
					writer.println("sub i");
					break;
				case MULT:
					writer.println("mul i");
					break;
				case DIV:
					writer.println("div i");
					break;
				default:
					System.out.println("unknown operation!");
				}
			}
		}
		
		//Check if parent asked for boolean result and generate if so
		if(makeBool) {
			writer.println("conv i b");
			if(isNegated) { //negate result if required
				writer.println("not");
			}
		}
		
		//Check if while/if/for and place labels/jumps accordinly
		if(parent instanceof WhileStatNode) {
			writer.println("conv i b");
			if(isNegated) {
				writer.println("not");
			}
			writer.println("fjp label" + ((WhileStatNode)parent).skipLabel);
		}
		if(parent instanceof IfStatNode) {
			writer.println("conv i b");
			if(isNegated) {
				writer.println("not");
			}
			writer.println("fjp label" + ((IfStatNode)parent).elseLabel);
		}
		//Distinguish between middle and right part of for(...;...;...)
		if(parent instanceof ForStatNode && (rightSibling instanceof ScopedBodyNode)) {
			writer.println("conv i b");
			if(isNegated) {
				writer.println("not");
			}
			writer.println("fjp label" + ((ForStatNode)parent).skipLabel);
		}
		
		if(parent instanceof ForStatNode && rightSibling == null) {
			writer.println("ujp label" + ((ForStatNode)parent).loopLabel);
			writer.println("label" + ((ForStatNode)parent).skipLabel + ":");
		}
		
		//Don't visitRight if parent told not to
		if (visitRight) {
			visitRightSiblings(writer);
		}
	}

}
