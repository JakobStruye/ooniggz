package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.CaseSwitchContext;

public class CaseSwitchNode extends Node {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3283795096641299784L;
	
	// Info about this case
	public Type type;
	public Integer intgr;
	public char chr;
	public Boolean isDefault = false;
	public Integer endLabel; //labels to use for jumps
	public Integer label;
	
	public CaseSwitchNode(CaseSwitchContext ctx) {
		super(ctx);
		if(ctx.DEFAULTCASE() != null) {
			isDefault = true;
			name = "default case";
		}
		else {
			name = "case";
		}
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {

		//Grab the endLabel from the parent
		endLabel = ((SwitchStatNode)parent).endLabel;
		
		Integer retVal =  super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		
		if(!isDefault) {
			// not default case so leftmostchild is atomNode.
			type = ((AtomNode)leftmostChild).type;

			if(type == Type.CHAR) {
				chr = ((AtomNode)leftmostChild).chr;
			}
			else if(type == Type.INT) {
				intgr = ((AtomNode)leftmostChild).intgr;
			}
		}
		
		return retVal;
	}

	@Override
	public void visitNode(PrintWriter writer) {
		// The visitNode method will generate the case test and jumps
		if(!isDefault) {
			// regular case
			label = maxLabel;
			writer.println("switchTest" + maxLabel + ":");
			writer.println("ldo i " + ((SwitchStatNode)parent).location); // Load the main switch value
			if(type == Type.INT) {
				writer.println("ldc i " + intgr);
			}
			else if (type == Type.CHAR) {
				//convert to int first
				writer.println("ldc i " + (int) chr);
			}
			writer.println("equ i");
			writer.println("fjp switchTest" + (maxLabel+1));
			writer.println("ujp switchCode" + maxLabel);
			maxLabel++;
	
			if (rightSibling != null) {
				rightSibling.visitNode(writer);
			}
			
		}
		else {
			// default case
			writer.println("switchTest" + maxLabel + ":");
			label = maxLabel;
			writer.println("ujp switchCode" + maxLabel); //always jump if default is reached
			maxLabel++;
		}
	}
	
	public void generateSwitchCode(PrintWriter writer) {
		// The generateSwitchCode will generate the actual code that's within the case.
		
		writer.println("switchCode" + label + ":");
		// Do not visit the IDNode
		if (!isDefault && leftmostChild.rightSibling != null) {
			leftmostChild.rightSibling.visitNode(writer);
		}
		else {
			// default case
			// There is no IDNode so visit the leftmostChild
			leftmostChild.visitNode(writer);
		}
	}

	
	
}
