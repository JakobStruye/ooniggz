package tree;

import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.ForStatContext;

public class ForStatNode extends Node {

	private static final long serialVersionUID = 4415139975461985531L;
	
	Integer loopLabel = -1;
	Integer skipLabel = -1;

	public ForStatNode(ForStatContext ctx) {
		super(ctx);
		name = "for";
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		//Claim label numbers
		Node.maxLabel++;
		skipLabel = maxLabel;
		Node.maxLabel++;
		loopLabel = maxLabel;
		
		Integer retVal =  super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		
		//Rearrange children to order they should be evaluated in
		this.leftmostChild.rightSibling.rightSibling.rightSibling.rightSibling = this.leftmostChild.rightSibling.rightSibling;
		this.leftmostChild.rightSibling.rightSibling = this.leftmostChild.rightSibling.rightSibling.rightSibling;
		this.leftmostChild.rightSibling.rightSibling.rightSibling.rightSibling = null;
		
		return retVal;
	}
}
