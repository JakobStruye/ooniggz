package tree;

import org.antlr.v4.runtime.ParserRuleContext;

import symbolTable.SymbolTable;

public class AmpersandNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6930038782159487262L;

	//This node only exists to be seen by other nodes, doesn't do anything by itself.
	public AmpersandNode(ParserRuleContext ctx) {
		super(ctx);
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		//Always prune this
		prune();
		super.abstractify(symbolTable);
	}
	
	

}
