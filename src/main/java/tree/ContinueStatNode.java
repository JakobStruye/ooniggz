package tree;

import java.io.PrintWriter;

import smallC.SemanticException;
import smallC.smallCParser.ContinueStatContext;

public class ContinueStatNode extends Node {

	private static final long serialVersionUID = 5765745988412109066L;

	public ContinueStatNode(ContinueStatContext ctx) {
		super(ctx);
		name = "continue";
	}

	@Override
	public
	void visitNode(PrintWriter writer) {
		//Find if it's in a for or while
		Node ancestor = parent;
		while (!(ancestor instanceof ForStatNode) && !(ancestor instanceof WhileStatNode)) {
			ancestor = ancestor.parent;
			if (ancestor == null) {
				throw new SemanticException("Continue used outside of for or while loop", lineNr);
			}
		}
		//Set the appriopriate jump
		if (ancestor instanceof ForStatNode) {
			writer.println("ujp label" + ((ForStatNode)ancestor).loopLabel);
		}
		else if (ancestor instanceof WhileStatNode) {
			writer.println("ujp label" + ((WhileStatNode)ancestor).loopLabel);
		}
		
		super.visitRightSiblings(writer); //no children to visit
	}

}
