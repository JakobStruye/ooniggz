package tree;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.ArrayElemContext;
import symbolTable.Entry;

public class ArrayElemNode extends Node {
	
	private static final long serialVersionUID = -8386429541619795433L;
	Type type;
	Integer location;
	String id;
	
	Boolean isArith = false; //True if parent is atom -> this node is descendant of arith
	
	List<Integer> dimensions = new ArrayList<Integer>(); //Maintain dimensions of array for row-major index calculation

	public ArrayElemNode(ArrayElemContext ctx) {
		super(ctx);
		name = "array element";
		id = ctx.id().getText();
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		//Grab entry from symboltable
		Entry entry = symbolTable.getVarEntry(id, scopes);
		
		//Do nothing if no entry found, IDNode will take care of error
		if (entry == null) {
			return super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		}
		
		//Grab info from symboltable
		location = entry.location;
		String attr = entry.attributes;
		
		//Grab typeinfo
		if (attr.substring(0, 3).equals("int")) {
			type = Type.INT;
		}
		else if (attr.substring(0, 4).equals("char")) {
			type = Type.CHAR;
		}
		
		//Check if part of arithmetic expression
		if (parent instanceof AtomNode && (((AtomNode) parent).isAddress == false)) {
			isArith = true;
		}
		
		//Check if variable is actually an array
		if (entry.isArray == false) {
			throw new SemanticException("Using non-array variable as array", lineNr);
		}
		
		//Count dimensions of access
		Node child = leftmostChild.rightSibling;
		int dimCount = 0;
		while (child != null) {
			dimCount++;
			child = child.rightSibling;
		}
		if (dimCount > 1) {
			//Copy dimensions if multidimensional (not needed for 1-dimensional as index calculation is trivial then)
			dimensions = entry.dimensions;
		}
		//Check if dimensions are correct
		if (!(dimCount == entry.dimensions.size())) {
			throw new SemanticException("Trying to access array with wrong dimensions", lineNr);
		}
		return super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);	
	}
	

	@Override
	public
	void visitNode(PrintWriter writer) {
		//Load the address of the first element, then add offset to that
		writer.println("ldc a "+ ((IDNode)leftmostChild).location);
		if (dimensions.size() <= 1) {
			//Just let arithnode generate offset code if 1-dim
			leftmostChild.rightSibling.visitNode(writer);
		}
		else {
			//Multidimensional, calculate index with http://en.wikipedia.org/wiki/Row-major_order#Address_calculation_in_general
			Boolean prevVisitRight = visitRight; //save this so it can be set again
			visitRight = false;
			Node node = leftmostChild.rightSibling;
			node.visitNode(writer);
			for (int i = 1; i < dimensions.size(); i++) {
				node = node.rightSibling;
				writer.println("ldc i " + dimensions.get(i));
				writer.println("mul i");
				node.visitNode(writer); //visitRight = false so won't move on to next index
				writer.println("add i");
				
			}
			visitRight = prevVisitRight;
		}
		writer.println("ixa 1");
		
		if (isArith && visitRight) { //if visitRight is false, this is a function argument, functioncallnode will indirect load
			writer.println("ind i");
		}
		
		//Don't visitRight if parent told not to
		if (visitRight) {
			super.visitRightSiblings(writer);
		}

	}

}
