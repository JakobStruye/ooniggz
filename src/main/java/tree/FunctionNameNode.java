package tree;

import smallC.smallCParser.FunctionNameContext;

public class FunctionNameNode extends Node {

	private static final long serialVersionUID = -3649592489337532431L;
	
	String fctName;

	public FunctionNameNode(FunctionNameContext ctx) {
		super(ctx);
		name = "function name";
		fctName = ctx.getText();
		name += ": " + fctName;
	}
}
