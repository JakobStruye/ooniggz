package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.AtomContext;
import symbolTable.SymbolTable;

public class AtomNode extends Node {
	
	private static final long serialVersionUID = 456525248918563660L;
	
	//Store type of atom and its value
	Boolean isInt = false;
	Integer intgr;
	Boolean isChar = false;
	Character chr;
	Boolean isID = false;
	String id;
	Boolean isFctCall = false;
	Boolean isArray = false;
	
	//Some more straightforward information
	Type type;
	Boolean isPointer = false;
	Boolean isConst = false;
	Boolean isDeref = false;

	//Store if any unary operations should be performed
	Boolean isPrecrement = false;
	Boolean isPostcrement = false;
	Boolean isIncrement = false; //true is ++, false is --, unused if above two are false
	Boolean isInverted = false;
	Boolean isNegated = false; //True if !
	
	Boolean isAddress = false; //True if &
	
	Boolean hasArith = false; //True if there's still an ArithNode as child
		
	Boolean makeBool = false; //True if parent indicated result should be boolean
	
	Integer location = -1; //Location in store (if variable)
	Integer size = 1; //Number of store positions this consumes (if variable)
	
	public AtomNode(AtomContext ctx) {
		super(ctx);
		name = "atom: ";
		//Check for precrement and invert
		if (ctx.precrement() != null) {
			isPrecrement = true;
			if (ctx.precrement().INCREMENT() != null) {
				isIncrement = true; //else is decrement
				name += "++";
			}
			else {
				name += "--";
			}
		}
		//postcrement is below
		else if (ctx.invert() != null) {
			isInverted = true;
			name += "-";
		}
		
		//Fetch value
		if (ctx.INT() != null) {
			isInt = true;
			type = Type.INT;
			intgr = Integer.parseInt(ctx.INT().getText());
			name += intgr;
		}
		else if (ctx.CHAR() != null) {
			isChar = true;
			type = Type.CHAR;
			chr = ctx.CHAR().getText().charAt(1);
			name += chr;
		}
		else if (ctx.id() != null) {
			isID = true;
			id = ctx.id().getText();
			name += id;
		}
		else if (ctx.functionCall() != null) {
			isFctCall = true;
		}
		else if (ctx.arrayElem() != null) {
			isArray = true;
		}
		
		//Check if pointer/address
		if(ctx.ptr() != null) {
			isDeref = true;
		}
		
		if(ctx.AMPERSAND() != null) {
			isAddress = true;
		}
		
		//Check if child is arith
		if(ctx.arith() != null) {
			hasArith = true;
		}
		
		//Check for postcrement
		if (ctx.postcrement() != null) {
			isPostcrement = true;
			if (ctx.postcrement().INCREMENT() != null) {
				isIncrement = true; //else is decrement
				name += "++";
			}
			else {
				name += "--";
			}
		}
	}
	
	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {

		Integer retVal =  super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		
		if (isID) {
			//It's a variable
			Node child = leftmostChild;
			//Find the IDNode
			while (true) {
				if (child instanceof IDNode) {
					break;
				}
				child = child.rightSibling;
			}
			//Grab info from it
			type = ((IDNode)child).type;
			isPointer = ((IDNode)child).isPointer;
			isConst = ((IDNode)child).isConst;
		}
		
		//Grab info in case of function call 
		if (isFctCall) {
			type = ((FunctionCallNode)leftmostChild).type;
			isPointer = ((FunctionCallNode)leftmostChild).isPointer;
			isConst = ((FunctionCallNode)leftmostChild).isConst;
		}
		
		//if arith child (always 2nd child) is pointer, this is too
		if (hasArith) {
			isPointer = ((ArithNode)leftmostChild.rightSibling).isPointer;
			
		}
		
		//Grab info in case of array
		if (isArray) {
			type = ((ArrayElemNode)leftmostChild).type;
		}
		
		//Check for non-pointer dereferencing
		if(isDeref && !isPointer) {
			throw new SemanticException("Trying to dereference non-pointer", lineNr);
		}
				
		return retVal;
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		if(hasArith) {
			// skip the rest, airth will take care of stuff
		}
		else if ((isFctCall || isArray) && !isAddress) {
			//This node is superfluous if only child is a function call or array and no dereferencing has to occur
			prune();
		}
		else {
			//Else all information of children stored in this node, prune all children
			Node child = leftmostChild;
			while (child != null) {
				if(isID && (child instanceof IDNode)) {
					location = ((IDNode)child).location;
				}
				
				child.prune();
				child = child.rightSibling;
			}
		}
		
		//assigningID contains the id currently being assigned if any. Check for self-assignment
		if (assigningID.equals(id)) {
			throw new SemanticException("Trying to self-assign " + id, lineNr);
		}
		super.abstractify(symbolTable);
	}
	
	@Override
	public
	void visitNode(PrintWriter writer) {
		if(isAddress && isID) {
			// Use address as integer to allow arithmetic 
			writer.println("ldc i " + location);
		}
		else {
			if(isInt) {
				// is constant
				writer.println("ldc i " + intgr);
			}
			else if(isChar) {
				// is constant
				writer.println("ldc i " + (int) chr);
			}
			else if(isID) {
				//FunctionCallNode will take care of loading, don't do it in that case
				if(!(parent instanceof FunctionCallNode)) {
					writer.println("ldo i " + location);
				}
			}
			
			//convert to bool is asked by parent
			if(makeBool) {
				writer.println("conv i b");
			}
			
			visitChildren(writer); //Visit children first so that their code is generated
			
			//invert if required
			if (isInverted) {
				writer.println("neg i");
			}

			//value++: increment, store if variable, then undo 
			if (isPostcrement && isIncrement) {
				writer.println("inc i 1");
				if (isID) {
					writer.println("sro i " + location);
					writer.println("ldo i " + location);
				}
				writer.println("dec i 1");
			}
			//value--: decrement, store if variable, then undo
			else if (isPostcrement && !isIncrement) {
				writer.println("dec i 1");
				if (isID) {
					writer.println("sro i " + location);
					writer.println("ldo i " + location);
				}
				writer.println("inc i 1");
			}
			//++value: increment, store if variable, don't undo
			if (isPrecrement && isIncrement) {
				writer.println("inc i 1");
				if (isID) {
					writer.println("sro i " + location);
					writer.println("ldo i " + location);
				}
			}
			//--value: decrement, store if variable, don't undo
			else if (isPrecrement && !isIncrement) {
				writer.println("dec i 1");
				if (isID) {
					writer.println("sro i " + location);
					writer.println("ldo i " + location);
				}
			}
			
			//if dereference, convert to address and indirect load from it
			if(isDeref) {
				writer.println("conv i a");
				writer.println("ind i");
			}
		}
		
		//Set label/jump if part of for(...;...;...)
		if(parent instanceof ForStatNode && rightSibling == null) {
			writer.println("ujp label" + ((ForStatNode)parent).loopLabel);
			writer.println("label" + ((ForStatNode)parent).skipLabel + ":");
		}
		
		//VisitRight only if allowed by parent
		if(visitRight) {
			visitRightSiblings(writer);
		}
	}
	
	//returns the size if printed
	public Integer getLength() {
		if(isInt) {
			return String.valueOf(intgr).length();
		}
		else if(isChar) {
			return 1;
		}
		else if(isID) {
			return 0;
		}
		return 0;
	}

}
