package tree;

import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.IdContext;
import symbolTable.Entry;
import symbolTable.SymbolTable;

public class IDNode extends Node {


	private static final long serialVersionUID = -6515169330572768940L;
	
	//Bunch of info on variable
	String id;
	Boolean exists;
	Stack<Integer> scopesID;
	public Type type;
	public Boolean isPointer = false;
	public Boolean isConst = false;
	Integer location = -1;
	public Boolean isDecl = false; 
	public Boolean isArray = true;
	public Type arrayType;
	public Boolean allowArrayAsVariable = false; //true if in printf/scanf
	
	

	public IDNode(IdContext ctx) {
		super(ctx);
		name = "id";
		id = ctx.getText();
		name += ": " + id;
	}
	
	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		
		//Grab the entry from the symboltable
		Entry entry = symbolTable.getVarEntry(id, scopes, isDecl); //if isDecl is true, undeclared entries will also be returned
		if (entry == null) {
			throw new SemanticException("Trying to use undeclared variable " + id, lineNr);
		}
		
		//Set some location and type info
		location = entry.location;
		
		String attr = entry.attributes;
		if (attr.endsWith("*")) {
			isPointer = true;
		}
		if (attr.startsWith("const ")) {
			isConst = true;
			attr = attr.substring(6);
		}
		if (attr.substring(0, 3).equals("int")) {
			type = Type.INT;
		}
		else if (attr.substring(0, 4).equals("char")) {
			type = Type.CHAR;
		}
		
		isArray = entry.isArray;
		
		//Check if trying to use array without index where one is needed
		if (!((parent instanceof ArrayElemNode) || (parent instanceof ArrayDeclNode)) && entry.isArray 
				&& !(parent instanceof ScanfCallNode) && !allowArrayAsVariable) {
			throw new SemanticException("Using array variable as regular variable", lineNr); 
		}
		return super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		//Check if the used variable exists in any of the current scopes
		scopesID = symbolTable.getVarScopeList(id);
		exists = false;
		for (Integer scope : scopesID) {
			if (scopes.contains(scope)) {
				exists = true;
				break;
			}
		}
		if (parent.idShouldExist && !exists) {
			throw new SemanticException("Trying to use undeclared variable" + id, lineNr);
		}
		super.abstractify(symbolTable);
	}
	
}