package tree;

import smallC.smallCParser.CompoundNoScopeContext;
import symbolTable.SymbolTable;

public class CompoundNoScopeNode extends Node {


	private static final long serialVersionUID = -2998344356321524335L;

	public CompoundNoScopeNode(CompoundNoScopeContext ctx) {
		super(ctx);
		name = "CompoundNoScope";

	}
	//no new scope so no explicit setInfo

	@Override
	public void abstractify(SymbolTable symbolTable) {
		//if zero or one stat in CompoundNoScope, remove CompoundNoScope node
		//if there are more, it's kept to keep AST visualisation clear
		if (this.leftmostChild == null || this.leftmostChild.rightSibling == null) {
			this.prune();
		}
		super.abstractify(symbolTable);
	}

}
