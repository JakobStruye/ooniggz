package tree;

import org.antlr.v4.runtime.ParserRuleContext;

import smallC.smallCParser.*;



public class NodeGenerator {

	/**
	 * Generate correct Node depending on specific type of ctx
	 */
	public static Node generateNode(ParserRuleContext ctx) {
		if (ctx instanceof AmpersandContext) {
			return new AmpersandNode((AmpersandContext) ctx);
		}
		if (ctx instanceof ArgTypeContext) {
			return new ArgTypeNode((ArgTypeContext) ctx);
		}
		if (ctx instanceof ArithContext) {
			if (((ArithContext)ctx).LPAREN() != null) {
				return null;
			}
			return new ArithNode((ArithContext) ctx);
		}
		if (ctx instanceof ArrayDeclContext) {
			return new ArrayDeclNode((ArrayDeclContext) ctx);
		}
		if (ctx instanceof ArrayElemContext) {
			return new ArrayElemNode((ArrayElemContext) ctx);
		}
		if (ctx instanceof AtomContext) {
			return new AtomNode((AtomContext) ctx);
		}
		if (ctx instanceof BreakStatContext) {
			return new BreakStatNode((BreakStatContext) ctx);
		}
		if (ctx instanceof CaseSwitchContext) {
			return new CaseSwitchNode((CaseSwitchContext) ctx);
		}
		if (ctx instanceof CompoundContext) {
			return new CompoundNode((CompoundContext) ctx);
		}
		if (ctx instanceof CompoundNoScopeContext) {
			return new CompoundNoScopeNode((CompoundNoScopeContext) ctx);
		}
		if (ctx instanceof ContinueStatContext) {
			return new ContinueStatNode((ContinueStatContext) ctx);
		}
		if (ctx instanceof DeclarationContext) {
			return new DeclarationNode((DeclarationContext) ctx);
		}
		if (ctx instanceof DeclTypeContext) {
			return new DeclTypeNode((DeclTypeContext) ctx);
		}
		if (ctx instanceof DefinitionContext) {
			return new DefinitionNode((DefinitionContext) ctx);
		}
		if (ctx instanceof ElseStatContext) {
			return new ElseStatNode((ElseStatContext) ctx);
		}
		if (ctx instanceof EmptyContext) {
			return new EmptyNode((EmptyContext) ctx);
		}
		if (ctx instanceof ForStatContext) {
			return new ForStatNode((ForStatContext) ctx);
		}
		if (ctx instanceof FunctionArgumentsContext) {
			return new FunctionArgumentsNode((FunctionArgumentsContext) ctx);
		}
		if (ctx instanceof FunctionCallContext) {
			return new FunctionCallNode((FunctionCallContext) ctx);
		}
		if (ctx instanceof FunctionDeclContext) {
			return new FunctionDeclNode((FunctionDeclContext) ctx);
		}
		if (ctx instanceof FunctionDefContext) {
			return new FunctionDefNode((FunctionDefContext) ctx);
		}
		if (ctx instanceof FunctionNameContext) {
			return new FunctionNameNode((FunctionNameContext) ctx);
		}
		if (ctx instanceof FunctionTypeContext) {
			return new FunctionTypeNode((FunctionTypeContext) ctx);
		}
		if (ctx instanceof GotoStatContext) {
			return new GotoStatNode((GotoStatContext) ctx);
		}
		if (ctx instanceof HighopContext || ctx instanceof LowopContext || ctx instanceof HighrelopContext ||
				ctx instanceof LowrelopContext || ctx instanceof LogicalandorContext) {
				return new OpNode((ParserRuleContext) ctx);
			}
		if (ctx instanceof IdContext) {
			return new IDNode((IdContext) ctx);
		}
		if (ctx instanceof IfStatContext) {
			return new IfStatNode((IfStatContext) ctx);
		}
		if (ctx instanceof IncludeContext) {
			return new IncludeNode((IncludeContext) ctx);
		}
		if (ctx instanceof InvertContext) {
			return new InvertNode((InvertContext) ctx);
		}
		if (ctx instanceof JumpLabelContext) {
			return new JumpLabelNode((JumpLabelContext) ctx);
		}
		if (ctx instanceof PostcrementContext) {
			return new PostcrementNode((PostcrementContext) ctx);
		}
		if (ctx instanceof PrecrementContext) {
			return new PrecrementNode((PrecrementContext) ctx);
		}
		if (ctx instanceof PrintfCallContext) {
			return new PrintfCallNode((PrintfCallContext) ctx);
		}
		if (ctx instanceof ProgContext) {
			return new ProgramNode((ProgContext) ctx);
		}
		if (ctx instanceof PtrContext) {
			return new PtrNode((PtrContext) ctx);
		}
		if (ctx instanceof ReturnStatContext) {
			return new ReturnStatNode((ReturnStatContext) ctx);
		}
		if (ctx instanceof ScanfCallContext) {
			return new ScanfCallNode((ScanfCallContext) ctx);
		}
		if (ctx instanceof ScopedBodyContext) {
			return new ScopedBodyNode((ScopedBodyContext) ctx);
		}
		if (ctx instanceof StatContext) {
			//bypass stat (easier than pruning it afterwards)
			return null;
		}
		if (ctx instanceof StringContext) {
			return new StringNode((StringContext) ctx);
		}
		if (ctx instanceof SwitchStatContext) {
			return new SwitchStatNode((SwitchStatContext) ctx);
		}
		if (ctx instanceof TypeContext) {
			return new TypeNode((TypeContext) ctx);
		}
		if (ctx instanceof TypedefContext) {
			return new TypedefNode((TypedefContext) ctx);
		}
		if (ctx instanceof TypedefTypeContext) {
			return new TypedefTypeNode((TypedefTypeContext) ctx);
		}
		if (ctx instanceof WhileStatContext) {
			return new WhileStatNode((WhileStatContext) ctx);
		}

		
		return null;
	}


}
