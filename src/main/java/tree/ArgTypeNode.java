package tree;

import java.io.PrintWriter;

import smallC.smallCParser.ArgTypeContext;

public class ArgTypeNode extends Node {

	private static final long serialVersionUID = -7415653795590606682L;
	
	String argTypeName;

	public ArgTypeNode(ArgTypeContext ctx) {
		super(ctx);
		name = "Arg type";
		argTypeName = ctx.getText();
		name += ": " + argTypeName;
	}

	@Override
	public
	void visitNode(PrintWriter writer) {
		super.visitNode(writer);

	}

}
