/**
 * 
 */
package tree;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;

import symbolTable.SymbolTable;

public abstract class Node implements java.io.Serializable {
	
	private static final long serialVersionUID = -8724309729432037085L;
	
	//Keeps four pointers to other nodes (possibly null)
	public Node parent = null;
	public Node leftmostSibling = null;
	public Node rightSibling = null;
	public Node leftmostChild = null;
	
	protected Boolean prune = false; //True if to be pruned
	
	public Integer lineNr; //line on which the Node's code appeared
	
	public static Boolean isMain = false; //true while traversing main function
	public static Boolean visitRight = true; //false while no node should visit its right siblings
	
	String name = "undefined"; //Each constructor should set this
	
	Stack<Integer> scopes; //All scopes currently in
	
	Boolean idShouldExist = true; //false if current id should be undeclared
	
	public static SymbolTable symbolTable; //static to easily reach symboltable
	public static Integer maxLabel = 0; //Nextt label value to be used
	
	public Boolean hasReturn = false; //True if this node has a return descendant it will always reach
	public static Boolean noReturnError = false; //True if missing return should generate warning instead of error
	
	public static String assigningID = ""; //Contains the ID currently being assigned
	static public Map<String, Integer> overloadMap = new HashMap<String, Integer>(); //Holds how many of each function name already declared (for overloading)
		
	public Node () { //lineNr to be set manually
		lineNr = 0;
	}
	
	public Node (ParserRuleContext ctx) {
		lineNr = ctx.getStart().getLine(); //set lineNr from context
	}
	
	/**
	 * visitNode just accesses leftmost child then right sibling (if applicable)
	 * Specific node implementations take care of actual code generation
	 * but should always end with calling superfunction to keep going
	 */
	public void visitNode(PrintWriter writer) {
		if (leftmostChild != null) {
			leftmostChild.visitNode(writer);
		}
		if (rightSibling != null) {
			rightSibling.visitNode(writer);
		}
	}
	
	/**
	 * Separate children and rightsibling visits in case code needs to be executed in between
	 */
	public void visitChildren(PrintWriter writer) {
		if (leftmostChild != null) {
			leftmostChild.visitNode(writer);
		}
	}
	
	public void visitRightSiblings(PrintWriter writer) {
		if (rightSibling != null) {
			rightSibling.visitNode(writer);
		}
	}
	/**
	 * SetInfo will set the scopes every node is in, as well as possibly other info, to be determined by the separate nodes.
	 */
	@SuppressWarnings("unchecked")
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope, Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		this.scopes = (Stack<Integer>)scopes.clone();
		if (leftmostChild != null) {
			maxScope = leftmostChild.setInfo(scopes, maxScope, useMaxScopeList, false);
		}
		if (undoUseMaxScope) { //undo if useMaxScope was only meant for children, not for rightSiblings
			scopes.pop();
		}
		if (rightSibling != null) {
			maxScope = rightSibling.setInfo(scopes, maxScope, useMaxScopeList, false);
		}
		return maxScope;
	}
	
	/**
	 * visitNode just accesses leftmost child then right sibling (if applicable)
	 * Specific node implementations take care of actual abstractifying
	 * but should always end with calling superfunction to keep going
	 */
	public void abstractify(SymbolTable symbolTable) {
		if (leftmostChild != null) {
			leftmostChild.abstractify(symbolTable);
		}
		if (rightSibling != null) {
			rightSibling.abstractify(symbolTable);
		}
	}
	
	/**
	 * Generates binary file of Node + descendants
	 * Useful for checking equality between 2 trees
	 */
	public void serialize(String filename) throws IOException {
		 FileOutputStream fileOut = new FileOutputStream(filename + ".bin");
         ObjectOutputStream out = new ObjectOutputStream(fileOut);
         out.writeObject(this);
         out.close();
         fileOut.close();
	}
	
	/**
	 * Generates dot file for visualisation of tree
	 * id and parentId are used internally, just call function with those 0
	 */
	public int dotTree(int id, int parentId, String filename) throws IOException {
		//Append to file
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filename, true)));
		out.println(id +  " [label=\"" + name +"\"];");
		//Draw line between parent and this
		if (parent != null) {
			out.println(parentId + " -- " + id + ";");
		}
		out.close();
		int thisId = id; //Save unique ID for this node
		id++;
		//Visit rest of tree in-order
		if (leftmostChild != null) {
			id = leftmostChild.dotTree(id, thisId, filename);
		}
		if (rightSibling != null) {
			id = rightSibling.dotTree(id, parentId, filename);
		}
		return id;

	}
	
	/**
	 * Will remove any and all nodes with prune set to true
	 * Note that only nodes with 0 or 1 children can be pruned
	 */
	public void pruneTree() {
		if (prune) {
			//if leftmost sibling
			if (leftmostSibling == this) {
				//if 1 child
				if (this.leftmostChild != null) {
					//child will replace this node
					//child becomes leftmost child of this node's parent
					this.parent.leftmostChild = this.leftmostChild;
					//this node's parent becomes parent of child
					this.leftmostChild.parent = this.parent;
					//right sibling of this node becomes right sibling of child
					this.leftmostChild.rightSibling = this.rightSibling;
					Node sibling = this.rightSibling;
					//All siblings to the right get child as leftmostSibling
					while (sibling != null) {
						sibling.leftmostSibling = this.leftmostChild;
						sibling = sibling.rightSibling;
					}
				}
				//if no children
				else {
					//no siblings
					if (this.rightSibling == null) {
						//this node's parent has no more children
						this.parent.leftmostChild = null;
					}
					//Sibling to take place as leftmost
					else {
						Node newLeftmost = this.rightSibling;
						//set right sibling of node as new leftmost child of parent...
						this.parent.leftmostChild = newLeftmost;
						Node sibling = this.rightSibling;
						//..and as new leftmost sibling of possible other right siblings
						while (sibling != null) {
							sibling.leftmostSibling = newLeftmost;
							sibling = sibling.rightSibling;
						}
					}
				}
			}
			//not leftmost sibling
			else {
				//no child
				if (this.leftmostChild == null) {
					Node sibling = this.leftmostSibling;
					//find direct left sibling of this node
					while (sibling.rightSibling != this) {
						sibling = sibling.rightSibling;
					}
					//connect left and right sibling, cutting out this node
					sibling.rightSibling = this.rightSibling;
				}
				//node has a child, child replaces this node
				else {
					//child inherits parent, leftmost sibling, right sibling of this node
					this.leftmostChild.parent = this.parent;
					this.leftmostChild.leftmostSibling = this.leftmostSibling;
					this.leftmostChild.rightSibling = this.rightSibling;
					Node sibling = this.leftmostSibling;
					//Find left sibling of this node
					while (sibling.rightSibling != this) {
						sibling = sibling.rightSibling;
					}
					//Left sibling gets child as new right sibling
					sibling.rightSibling = this.leftmostChild;
				}
			}
		}
		//Continue along the tree in-order
		//note that even if this node was pruned, leftmostChild still needs to be accessed,
		//as it won't be reached any other way
		if (leftmostChild != null) {
			leftmostChild.pruneTree();
		}
		//If this node was pruned and replaced by a child, don't access right sibling
		//child inherited that sibling and will access it
		//accessing it here could cause a double prune of the right sibling
		if (rightSibling != null && (!prune || leftmostChild == null)) {
			rightSibling.pruneTree();
		}
	}
	
	/**
	 * Sets Node to be pruned on next pruneTree()
	 * Specific nodes can override this for custom pruning options (e.g. also pruning children)
	 */
	void prune() {
		prune = true;
	}
}