package tree;

import java.io.PrintWriter;

import smallC.smallCParser.FunctionArgumentsContext;
import symbolTable.SymbolTable;

public class FunctionArgumentsNode extends Node {

	private static final long serialVersionUID = 3918192611742685099L;
	
	public FunctionArgumentsNode(FunctionArgumentsContext ctx) {
		super(ctx);
		name = "FunctionArguments";
	}


	@Override
	public void abstractify(SymbolTable symbolTable) {
		if (leftmostChild == null) { //prune if no arguments
			this.prune();
		}
		super.abstractify(symbolTable);
	}
	
	@Override
	public
	void visitNode(PrintWriter writer) {
		Node child = leftmostChild;
		Integer loc = 5; //should be hardcoded
		//Load all arguments onto stack
		while(child != null) {
			writer.println("lod i 0 " + loc);
			loc++;
			writer.println("sro i " + ((IDNode)child.leftmostChild).location);
			child = child.rightSibling;
		}
		visitRightSiblings(writer);
	}

}
