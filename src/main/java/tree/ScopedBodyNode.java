package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.ScopedBodyContext;

public class ScopedBodyNode extends Node {


	private static final long serialVersionUID = -2998344356321524335L;

	public ScopedBodyNode(ScopedBodyContext ctx) {
		super(ctx);
		name = "scopedBody";

	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		//Add scope
		maxScope = maxScope + 1;

		scopes.push(maxScope);
		Integer retVal = super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		scopes.pop(); //remove after visiting children
		return retVal;
	}
	
	
	@Override
	public
	void visitNode(PrintWriter writer) {
		//Check for conditionals and set jumps/labels accordingly
		if(parent instanceof WhileStatNode) {
			visitChildren(writer);
			writer.println("ujp label" + ((WhileStatNode)parent).loopLabel);
			writer.println("label" + ((WhileStatNode)parent).skipLabel + ":");
		}
		else if(parent instanceof IfStatNode) {
			visitChildren(writer);
			writer.println("ujp label" + ((IfStatNode)parent).ifLabel);
			visitRightSiblings(writer);
		}
		else if(parent instanceof ElseStatNode) {
			writer.println("label" + ((IfStatNode)parent.parent).elseLabel + ":");
			visitChildren(writer);
			writer.println("label" + ((IfStatNode)parent.parent).ifLabel + ":");
		}
		else if (parent instanceof ForStatNode) {
			super.visitNode(writer);
		}
	}

}
