package tree;

import smallC.smallCParser.TypedefContext;

public class TypedefNode extends Node {

	private static final long serialVersionUID = 1266921236308790854L;

	public TypedefNode(TypedefContext ctx) {
		super(ctx);
		name = "typedef";
	}

	//Not fully supported
}
