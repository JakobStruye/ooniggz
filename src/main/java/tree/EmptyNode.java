package tree;

import java.io.PrintWriter;

import org.antlr.v4.runtime.ParserRuleContext;

public class EmptyNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = -997049649195137118L;

	public EmptyNode(ParserRuleContext ctx) {
		super(ctx);
		name = "empty";
	}

	@Override
	public void visitNode(PrintWriter writer) {
		//Part of ForStat condition: check which part and add labels and jumps accordingly
		if (rightSibling != null && rightSibling instanceof ScopedBodyNode && leftmostSibling != this) {
			writer.println("label" + ((ForStatNode)parent).loopLabel + ":");
		}
		
		if(parent instanceof ForStatNode && rightSibling == null) {
			writer.println("ujp label" + ((ForStatNode)parent).loopLabel);
			writer.println("label" + ((ForStatNode)parent).skipLabel + ":");
		}
		
		super.visitRightSiblings(writer);
	}

}
