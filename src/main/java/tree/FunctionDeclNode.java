package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.FunctionDeclContext;
import symbolTable.SymbolTable;

public class FunctionDeclNode extends Node {

	private static final long serialVersionUID = 6435480325779654436L;
	
	String fctDeclName;

	public FunctionDeclNode(FunctionDeclContext ctx) {
		super(ctx);
		name = "function decl";
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		
		Node backup = leftmostChild; //ignore children during setinfo
		leftmostChild = null;
		Integer retVal = super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		leftmostChild = backup;
		return retVal;
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		//Grab function name from FunctionNameNode and prune it
		FunctionNameNode fctNameNode = (FunctionNameNode) this.leftmostChild.rightSibling;
		fctDeclName = fctNameNode.fctName;
		name += ": " + fctDeclName;
		fctNameNode.prune();
		
		Node backup = leftmostChild; //ignore children during abstractify
		leftmostChild = null;
		super.abstractify(symbolTable);
		leftmostChild = backup;

	}
	

	@Override
	public
	void visitNode(PrintWriter writer) {
		visitRightSiblings(writer); //No point in visiting children
	}
}
