package tree;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.FunctionCallContext;
import symbolTable.Entry;
import symbolTable.FunctionEntry;
import symbolTable.SymbolTable;

public class FunctionCallNode extends Node {

	private static final long serialVersionUID = -3277301483248920253L;
	
	String fctName;
	
	List<Type> paramTypeList = new ArrayList<Type>();
	List<Boolean> paramPointerList = new ArrayList<Boolean>(); //i-th elem true if i-th param is pointer
	
	Type type;
	Boolean isPointer = false;
	Boolean isConst = false;
	
	Integer size = 0;
	
	Integer overloadID;
	
	List<Integer> localLocations = new ArrayList<Integer>();

	public FunctionCallNode(FunctionCallContext ctx) {
		super(ctx);
		name = "function call";
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		Integer retVal = super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		
		//set some info
		FunctionNameNode fctNameNode = (FunctionNameNode) this.leftmostChild;
		String thisFctName = fctNameNode.fctName;

		Node child = this.leftmostChild.rightSibling;
		while (child != null) {
			//fill in the lists
			paramTypeList.add(((ArithNode)(child)).type);
			paramPointerList.add(((ArithNode)(child)).isPointer);
			child = child.rightSibling;
		}
		
		//Find all functions with the right name
		List<FunctionEntry> functions = symbolTable.getFunctionEntries(thisFctName);
		if(functions.isEmpty()) {
			//Check if any exist
			throw new SemanticException("Call to undefined function " + thisFctName, lineNr);
		}
		
		//Check if one with the right signature exists
		FunctionEntry fctEntry = null;
		for (FunctionEntry entry : functions) {
			if (paramTypeList.equals(entry.paramTypes) && paramPointerList.equals(entry.paramPointers)) {
				fctEntry = entry;
				break;
			}
		}
		if (fctEntry == null) {
			throw new SemanticException("Parameters of function call not recognized", lineNr);
		}
		if (fctEntry.returnType.equals("int")) {
			type = Type.INT;
		}
		else if (fctEntry.returnType.equals("char")) {
			type = Type.CHAR;
		}
		
		else if (fctEntry.returnType.equals("void")) {
			type = Type.VOID;
		}
		else { System.out.println("UNKNOWN TYPE FCTCALLNODE"); }

		overloadID = fctEntry.overloadID; //For argument overloading, to create unique labels
		
		return retVal;
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		
		//Get all vars at this scope
		Entry entry = symbolTable.getFirstAtLevel(scopes.peek());
		while (entry != null) {
			localLocations.add(entry.location);
			entry = entry.level;
		}
		
		//Grab function name from FunctionNameNode and prune it
		FunctionNameNode fctNameNode = (FunctionNameNode) this.leftmostChild;
		fctName = fctNameNode.fctName;
		name += ": " + fctName;
		fctNameNode.prune();

		super.abstractify(symbolTable);
		
		//Get size of arguments
		Node child = leftmostChild.rightSibling;
		while(child != null) {
			size++;
			child = child.rightSibling;
		}
	}

	@Override
	public
	void visitNode(PrintWriter writer) {
		
		//Backup some values that might be overwritten by recursive calls
		for (Integer loc : localLocations) {
			writer.println("ldo i " + loc);
		}
		
		Boolean wasVisitRight = visitRight; //backup
		visitRight = true;
		writer.println("mst 0");
		Node child = leftmostChild;
		while(child != null) { //iterate over arguments
			if(child instanceof AtomNode && ((AtomNode)child).location != -1) {
				writer.println("ldc a " + ((AtomNode)child).location); //load variables
				writer.println("ind i");
			}
			else if(child instanceof AtomNode){ //load constants
				AtomNode aChild = ((AtomNode)child);
				if(aChild.isInt) {
					// is literal
					writer.println("ldc i " + aChild.intgr);
				}
				else if(aChild.isChar) {
					writer.println("ldc i " + (int) aChild.chr);
				}
			}
			else if (child instanceof ArrayElemNode) { //load arrays
				visitRight = false;
				child.visitNode(writer);
				visitRight = true;
				writer.println("ind i");
			}
			else if(child instanceof ArithNode || child instanceof FunctionCallNode) { //load ariths and functioncalls
				visitRight = false;
				child.visitNode(writer);
				visitRight = true;
			}
			child = child.rightSibling;
		}
		writer.println("cup " + size + " func_" + fctName + overloadID); //reserve stack space
		visitRight = wasVisitRight; //restore value
		
		//Restore some values but backup return first if not void
		
		if (type != Type.VOID) { //backup return to reserved STORE[0]
			writer.println("sro i 0");
		}
		Collections.reverse(localLocations); //in reverse, reload backupped values from stack
		for (Integer loc : localLocations) {
			writer.println("sro i " + loc);
		}
		if (type != Type.VOID) { //restore return backup
			writer.println("ldo i 0");
		}		
		
		if (visitRight) {
			visitRightSiblings(writer);
		}
		
	}

}
