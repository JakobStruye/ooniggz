package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.SwitchStatContext;

public class SwitchStatNode extends Node {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8937866227130946517L;
	
	Type type;
	Integer location = -1;
	public Integer endLabel;

	public SwitchStatNode(SwitchStatContext ctx) {
		super(ctx);
		name = "switch";
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		
		endLabel = maxLabel;
		maxLabel++;
		
		maxScope = maxScope + 1;
		scopes.push(maxScope);
		
		Integer retVal = super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		
		scopes.pop();
		
		type = ((IDNode)leftmostChild).type;
		location = ((IDNode)leftmostChild).location; // The cases will use this location to load the switch value
		
		// Compare the switch type with the type of its cases
		Node nextChild = leftmostChild.rightSibling;
		while(nextChild != null) {
			if(!((CaseSwitchNode)nextChild).isDefault && ((CaseSwitchNode)nextChild).type != type) {
				if(((CaseSwitchNode)nextChild).type != null) {
					throw new SemanticException("Switch/Case type mismatch: Switch is of type " + type + " and case is of type " +  ((CaseSwitchNode)nextChild).type, lineNr);
				}
				else {
					throw new SemanticException("Switch/Case type mismatch", lineNr);
				}
			}
			nextChild = nextChild.rightSibling;
		}
		
		return retVal;
	}

	@Override
	public void visitNode(PrintWriter writer) {
		// Generate the switch case tests
		if (leftmostChild != null) {
			leftmostChild.visitNode(writer);
		}
		
		// Generate switch case code
		Node nextCase = leftmostChild.rightSibling;
		while(nextCase != null) {
			((CaseSwitchNode)nextCase).generateSwitchCode(writer);
			nextCase = nextCase.rightSibling;
		}
		
		// Mark the end of the switch
		writer.println("switchEnd" + endLabel + ":");
		
		if (rightSibling != null) {
			rightSibling.visitNode(writer);
		}
	}
	
	
	
	

}
