package tree;

import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.IfStatContext;

public class IfStatNode extends Node {

	private static final long serialVersionUID = 4971620403901404630L;
	
	Integer ifLabel = -1;
	Integer elseLabel = -1;

	public IfStatNode(IfStatContext ctx) {
		super(ctx);
		name = "if";
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		//Claim labels
		Node.maxLabel++;
		ifLabel = maxLabel;
		Node.maxLabel++;
		elseLabel = maxLabel;
		
		return super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
	}
}
