package tree;

import smallC.smallCParser.InvertContext;

public class InvertNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2907213050571925140L;

	public InvertNode(InvertContext ctx) {
		super(ctx);
		name = "invert";
	}
	
	//Parent will check presence and generate code

}
