package tree;

public enum Type {
	INT {
		public String toString() {
			return "int";
		}
	}
	, CHAR {
		public String toString() {
			return "char";
		}
	}
	, VOID {
		public String toString() {
			return "void";
		}
	}
}
