package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.PrintfCallContext;
import symbolTable.SymbolTable;

public class PrintfCallNode extends Node {

	private static final long serialVersionUID = -2919658974596245367L;
	
	//Some info
	String string;
	char type = 'x';
	public Integer width = 0;
	
	//labels used to jump out of char array print
	public static Integer pLabel = 0;
	public Boolean arrayPrint = false;

	public PrintfCallNode(PrintfCallContext ctx) {
		super(ctx);
		name = "printfcall";
		if (ctx.EMPTYSTRING() != null) { //nothing to print
			string = "";
		}
		else {
			string = ctx.STRING().getText();
			//cut off the ""
			string = string.substring(1, string.length()-1);
		}
		name += ": " + string;
	}
	
	public PrintfCallNode(String str, Integer linenr) {
		//used when one printfcallnode generates others
		name = "printfcall";
		string = str;
		name += ": " + string;
		this.lineNr = linenr;
	}
	
	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		Boolean argNeeded = false;
		String newString = "";
		for(int i = 0; i < string.length(); i++) {
			//Parse char per char until % is encountered
			char c = string.charAt(i);
			
			if(c == '%') {
				argNeeded = true; //An argument should be supplied in this case
				// Create new printfNode
				c = string.charAt(++i); //skip past %
				String widthStr = ""; //check if width specified
				while(Character.isDigit(c)) {
					widthStr += c;
					c = string.charAt(++i);
				}
				
				if (this.leftmostChild == null) {
					throw new SemanticException("Missing argument for printf", lineNr);
				}
				if(widthStr != "") { //convert width string to Integer
					width = Integer.parseInt(widthStr);
				}	
				
				// parse type
				switch(c) {
				case 's':
					//Try to find ID
					Node child = leftmostChild;
					if (!(leftmostChild instanceof StringNode)) {
						child = leftmostChild.leftmostChild;
						if (child == null || !(child instanceof AtomNode )) {
							throw new SemanticException("Printf call type mismatch for type code " + c, lineNr);
						}
						child = child.leftmostChild; //should be ID
						if (!(child instanceof IDNode)) {
							throw new SemanticException("Printf call type mismatch for type code " + c, lineNr);
						}
						arrayPrint = true; //char[] print detected!
						((IDNode)child).allowArrayAsVariable = true; //In this case array variable can be used without []
					}
					
					//Check if correct type of argument was supplied
					if(!(this.leftmostChild instanceof StringNode) && !((child instanceof IDNode) &&
							(((IDNode)child).isArray == true))) {
						throw new SemanticException("Printf call type mismatch for type code " + c, lineNr);
					}
					type = 's';

					break;
				case 'd': //d and i are the same
				case 'i':
					if(!(this.leftmostChild instanceof ArithNode)) {
						throw new SemanticException("Printf call type mismatch for type code " + c, lineNr);
					}
					type = 'i';
					break;
				case 'c':
					if(!(this.leftmostChild instanceof ArithNode)) {
						throw new SemanticException("Printf call type mismatch for type code " + c, lineNr);
					}
					type = 'c';
					break;
				default:
					throw new SemanticException("Unsupported printf type code " + c, lineNr);
				}
				
				// skip typecode
				++i;
				
				if(i < string.length()-1) {
					// Create new printfcall node
					PrintfCallNode newNode = new PrintfCallNode(string.substring(i), lineNr);
					newNode.parent = this.parent;
					newNode.leftmostSibling = this.leftmostSibling;
					newNode.rightSibling = this.rightSibling;
					newNode.leftmostChild = this.leftmostChild.rightSibling;
					
					if(newNode.leftmostChild != null) {
						newNode.leftmostChild.parent = newNode;
						newNode.leftmostChild.leftmostSibling = newNode.leftmostChild;
					}
					this.rightSibling = newNode;
					this.leftmostChild.rightSibling = null;
				}
				else {
					//End of string, check if any arguments left
					if (this.leftmostChild.rightSibling != null) {
						throw new SemanticException("Too many arguments for printf call", lineNr);
					}
				}
				this.string = newString;
				name = "printfcall";
				name += ": " + string;
				break;
			}
			newString += c;

		}
		if (!argNeeded && this.leftmostChild != null) { //Also for extra args if no % was found
			throw new SemanticException("Too many arguments for printf call", lineNr);
		}
		return super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
	}

	@Override
	public
	void visitNode(PrintWriter writer) {
		for(int i = 0; i < string.length(); i++) {
			//Iterate over string again
			char c = string.charAt(i);
			if(c == '\\' && (i < (string.length()-1)) && string.charAt(i+1) == 'n') {
				//Newline print
				writer.println("ldc i 10");
				writer.println("conv i c");
				writer.println("out c");
				i++;
				continue;
			}
			writer.println("ldc c '" + c + "'"); //load and print the char
			writer.println("out c");
		}
		//Printf string was cut off after %., so it's always at the end
		if (leftmostChild != null) {
			int len = 0; //get the length of the thing to be printed
			if(leftmostChild instanceof AtomNode) {
				len = ((AtomNode)leftmostChild).getLength();
			}
			else if (leftmostChild instanceof StringNode) {
				len = ((StringNode)leftmostChild).string.length();
			}
			
			for(int j = 0; j < (width-len); j++) {
				// print whitespace
				writer.println("ldc c ' '");
				writer.println("out c");
			}
			
			leftmostChild.visitNode(writer); //visit will load the value (has no rightsiblings, no need to prevent visit)
		}
		
		switch(type) {
		//actually do the printing
		case 'i':
			writer.println("out i");
			break;
		case 'c':
			writer.println("conv i c");
			writer.println("out c");
			break;
		case 's':
			if (leftmostChild instanceof StringNode) {
				break; //StringNode will print
			}
			//char array print
			Integer loc = ((AtomNode)(leftmostChild)).location -1; //-1 as we start by incrementing it
			writer.println("ldc a " + loc); //starting address
			writer.println("printf" + pLabel + ":"); //jump back to this
			writer.println("inc a 1"); //on to next char
			writer.println("dpl a"); //copy the address and check if exit char
			writer.println("ind i");
			writer.println("ldc i 27");
			writer.println("neq i");
			writer.println("fjp dummyP" + pLabel); //Exit loop if end reached
			
			
			writer.println("dpl a"); //copy the address, load char and print
			writer.println("ind i");
			writer.println("conv i c");
			writer.println("out c");
			writer.println("ujp printf" + pLabel);
			
			writer.println("conv a b");
			writer.println("fjp dummyP" + pLabel); //end of loop and also way to pop address
			writer.println("dummyP" + pLabel + ":");
			
			pLabel++;		}
		
		if (rightSibling != null) {
			rightSibling.visitNode(writer);
		}	}

}
