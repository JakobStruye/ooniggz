package tree;

import java.io.PrintWriter;

import smallC.SemanticException;

import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.ReturnStatContext;
import symbolTable.SymbolTable;

public class ReturnStatNode extends Node {

	private static final long serialVersionUID = 882901613947668437L;
	
	Type type;
	Boolean isPointer = false;
	Boolean isConst = false;
	

	public ReturnStatNode(ReturnStatContext ctx) {
		super(ctx);
		name = "return stat";
	}

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {

		//Notify all ancestors of return
		Node parentNode = parent;
		while (parentNode != null) {
			parentNode.hasReturn = true;
			//Can't be sure if parent of while/for/if will reach this code, stop propagating
			//Obviously also stop propagating at function definition
			if((parentNode instanceof WhileStatNode)|| (parentNode instanceof ForStatNode) ||
					(parentNode instanceof FunctionDefNode) || (parentNode instanceof IfStatNode)) {
				break;
			}
			else if (parentNode instanceof ElseStatNode) {
				//Do propagate from else in case if also has return
				if (!(parentNode.parent.hasReturn)) { //ifstatnode
					break;
				}
				else {
					parentNode = parentNode.parent; //skip past if
				}
			}
			
			parentNode = parentNode.parent;
		}
		
		
		Integer retVal =  super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		
		if(this.leftmostChild == null) {
			//void return
			type = null;
			isPointer = null;
			isConst = null;
		}
		else {
			//Get info from the return value
			type = ((ArithNode)(this.leftmostChild)).type;
			isPointer = ((ArithNode)(this.leftmostChild)).isPointer;
			isConst = ((ArithNode)(this.leftmostChild)).isConst;
		}
		return retVal;
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		if(leftmostChild != null) {
			// This is not just a 'return;' there is something to be returned
			// Find out what our returnType should be.
			Node node = parent;
			while(!(node instanceof FunctionDefNode)) {
				node = node.parent;
			}
			
			// Right now node is the function definition
			FunctionDefNode fncNode = (FunctionDefNode) node;
			// Figure out the return type
			String returnType = ((symbolTable.getFunctionEntries(fncNode.fctDefName).get(0))).returnType;
			Type type = ((ArithNode) leftmostChild).type;
			//Check if it matches this return
			if (type != null && !(type.toString().equals(returnType))) {
				throw new SemanticException("Incorrect returnType", lineNr);
			}
		}
		super.abstractify(symbolTable);
	}

	@Override
	public
	void visitNode(PrintWriter writer) {
		if(!isMain) {
			if(leftmostChild == null) {
				writer.println("retp"); //no return value
			}
			else {
				visitChildren(writer); //load return value
				writer.println("str i 0 0");
				writer.println("retf");
			}
			visitRightSiblings(writer);
		}
		else {
			//Program halts on main return
			writer.println("hlt");			
		}
	}

}
