package tree;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.DefinitionContext;
import symbolTable.Entry;
import symbolTable.SymbolTable;

public class DefinitionNode extends Node {
	
	private static final long serialVersionUID = 3639057516859420150L;
	
	//Some info on the defined value
	String type = "";
	Boolean isConst = false;
	Boolean firstConst = false;
	Boolean isPointer = false;
	String varName;
	Boolean isArray = false;
	
	Integer location = -1; //on store

	public DefinitionNode(DefinitionContext ctx) {
		super(ctx);
		name = "Definition";
	}
	
	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		Boolean isDecl = false;
		if (this.leftmostChild instanceof TypeNode) {
			isDecl = true; //check if also declaration
		}
		
		//Possible useMaxScope if defined in loop or if/else condition
		Integer thisMaxScope = maxScope;
		Boolean useMaxScope = useMaxScopeList.remove();
		if (useMaxScope) {
			scopes.push(thisMaxScope+1);
		}
		
		Node node = this.leftmostChild;
		//Find ID/ArrayElemNode
		while(!(node instanceof IDNode) && !(node instanceof ArrayElemNode)) {
			node = node.rightSibling;
		}
		
		if (node instanceof IDNode) {
			//Set some info
			this.varName = ((IDNode)node).id; 
			((IDNode)node).isDecl = isDecl;
		}
		
		if (this.leftmostChild instanceof TypeNode) {
			idShouldExist = false; //declaration, ID should not yet be declared
			TypeNode typeNode = (TypeNode) this.leftmostChild; //grab type info
			type = typeNode.type;
			name += ": " + type;
			
			//Find the entry in the symboltable
			Entry entry = symbolTable.getVarEntry(varName, scopes, isDecl);
			if (entry != null && isDecl) {
				entry.declared = true; //Indicate it's been declared now
			}
			
			//set some more info
			if (typeNode.isConst) {
				isConst = true;
				name += "\n(const)";
				if(entry != null) {
					if(entry.isConst) {
						throw new SemanticException("Trying to redefine const variable", lineNr);
					}
					entry.isConst = true;
				}
			}
		}

		//check for pointer
		if (this.leftmostChild.rightSibling instanceof PtrNode) {
			isPointer = true;
			name += "\n(ptr)";
		}
		
		return super.setInfo(scopes, maxScope, useMaxScopeList, useMaxScope);
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		Boolean isDecl = false;
		//Prune Type and PtrNode
		if (this.leftmostChild instanceof TypeNode) {
			isDecl = true;
			this.leftmostChild.prune();
		}
		if (this.leftmostChild.rightSibling instanceof PtrNode) {
			this.leftmostChild.rightSibling.prune();
		}
		
		if (isDecl) {
			//check for self-assignment
			if (!isArray) {
				assigningID = varName;
			}
		}
		
		if (leftmostChild != null) {
			leftmostChild.abstractify(symbolTable);
		}
		
		assigningID = ""; //reset this, must be in between children and rightsibling abstractify

		if (rightSibling != null) {
			rightSibling.abstractify(symbolTable);
		}		
		
		//Grab the entry again
		Entry entry = symbolTable.getVarEntry(varName, scopes, isDecl);
		//Check for modifying of const var
		if(entry != null && entry.isConst && !isConst) {
			throw new SemanticException("Trying to modify a const variable", lineNr);
		}
		
		Node node = leftmostChild;
		while(!(node instanceof IDNode) && !(node instanceof ArrayElemNode)) {
			node = node.rightSibling;
		}
		if(node instanceof IDNode) {
			location = ((IDNode) node).location;
		}
		else {
			// arrayElem: indicate by leaving location -1
		}
	}
	
	@Override
	public
	void visitNode(PrintWriter writer) {
		
		PrintWriter backup = writer;
		//Write global scope code to temporary file
		if (scopes.size() == 1) {
			try {
				writer = new PrintWriter(new BufferedWriter(new FileWriter("temp.p", true)));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
		if (leftmostChild != null) {
			leftmostChild.visitNode(writer);
		}
		
		//if array supply location
		if (location != -1) {
			writer.println("sro i " + location);
		}
		else { //else it will be top of stack
			writer.println("sto i");
		}

		//Check if in ForStat and create label/jump accordingly
		if(parent != null && parent instanceof ForStatNode && rightSibling == null) {
			writer.println("ujp label" + ((ForStatNode)parent).loopLabel);
			writer.println("label" + ((ForStatNode)parent).skipLabel + ":");
		}
		
		writer.flush(); //Actually push to file
		writer = backup; //Reset regular writer (if it was replaced by temp
		if (rightSibling != null) {
			rightSibling.visitNode(writer);
		}	}

}
