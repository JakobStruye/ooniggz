package tree;

import smallC.smallCParser.DeclTypeContext;

public class DeclTypeNode extends Node {

	private static final long serialVersionUID = 6470222820361899225L;
	
	String type;
	Boolean isConst = false;

	public DeclTypeNode(DeclTypeContext ctx) {
		super(ctx);
		if (ctx.CONST() != null) {
			isConst = true;
			type = ctx.getText().substring(5);
		}
		else {
			type = ctx.getText();
		}
		name = "type";
	}

}
