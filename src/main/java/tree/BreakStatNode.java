package tree;

import java.io.PrintWriter;

import smallC.SemanticException;
import smallC.smallCParser.BreakStatContext;

public class BreakStatNode extends Node {

	private static final long serialVersionUID = -2943796014055237921L;

	public BreakStatNode(BreakStatContext ctx) {
		super(ctx);
		name = "break";
	}

	@Override
	public
	void visitNode(PrintWriter writer) {
		Node ancestor = parent;
		//Find if it's in a for, while or switch
		while (!(ancestor instanceof ForStatNode) && !(ancestor instanceof WhileStatNode) && !(ancestor instanceof CaseSwitchNode)) {
			ancestor = ancestor.parent;
			if (ancestor == null) {
				throw new SemanticException("break used outside of loop or switch", lineNr);
			}
		}
		//Add the correct jump
		if (ancestor instanceof ForStatNode) {
			writer.println("ujp label" + ((ForStatNode)ancestor).skipLabel);
		}
		else if (ancestor instanceof WhileStatNode) {
			writer.println("ujp label" + ((WhileStatNode)ancestor).skipLabel);
		}
		else if (ancestor instanceof CaseSwitchNode) {
			writer.println("ujp switchEnd" + ((CaseSwitchNode)ancestor).endLabel);
		}
		
		super.visitRightSiblings(writer); //no children to visit

	}

}
