package tree;

import java.util.Queue;
import java.util.Stack;

import smallC.smallCParser.WhileStatContext;

public class WhileStatNode extends Node {

	private static final long serialVersionUID = 3110243138732491435L;
	
	Integer skipLabel = -1;
	Integer loopLabel = -1;

	public WhileStatNode(WhileStatContext ctx) {
		super(ctx);
		name = "while";
	}
	
	

	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		//Claim the labels
		Node.maxLabel++;
		skipLabel = maxLabel;
		Node.maxLabel++;
		loopLabel = maxLabel;
		
		return super.setInfo(scopes, maxScope, useMaxScopeList, undoUseMaxScope);
		
	}

}
