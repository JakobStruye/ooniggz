package tree;

import java.io.PrintWriter;

import smallC.SemanticException;
import smallC.smallCParser.FunctionDefContext;
import symbolTable.SymbolTable;

public class FunctionDefNode extends Node {

	private static final long serialVersionUID = 6435480325779654436L;
	
	String fctDefName;
	
	Integer overloadID; //for argument overloading

	public FunctionDefNode(FunctionDefContext ctx) {
		super(ctx);
		name = "function def";
	}

	@Override
	public void abstractify(SymbolTable symbolTable) {
		//Grab function name from FunctionNameNode and prune it
		FunctionNameNode fctNameNode = (FunctionNameNode) this.leftmostChild.rightSibling;
		fctDefName = fctNameNode.fctName;
		name += ": " + fctDefName;
		fctNameNode.prune();
		
		//Check if returns missing in any control branch
		if (!hasReturn && !((FunctionTypeNode)leftmostChild).fctTypeName.equals("void")) {
			if (!noReturnError) { //Error or warning?
				throw new SemanticException("Not all control branches have a return statement in " + fctDefName, lineNr);
			}
			else {
				System.out.println("Warning: Not all control branches have a return statement in " + fctDefName);
			}
		}
		
		//Set the label number, unique for all functions with this name
		Integer overloadVal = overloadMap.get(fctDefName);
		if (overloadVal != null) {
			overloadID = overloadVal;
		}
		else {
			overloadID = 0;
		}
		overloadMap.put(fctDefName, overloadID+1); //to notify others with same name
		
		
		super.abstractify(symbolTable);
	}
	
	@Override
	public
	void visitNode(PrintWriter writer) {
		if(fctDefName.equals("main")) {
			isMain = true;
		}
		if (isMain) {
			writer.println("func_" + fctDefName + ":"); //main can't be overloaded

		}
		else {
			writer.println("func_" + fctDefName + overloadID + ":");
		}

		visitChildren(writer);
		if(!isMain) {
			writer.println("retp"); //return if not main
			
		}
		else {
			writer.println("hlt"); //halt if main
		}
		isMain = false;
		visitRightSiblings(writer);
	}

}
