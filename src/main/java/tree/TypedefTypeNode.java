package tree;

import smallC.smallCParser.TypedefTypeContext;

public class TypedefTypeNode extends Node {

	private static final long serialVersionUID = -4214976740369452289L;
	
	String type;

	public TypedefTypeNode(TypedefTypeContext ctx) {
		super(ctx);
		name = "typedefType";
		type = ctx.getText();
	}

	//Not fully supported
}
