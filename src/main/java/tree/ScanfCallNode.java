package tree;

import java.io.PrintWriter;
import java.util.Queue;
import java.util.Stack;

import smallC.SemanticException;
import smallC.smallCParser.ScanfCallContext;
import symbolTable.SymbolTable;

public class ScanfCallNode extends Node {

	private static final long serialVersionUID = -2919658974596245367L;
	
	String string;
	char type = 'x';
	Boolean hasAmpersand = false;
	
	static Integer sLabel = 0; 
	
	public ScanfCallNode(ScanfCallContext ctx) {
		super(ctx);
		name = "scanfcall";
		if (ctx.EMPTYSTRING() != null) {
			string = ""; //nothing to scan
		}
		else {
			string = ctx.STRING().getText();
			//cut off the ""
			string = string.substring(1, string.length()-1);
		}
		name += ": " + string;
	}
	
	public ScanfCallNode(String str, Integer linenr) {
		//used when a ScanfCallNode generates more
		name = "scanfall";
		string = str;
		name += ": " + string;
		this.lineNr = linenr;
	}


	@SuppressWarnings("unchecked")
	@Override
	public Integer setInfo(Stack<Integer> scopes, Integer maxScope,
			Queue<Boolean> useMaxScopeList, Boolean undoUseMaxScope) {
		
		//SETINFO CHILDREN
		this.scopes = (Stack<Integer>)scopes.clone();
		if (leftmostChild != null) {
			maxScope = leftmostChild.setInfo(scopes, maxScope, useMaxScopeList, false);
		}
		if (undoUseMaxScope) {
			scopes.pop();
		}

		
		Boolean argNeeded = false;
		for(int i = 0; i < string.length(); i++) {
			//Read and ignore any non %
			char c = string.charAt(i);
			
			if(c == '%') {
				argNeeded = true; //some argument to store scan in required
				c = string.charAt(++i);
				
				if (this.leftmostChild == null) {
					throw new SemanticException("Missing argument for scanf", lineNr);
				}
				
				Node child = this.leftmostChild;
				if (child instanceof AmpersandNode) { //check for ampersand (address)
					hasAmpersand = true;
					//remove the ampersandnode
					child = child.rightSibling;
					child.leftmostSibling = child;
					this.leftmostChild = child;
				}
				
				// parse type and check if provided argument is correct type (and if ampersand usage is correct
				switch(c) {
				case 's':
					if(!(((IDNode)child).type == Type.CHAR) && (((IDNode)child).isArray == true)) {
						throw new SemanticException("Scanf call type mismatch for type code " + c, lineNr);
					}
					if (hasAmpersand) {
						throw new SemanticException("Superfluous & in scanf call", lineNr);
					}
					type = 's';
					break;
				case 'd':
				case 'i':
					if(!(((IDNode)child).type == Type.INT)) {
						throw new SemanticException("Scanf call type mismatch for type code " + c, lineNr);
					}
					if (!hasAmpersand) {
						throw new SemanticException("Missing & in scanf call", lineNr);
					}
					type = 'i';
					break;
				case 'c':
					if(!(((IDNode)child).type == Type.CHAR)) {
						throw new SemanticException("Scanf call type mismatch for type code " + c, lineNr);
					}
					if (!hasAmpersand) {
						throw new SemanticException("Missing & in scanf call", lineNr);
					}
					type = 'c';
					break;
				default:
					throw new SemanticException("Unsupported scanf type code " + c, lineNr);
				}
				
				// skip typecode
				++i;
				
				if(i < string.length()-1) {
					// Create new ScanfCallNode
					ScanfCallNode newNode = new ScanfCallNode(string.substring(i), lineNr);
					newNode.parent = this.parent;
					newNode.leftmostSibling = this.leftmostSibling;
					newNode.rightSibling = this.rightSibling;
					newNode.leftmostChild = this.leftmostChild.rightSibling;
					
					if(newNode.leftmostChild != null) {
						newNode.leftmostChild.parent = newNode;
						newNode.leftmostChild.leftmostSibling = newNode.leftmostChild;
					}
					this.rightSibling = newNode;
					this.leftmostChild.rightSibling = null;
				}
				else {
					//End of string, check if any arguments left
					if (this.leftmostChild.rightSibling != null) {
						throw new SemanticException("Too many arguments for scanf call", lineNr);
					}
				}

				break;
			}
		}
		//Also check for extra arguments if no % detected
		if (!argNeeded && this.leftmostChild != null) {
			throw new SemanticException("Too many arguments for scanf call", lineNr);
		}
		//SETINFO RIGHTSIBLINGS
		if (rightSibling != null) {
			maxScope = rightSibling.setInfo(scopes, maxScope, useMaxScopeList, false);
		}
		//empty the string, it's not needed
		string = "";
		name = "scanfcall " + type;
		return maxScope;
	}

	@Override
	public
	void visitNode(PrintWriter writer) {
		if (type == 'c') {
			//Load the address to store scan in, scan, store
			writer.println("ldc a " + ((IDNode)leftmostChild).location);
			writer.println("in c");
			writer.println("conv c i"); //convert to int to store
			writer.println("sto i");
		}
		else if (type == 'i') {
			//Load the address to store scan in, scan, store
			writer.println("ldc a " + ((IDNode)leftmostChild).location);
			writer.println("in i");
			writer.println("sto i");
		}
		else if (type == 's') {
			Integer loc = ((IDNode)leftmostChild).location -1; //-1 as we start by incrementing it
			writer.println("ldc a " + loc);
			writer.println("scanf" + sLabel + ":");
			writer.println("inc a 1"); //move on to next address
			writer.println("dpl a"); //copy the address
			writer.println("in c");  //scan and store the char
			writer.println("conv c i");
			writer.println("sto i");
			writer.println("dpl a"); //copy the address
			writer.println("ind i"); //check if escape char was read and exit loop if so
			writer.println("ldc i 27");
			writer.println("equ i");
			writer.println("fjp scanf" + sLabel);
			
			//This part is just a workaround to pop the unneeded address
			writer.println("conv a b");
			writer.println("fjp dummyS" + sLabel); //workaround to pop stack, need to get rid of that address
			writer.println("dummyS" + sLabel + ":");
			
			sLabel++;
		}
		super.visitRightSiblings(writer);

	}

}
