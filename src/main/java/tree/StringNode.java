package tree;

import java.io.PrintWriter;

import smallC.smallCParser.StringContext;

public class StringNode extends Node {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2770049897238657381L;

	public String string;
	
	StringNode(StringContext ctx) {
		super(ctx);
		this.string = ctx.STRING().getText();
		string = string.substring(1, string.length()-1);
		name = string;
	}
	
	@Override
	public
	void visitNode(PrintWriter writer) {
		//Always in printfcall, so just print the entire thing
		for(char c : string.toCharArray()) {
			writer.println("ldc c '" + c + "'");
			writer.println("out c");
		}
	}
}
