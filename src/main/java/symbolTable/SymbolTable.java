package symbolTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class SymbolTable {
	HashMap<String, Entry> varMap = new HashMap<String, Entry>(); //Entries by name
	HashMap<Integer, Entry> scopeMap = new HashMap<Integer, Entry>(); //Entries by scope
	
	HashMap<String, FunctionEntry> varMapFcts = new HashMap<String, FunctionEntry>(); //Functions by name
	Integer location = 1; // The next location to be used
	Integer functionCounter = 0; // The next functionCounter to be used
	
	/**
	 * Add entry to symbol table
	 * @param name name of the var
	 * @param attributes attributes of the var
	 * @param scope level of the var
	 * @param size number of positions it will take to store in P
	 * @return
	 */
	public Boolean putEntry(String name, String attributes, Integer scope, Integer size, List<Integer> dimensions) {
		//Look for current entry with same name (highest level)
		Entry var = varMap.get(name);
		
		//Check if already in symbol table
		if(var != null && name.equals(var.name) && var.scope.equals(scope)) {
			return false;
		}
		
		//Get previous entry with same level
		Entry level = scopeMap.get(scope);
		
		//Set memory location
		Integer loco = location;
		location += size;

		Entry newEntry = new Entry(name, attributes, var, level, loco, scope, size, dimensions);
		
		//Add to both maps
		varMap.put(name, newEntry);
		scopeMap.put(scope, newEntry);
		
		return true;
	}
	
	/**
	 * putEntry with default size 1 (so not an array)
	 */
	public Boolean putEntry(String name, String attributes, Integer scope) {
		return putEntry(name, attributes, scope, 1, new ArrayList<Integer>());
	}
	
	/**
	 * putEntry for functions
	 */
	public Boolean putEntryFunction(String name, String attributes, String returnType, List<String> paramTypes, Boolean declaration) {
		//Look for current entry with same name (highest level)
		FunctionEntry var = varMapFcts.get(name);
		
		if (var != null && var.paramTypesStrings.equals(paramTypes) && !var.isDeclaration && !declaration) {
			// this function is already defined
			// function add to symbol table is a definition but we already had a declaration for that function
			return false;
		}
		else if(var != null && var.paramTypesStrings.equals(paramTypes) && var.returnType.equals(returnType) && var.isDeclaration && !declaration) {
			// we had a declaration function entry
			// the definition is a perfect match so this is a legit definition
			var.isDeclaration = false;
			return true;
		}
		else if(var != null && var.isDeclaration && (!(var.returnType.equals(returnType)) || !(var.paramTypesStrings.equals(paramTypes))))  {
			// We had a declaration function entry
			// the definition was not a perfect match, not consistent!
			// If function forward declared we do not allow overloading
			return false;
		}
		else
		{

			FunctionEntry newEntry = new FunctionEntry(name, attributes, var, returnType, paramTypes, declaration);
			
			//Add to map
			varMapFcts.put(name, newEntry);
			
			return true;	
		}
	}


	public Integer getSize() {
		return varMap.size();
	}
	
	/**
	 * Basic printing method for the contents of the symbol table
	 */
	public void print() {
		for (String name: varMap.keySet()){
            System.out.println(varMap.get(name).toString());
		}
		for (String name: varMapFcts.keySet()) {
            System.out.println(varMapFcts.get(name).toString());

		}
	}
	
	/*
	 * Return a stack of all scopes in which a var with given name appears
	 */
	public Stack<Integer> getVarScopeList(String name) {
		Stack<Integer> scopes = new Stack<Integer>();
		Entry entry = varMap.get(name);
		while (entry != null) {
			scopes.push(entry.scope);
			entry = entry.var;
		}
		return scopes;
	}
	
	/**
	 * Get all function entries with a given name
	 */
	public List<FunctionEntry> getFunctionEntries(String name) {
		List<FunctionEntry> functionEntries = new ArrayList<FunctionEntry>();
		FunctionEntry entry = varMapFcts.get(name);
		while (entry != null) {
			if (entry.isDeclaration != true) {
				functionEntries.add(entry);
			}
			entry = entry.var;
		}
		return functionEntries;
	}
	
	/**
	 * Get a var Entry with a given name in one of the given scopes
	 * declare indicates whether it should already have been declared
	 */
	public Entry getVarEntry(String name, Stack<Integer> scopes, Boolean declare) {
		Entry entry = varMap.get(name);
		while (entry != null) {
			if (scopes.contains(entry.scope)) {
				if (declare || entry.declared) {
					return entry;
				}
			}
			entry = entry.var;
		}
		return null;
	}
	
	public Entry getVarEntry(String name, Stack<Integer> scopes) {
		return getVarEntry(name, scopes, false);
	}
	
	public Integer getLocation() {
		return location;
	}
	
	public Entry getFirstAtLevel(Integer lv) {
		return scopeMap.get(lv);
	}
	

}
