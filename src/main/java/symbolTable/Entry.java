package symbolTable;

import java.util.ArrayList;
import java.util.List;

public class Entry {
	
	public String name;
	public String attributes;
	
	public Entry var; // Points to next Entry with same name  in lower level
	public Entry level; // Points to next Entry within same level
	
	public Integer scope; //Indicates level of this Entry
	public Integer location; //Indicates store location
	public Integer size; //Number of store locations this Entry needs
	
	public Boolean declared = false; //Set to true as soon as declaration is encounted while walking AST
	public Boolean isConst = false;  //Set to true if const entry

	public Boolean isArray = false; //Set to true if array
	public List<Integer> dimensions = new ArrayList<Integer>(); //nonzero for arrays
	
	public Entry(String name, String attributes, Entry var, Entry level, Integer location, Integer scope, Integer size, List<Integer> dimensions) {
		this.name = name;
		this.attributes = attributes;
		this.var = var;
		this.level = level;
		this.location = location;
		this.scope = scope;
		this.size = size;
		this.isArray = (dimensions.size() != 0);
		this.dimensions = dimensions;
	}
	
	public String toString() {
		String str = "[" + name + " " + attributes + " " + location.toString() + " " + scope.toString();
		if(isConst) {
			str += " const";
		}
		str += "]";
		Boolean nextLevel = (this.level != null);
		if (nextLevel) {
			str += " level: " + this.level.name;
		}
		Boolean nextVar = (this.var != null);
		if (nextVar) {
			str += "\n " + this.var.toString();
		}

		return str;
	}
}
