package symbolTable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tree.Type;

public class FunctionEntry {
	
	public String name;
	public String attributes;
	
	public FunctionEntry var; // Points to next FunctionEntry with same name  but different signature
			
	public String returnType;
	public List<Type> paramTypes = new ArrayList<Type>();
	public List<String> paramTypesStrings = new ArrayList<String>(); //same info as above, but as strings
	public List<Boolean> paramPointers = new ArrayList<Boolean>(); //i-th entry true -> i-th entry is pointer
	
	public Boolean isDeclaration = false; //True if declared but not (yet) defined
	
	static public Map<String, Integer> overloadMap = new HashMap<String, Integer>(); //Contains how many times each name has appeared
	public Integer overloadID; //Index used to distinguish same name functions (for overloading bonus feature)
	
	public FunctionEntry(String name, String attributes, FunctionEntry var, 
			String returnType, List<String> paramTypes, Boolean declaration) {
		this.name = name;
		this.attributes = attributes;
		this.var = var;
		this.isDeclaration = declaration;
		
		Integer overloadVal = overloadMap.get(name);
		if (overloadVal != null) {
			overloadID = overloadVal;
		}
		else {
			overloadID = 0;
		}
		overloadMap.put(name, overloadID+1);
		
		this.returnType = returnType;
		paramTypesStrings = paramTypes;
		
		//Get type info
		for (String type : paramTypes) {
			if (type.endsWith("*")) {
				paramPointers.add(true);
			}
			else {
				paramPointers.add(false);
			}
			if (type.startsWith("const ")) {
				type = type.substring(6); //cut off const
			}
			if (type.substring(0, 3).equals("int")) {
				this.paramTypes.add(Type.INT);
			}
			else if (type.substring(0, 4).equals("char")) {
				this.paramTypes.add(Type.CHAR);
			}		
		}
	}
	
	public String toString() {
		String str = "[" + name + " returns " + returnType + " "; 
		if (this.paramTypes.size() > 0) {
			str += "with param types ";
			for (Type param : paramTypes) {
				str += param + " ";
			}
		}	
		str+= "]";
		
		Boolean nextVar = (this.var != null);
		if (nextVar) {
			str += "\n " + this.var.toString();
		}
		return str;
	}

}
