package smallC;

import org.antlr.v4.runtime.CommonToken;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class MyErrorListener extends ConsoleErrorListener {
	
	private Boolean hasError = false;
	private Boolean canCrash = true; //won't call System.exit if false

	@Override
	/**
	 * Converts parser error to an easier to read, less technical format
	 */
	public void syntaxError(Recognizer<?, ?> recognizer,
			Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e) {
		if (!hasError) {
			hasError = true;
			CommonToken symbol = (CommonToken) offendingSymbol;
			if (e instanceof SemanticANTLRException) {
				System.out.println("A semantical error was detected at the token '" + symbol.getText() +
								   "' at line " + line + ":" + charPositionInLine +
								   ". \n" +  e.getMessage() + "\nThe compiler will now terminate.");
			}
			else {
				System.out.println("A syntactical error was detected at the token '" + symbol.getText() +
						   "' at line " + line + ":" + charPositionInLine +
						   ". \nThe compiler will now terminate.");
			}
			if (canCrash) {
				System.exit(1);
			}
			
		}
	}
	
	public Boolean hasErrored() {
		return hasError;
	}
	
	public void noCrash() {
		canCrash = false;
	}

	
}
