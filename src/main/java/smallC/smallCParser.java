// Generated from smallC.g4 by ANTLR 4.3

	package smallC;
	import tree.*;
	import symbolTable.*;
	import java.util.Stack;
	import java.util.Queue;
	import java.util.LinkedList;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class smallCParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__1=1, T__0=2, WHILE=3, IF=4, ELSE=5, FOR=6, BREAK=7, CONTINUE=8, INCLUDE=9, 
		STDIO=10, TYPEINT=11, TYPECHAR=12, TYPEVOID=13, CONST=14, TRUE=15, FALSE=16, 
		RETURN=17, TYPEDEF=18, GOTO=19, INCREMENT=20, DECREMENT=21, PLUS=22, MINUS=23, 
		MULT=24, DIV=25, MOD=26, LPAREN=27, RPAREN=28, LBRACE=29, RBRACE=30, LBRACKET=31, 
		RBRACKET=32, EQ=33, NEQ=34, GEQ=35, LEQ=36, GT=37, LT=38, AND=39, OR=40, 
		COMMA=41, QUOTE=42, DOUBLEQUOTE=43, SEMICOLON=44, COLON=45, ASSIGN=46, 
		AMPERSAND=47, NEGATE=48, SWITCH=49, CASE=50, DEFAULTCASE=51, INT=52, ID=53, 
		LABEL=54, CHAR=55, EMPTYSTRING=56, STRING=57, SINGLECOMMENT=58, MULTICOMMENT=59, 
		WS=60;
	public static final String[] tokenNames = {
		"<INVALID>", "'printf'", "'scanf'", "'while'", "'if'", "'else'", "'for'", 
		"'break'", "'continue'", "'#include'", "'<stdio.h>'", "'int'", "'char'", 
		"'void'", "'const'", "'true'", "'false'", "'return'", "'typedef'", "'goto'", 
		"'++'", "'--'", "'+'", "'-'", "'*'", "'/'", "'%'", "'('", "')'", "'{'", 
		"'}'", "'['", "']'", "'=='", "'!='", "'>='", "'<='", "'>'", "'<'", "'&&'", 
		"'||'", "','", "'''", "'\"'", "';'", "':'", "'='", "'&'", "'!'", "'switch'", 
		"'case'", "'default:'", "INT", "ID", "LABEL", "CHAR", "EMPTYSTRING", "STRING", 
		"SINGLECOMMENT", "MULTICOMMENT", "WS"
	};
	public static final int
		RULE_prog = 0, RULE_stat = 1, RULE_arith = 2, RULE_highop = 3, RULE_lowop = 4, 
		RULE_highrelop = 5, RULE_lowrelop = 6, RULE_logicalandor = 7, RULE_atom = 8, 
		RULE_invert = 9, RULE_precrement = 10, RULE_postcrement = 11, RULE_include = 12, 
		RULE_definition = 13, RULE_declaration = 14, RULE_type = 15, RULE_declType = 16, 
		RULE_ptr = 17, RULE_functionType = 18, RULE_argType = 19, RULE_typedefType = 20, 
		RULE_id = 21, RULE_ampersand = 22, RULE_functionName = 23, RULE_compound = 24, 
		RULE_compoundNoScope = 25, RULE_caseSwitch = 26, RULE_switchStat = 27, 
		RULE_ifStat = 28, RULE_elseStat = 29, RULE_whileStat = 30, RULE_forStat = 31, 
		RULE_breakStat = 32, RULE_continueStat = 33, RULE_functionDef = 34, RULE_functionArguments = 35, 
		RULE_functionDecl = 36, RULE_returnStat = 37, RULE_printfCall = 38, RULE_scanfCall = 39, 
		RULE_functionCall = 40, RULE_arrayDecl = 41, RULE_arrayElem = 42, RULE_typedef = 43, 
		RULE_scopedBody = 44, RULE_gotoStat = 45, RULE_jumpLabel = 46, RULE_string = 47, 
		RULE_empty = 48;
	public static final String[] ruleNames = {
		"prog", "stat", "arith", "highop", "lowop", "highrelop", "lowrelop", "logicalandor", 
		"atom", "invert", "precrement", "postcrement", "include", "definition", 
		"declaration", "type", "declType", "ptr", "functionType", "argType", "typedefType", 
		"id", "ampersand", "functionName", "compound", "compoundNoScope", "caseSwitch", 
		"switchStat", "ifStat", "elseStat", "whileStat", "forStat", "breakStat", 
		"continueStat", "functionDef", "functionArguments", "functionDecl", "returnStat", 
		"printfCall", "scanfCall", "functionCall", "arrayDecl", "arrayElem", "typedef", 
		"scopedBody", "gotoStat", "jumpLabel", "string", "empty"
	};

	@Override
	public String getGrammarFileName() { return "smallC.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }


		SymbolTable symbolTable = new SymbolTable();
		ProgramNode root = null;
		Node parent = null;
		Node sibling = null;
		//scopes maintains a stack of all (unique!) scope numbers currently in use
		Stack<Integer> scopes = new Stack<Integer>();
		//maxScope indicates the highest scope number used so far
		Integer maxScope = 0;
		//useMaxScope indicates maxScope should be used instead of top of scopes (e.g. for function params as those are parsed before adding a scope)
		Boolean useMaxScope = false; 
		Queue<Boolean> useMaxScopeQueue = new LinkedList<Boolean>();
		Boolean stdioInclude = false;
		
		FunctionEntry latestFunction = null;
		Boolean setFunctionArgs = false;
		ArrayList functionArgs = new ArrayList<String>();

	public smallCParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public TerminalNode EOF() { return getToken(smallCParser.EOF, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			this.scopes.push(0);
			setState(100); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(99); stat();
				}
				}
				setState(102); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__0) | (1L << WHILE) | (1L << IF) | (1L << FOR) | (1L << BREAK) | (1L << CONTINUE) | (1L << INCLUDE) | (1L << TYPEINT) | (1L << TYPECHAR) | (1L << TYPEVOID) | (1L << CONST) | (1L << TRUE) | (1L << FALSE) | (1L << RETURN) | (1L << TYPEDEF) | (1L << GOTO) | (1L << INCREMENT) | (1L << DECREMENT) | (1L << MINUS) | (1L << MULT) | (1L << LPAREN) | (1L << LBRACE) | (1L << SEMICOLON) | (1L << AMPERSAND) | (1L << NEGATE) | (1L << SWITCH) | (1L << INT) | (1L << ID) | (1L << LABEL) | (1L << CHAR))) != 0) );
			setState(104); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatContext extends ParserRuleContext {
		public ContinueStatContext continueStat() {
			return getRuleContext(ContinueStatContext.class,0);
		}
		public TerminalNode SEMICOLON() { return getToken(smallCParser.SEMICOLON, 0); }
		public ScanfCallContext scanfCall() {
			return getRuleContext(ScanfCallContext.class,0);
		}
		public JumpLabelContext jumpLabel() {
			return getRuleContext(JumpLabelContext.class,0);
		}
		public WhileStatContext whileStat() {
			return getRuleContext(WhileStatContext.class,0);
		}
		public IfStatContext ifStat() {
			return getRuleContext(IfStatContext.class,0);
		}
		public PrintfCallContext printfCall() {
			return getRuleContext(PrintfCallContext.class,0);
		}
		public BreakStatContext breakStat() {
			return getRuleContext(BreakStatContext.class,0);
		}
		public CompoundContext compound() {
			return getRuleContext(CompoundContext.class,0);
		}
		public GotoStatContext gotoStat() {
			return getRuleContext(GotoStatContext.class,0);
		}
		public SwitchStatContext switchStat() {
			return getRuleContext(SwitchStatContext.class,0);
		}
		public IncludeContext include() {
			return getRuleContext(IncludeContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ForStatContext forStat() {
			return getRuleContext(ForStatContext.class,0);
		}
		public FunctionDefContext functionDef() {
			return getRuleContext(FunctionDefContext.class,0);
		}
		public FunctionDeclContext functionDecl() {
			return getRuleContext(FunctionDeclContext.class,0);
		}
		public TypedefContext typedef() {
			return getRuleContext(TypedefContext.class,0);
		}
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public ReturnStatContext returnStat() {
			return getRuleContext(ReturnStatContext.class,0);
		}
		public ArithContext arith() {
			return getRuleContext(ArithContext.class,0);
		}
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public StatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitStat(this);
		}
	}

	public final StatContext stat() throws RecognitionException {
		StatContext _localctx = new StatContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_stat);
		try {
			setState(147);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(106); arith(0);
				setState(107); match(SEMICOLON);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(109); include();
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(110); definition();
				setState(111); match(SEMICOLON);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(113); declaration();
				setState(114); match(SEMICOLON);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(116); compound();
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(117); ifStat();
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(118); whileStat();
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(119); forStat();
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(120); breakStat();
				setState(121); match(SEMICOLON);
				}
				break;

			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(123); continueStat();
				setState(124); match(SEMICOLON);
				}
				break;

			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(126); functionDef();
				}
				break;

			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(127); functionDecl();
				}
				break;

			case 13:
				enterOuterAlt(_localctx, 13);
				{
				setState(128); returnStat();
				setState(129); match(SEMICOLON);
				}
				break;

			case 14:
				enterOuterAlt(_localctx, 14);
				{
				setState(131); functionCall();
				}
				break;

			case 15:
				enterOuterAlt(_localctx, 15);
				{
				setState(132); printfCall();
				setState(133); match(SEMICOLON);
				}
				break;

			case 16:
				enterOuterAlt(_localctx, 16);
				{
				setState(135); scanfCall();
				setState(136); match(SEMICOLON);
				}
				break;

			case 17:
				enterOuterAlt(_localctx, 17);
				{
				setState(138); typedef();
				setState(139); match(SEMICOLON);
				}
				break;

			case 18:
				enterOuterAlt(_localctx, 18);
				{
				setState(141); gotoStat();
				setState(142); match(SEMICOLON);
				}
				break;

			case 19:
				enterOuterAlt(_localctx, 19);
				{
				setState(144); jumpLabel();
				}
				break;

			case 20:
				enterOuterAlt(_localctx, 20);
				{
				setState(145); switchStat();
				}
				break;

			case 21:
				enterOuterAlt(_localctx, 21);
				{
				setState(146); match(SEMICOLON);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArithContext extends ParserRuleContext {
		public TerminalNode NEGATE() { return getToken(smallCParser.NEGATE, 0); }
		public LowrelopContext lowrelop() {
			return getRuleContext(LowrelopContext.class,0);
		}
		public ArithContext arith(int i) {
			return getRuleContext(ArithContext.class,i);
		}
		public LogicalandorContext logicalandor() {
			return getRuleContext(LogicalandorContext.class,0);
		}
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public HighopContext highop() {
			return getRuleContext(HighopContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public LowopContext lowop() {
			return getRuleContext(LowopContext.class,0);
		}
		public List<ArithContext> arith() {
			return getRuleContexts(ArithContext.class);
		}
		public HighrelopContext highrelop() {
			return getRuleContext(HighrelopContext.class,0);
		}
		public ArithContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arith; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterArith(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitArith(this);
		}
	}

	public final ArithContext arith() throws RecognitionException {
		return arith(0);
	}

	private ArithContext arith(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ArithContext _localctx = new ArithContext(_ctx, _parentState);
		ArithContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, 4, RULE_arith, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			switch (_input.LA(1)) {
			case NEGATE:
				{
				setState(150); match(NEGATE);
				setState(151); arith(2);
				}
				break;
			case LPAREN:
				{
				setState(152); match(LPAREN);
				setState(153); arith(0);
				setState(154); match(RPAREN);
				}
				break;
			case TRUE:
			case FALSE:
			case INCREMENT:
			case DECREMENT:
			case MINUS:
			case MULT:
			case AMPERSAND:
			case INT:
			case ID:
			case CHAR:
				{
				setState(156); atom();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			_ctx.stop = _input.LT(-1);
			setState(181);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(179);
					switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
					case 1:
						{
						_localctx = new ArithContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arith);
						setState(159);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(160); highop();
						setState(161); arith(9);
						}
						break;

					case 2:
						{
						_localctx = new ArithContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arith);
						setState(163);
						if (!(precpred(_ctx, 7))) throw new FailedPredicateException(this, "precpred(_ctx, 7)");
						setState(164); lowop();
						setState(165); arith(8);
						}
						break;

					case 3:
						{
						_localctx = new ArithContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arith);
						setState(167);
						if (!(precpred(_ctx, 6))) throw new FailedPredicateException(this, "precpred(_ctx, 6)");
						setState(168); highrelop();
						setState(169); arith(7);
						}
						break;

					case 4:
						{
						_localctx = new ArithContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arith);
						setState(171);
						if (!(precpred(_ctx, 5))) throw new FailedPredicateException(this, "precpred(_ctx, 5)");
						setState(172); lowrelop();
						setState(173); arith(6);
						}
						break;

					case 5:
						{
						_localctx = new ArithContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_arith);
						setState(175);
						if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
						setState(176); logicalandor();
						setState(177); arith(5);
						}
						break;
					}
					} 
				}
				setState(183);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class HighopContext extends ParserRuleContext {
		public TerminalNode MULT() { return getToken(smallCParser.MULT, 0); }
		public TerminalNode MOD() { return getToken(smallCParser.MOD, 0); }
		public TerminalNode DIV() { return getToken(smallCParser.DIV, 0); }
		public HighopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_highop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterHighop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitHighop(this);
		}
	}

	public final HighopContext highop() throws RecognitionException {
		HighopContext _localctx = new HighopContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_highop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(184);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << MULT) | (1L << DIV) | (1L << MOD))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LowopContext extends ParserRuleContext {
		public TerminalNode PLUS() { return getToken(smallCParser.PLUS, 0); }
		public TerminalNode MINUS() { return getToken(smallCParser.MINUS, 0); }
		public LowopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lowop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterLowop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitLowop(this);
		}
	}

	public final LowopContext lowop() throws RecognitionException {
		LowopContext _localctx = new LowopContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_lowop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(186);
			_la = _input.LA(1);
			if ( !(_la==PLUS || _la==MINUS) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HighrelopContext extends ParserRuleContext {
		public TerminalNode GEQ() { return getToken(smallCParser.GEQ, 0); }
		public TerminalNode LEQ() { return getToken(smallCParser.LEQ, 0); }
		public TerminalNode LT() { return getToken(smallCParser.LT, 0); }
		public TerminalNode GT() { return getToken(smallCParser.GT, 0); }
		public HighrelopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_highrelop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterHighrelop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitHighrelop(this);
		}
	}

	public final HighrelopContext highrelop() throws RecognitionException {
		HighrelopContext _localctx = new HighrelopContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_highrelop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(188);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << GEQ) | (1L << LEQ) | (1L << GT) | (1L << LT))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LowrelopContext extends ParserRuleContext {
		public TerminalNode NEQ() { return getToken(smallCParser.NEQ, 0); }
		public TerminalNode EQ() { return getToken(smallCParser.EQ, 0); }
		public LowrelopContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lowrelop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterLowrelop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitLowrelop(this);
		}
	}

	public final LowrelopContext lowrelop() throws RecognitionException {
		LowrelopContext _localctx = new LowrelopContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_lowrelop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190);
			_la = _input.LA(1);
			if ( !(_la==EQ || _la==NEQ) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LogicalandorContext extends ParserRuleContext {
		public TerminalNode AND() { return getToken(smallCParser.AND, 0); }
		public TerminalNode OR() { return getToken(smallCParser.OR, 0); }
		public LogicalandorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logicalandor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterLogicalandor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitLogicalandor(this);
		}
	}

	public final LogicalandorContext logicalandor() throws RecognitionException {
		LogicalandorContext _localctx = new LogicalandorContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_logicalandor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(192);
			_la = _input.LA(1);
			if ( !(_la==AND || _la==OR) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomContext extends ParserRuleContext {
		public TerminalNode FALSE() { return getToken(smallCParser.FALSE, 0); }
		public InvertContext invert() {
			return getRuleContext(InvertContext.class,0);
		}
		public TerminalNode TRUE() { return getToken(smallCParser.TRUE, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode AMPERSAND() { return getToken(smallCParser.AMPERSAND, 0); }
		public TerminalNode INT() { return getToken(smallCParser.INT, 0); }
		public PrecrementContext precrement() {
			return getRuleContext(PrecrementContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ArrayElemContext arrayElem() {
			return getRuleContext(ArrayElemContext.class,0);
		}
		public PtrContext ptr() {
			return getRuleContext(PtrContext.class,0);
		}
		public PostcrementContext postcrement() {
			return getRuleContext(PostcrementContext.class,0);
		}
		public TerminalNode CHAR() { return getToken(smallCParser.CHAR, 0); }
		public ArithContext arith() {
			return getRuleContext(ArithContext.class,0);
		}
		public AtomContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atom; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterAtom(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitAtom(this);
		}
	}

	public final AtomContext atom() throws RecognitionException {
		AtomContext _localctx = new AtomContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_atom);
		int _la;
		try {
			setState(230);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(194); invert();
				setState(198);
				switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
				case 1:
					{
					setState(195); match(INT);
					}
					break;

				case 2:
					{
					setState(196); id();
					}
					break;

				case 3:
					{
					setState(197); arith(0);
					}
					break;
				}
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(200); precrement();
				setState(204);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(201); match(INT);
					}
					break;

				case 2:
					{
					setState(202); id();
					}
					break;

				case 3:
					{
					setState(203); arith(0);
					}
					break;
				}
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(208);
				switch (_input.LA(1)) {
				case INT:
					{
					setState(206); match(INT);
					}
					break;
				case ID:
					{
					setState(207); id();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(210); postcrement();
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(211); match(INT);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(212); match(CHAR);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(213); match(TRUE);
				}
				break;

			case 7:
				enterOuterAlt(_localctx, 7);
				{
				setState(214); match(FALSE);
				}
				break;

			case 8:
				enterOuterAlt(_localctx, 8);
				{
				setState(215); functionCall();
				}
				break;

			case 9:
				enterOuterAlt(_localctx, 9);
				{
				setState(217);
				_la = _input.LA(1);
				if (_la==AMPERSAND) {
					{
					setState(216); match(AMPERSAND);
					}
				}

				setState(219); arrayElem();
				}
				break;

			case 10:
				enterOuterAlt(_localctx, 10);
				{
				setState(221);
				_la = _input.LA(1);
				if (_la==AMPERSAND) {
					{
					setState(220); match(AMPERSAND);
					}
				}

				setState(223); id();
				}
				break;

			case 11:
				enterOuterAlt(_localctx, 11);
				{
				setState(224); ptr();
				setState(225); id();
				}
				break;

			case 12:
				enterOuterAlt(_localctx, 12);
				{
				setState(227); ptr();
				setState(228); arith(0);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InvertContext extends ParserRuleContext {
		public TerminalNode MINUS() { return getToken(smallCParser.MINUS, 0); }
		public InvertContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_invert; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterInvert(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitInvert(this);
		}
	}

	public final InvertContext invert() throws RecognitionException {
		InvertContext _localctx = new InvertContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_invert);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(232); match(MINUS);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrecrementContext extends ParserRuleContext {
		public TerminalNode INCREMENT() { return getToken(smallCParser.INCREMENT, 0); }
		public TerminalNode DECREMENT() { return getToken(smallCParser.DECREMENT, 0); }
		public PrecrementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_precrement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterPrecrement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitPrecrement(this);
		}
	}

	public final PrecrementContext precrement() throws RecognitionException {
		PrecrementContext _localctx = new PrecrementContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_precrement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(234);
			_la = _input.LA(1);
			if ( !(_la==INCREMENT || _la==DECREMENT) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostcrementContext extends ParserRuleContext {
		public TerminalNode INCREMENT() { return getToken(smallCParser.INCREMENT, 0); }
		public TerminalNode DECREMENT() { return getToken(smallCParser.DECREMENT, 0); }
		public PostcrementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postcrement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterPostcrement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitPostcrement(this);
		}
	}

	public final PostcrementContext postcrement() throws RecognitionException {
		PostcrementContext _localctx = new PostcrementContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_postcrement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(236);
			_la = _input.LA(1);
			if ( !(_la==INCREMENT || _la==DECREMENT) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IncludeContext extends ParserRuleContext {
		public TerminalNode STDIO() { return getToken(smallCParser.STDIO, 0); }
		public TerminalNode INCLUDE() { return getToken(smallCParser.INCLUDE, 0); }
		public IncludeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_include; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterInclude(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitInclude(this);
		}
	}

	public final IncludeContext include() throws RecognitionException {
		IncludeContext _localctx = new IncludeContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_include);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(238); match(INCLUDE);
			setState(239); match(STDIO);
			stdioInclude = true;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefinitionContext extends ParserRuleContext {
		public TypeContext type;
		public PtrContext ptr;
		public IdContext id;
		public ArrayElemContext arrayElem;
		public TerminalNode ASSIGN() { return getToken(smallCParser.ASSIGN, 0); }
		public ArrayElemContext arrayElem() {
			return getRuleContext(ArrayElemContext.class,0);
		}
		public PtrContext ptr() {
			return getRuleContext(PtrContext.class,0);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ArithContext arith() {
			return getRuleContext(ArithContext.class,0);
		}
		public DefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitDefinition(this);
		}
	}

	public final DefinitionContext definition() throws RecognitionException {
		DefinitionContext _localctx = new DefinitionContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_definition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(246);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				{
				setState(242); ((DefinitionContext)_localctx).type = type();
				setState(244);
				_la = _input.LA(1);
				if (_la==MULT) {
					{
					setState(243); ((DefinitionContext)_localctx).ptr = ptr();
					}
				}

				}
				break;
			}
			setState(250);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(248); ((DefinitionContext)_localctx).id = id();
				}
				break;

			case 2:
				{
				setState(249); ((DefinitionContext)_localctx).arrayElem = arrayElem();
				}
				break;
			}
			setState(252); match(ASSIGN);
			setState(253); arith(0);

							//Add to symbolTable
							Integer thisScope;
							useMaxScopeQueue.add(useMaxScope);
							if (useMaxScope) {
								thisScope = maxScope + 1;
							}
							else {
								 thisScope = this.scopes.peek();
							}
							if ((((DefinitionContext)_localctx).type!=null?_input.getText(((DefinitionContext)_localctx).type.start,((DefinitionContext)_localctx).type.stop):null) != null) {
								Boolean success;
								if ((((DefinitionContext)_localctx).id!=null?_input.getText(((DefinitionContext)_localctx).id.start,((DefinitionContext)_localctx).id.stop):null) != null) {
									if((((DefinitionContext)_localctx).ptr!=null?_input.getText(((DefinitionContext)_localctx).ptr.start,((DefinitionContext)_localctx).ptr.stop):null) != null) {
										success = this.symbolTable.putEntry((((DefinitionContext)_localctx).id!=null?_input.getText(((DefinitionContext)_localctx).id.start,((DefinitionContext)_localctx).id.stop):null), (((DefinitionContext)_localctx).type!=null?_input.getText(((DefinitionContext)_localctx).type.start,((DefinitionContext)_localctx).type.stop):null) + "*", thisScope);
									}
									else {
										success = this.symbolTable.putEntry((((DefinitionContext)_localctx).id!=null?_input.getText(((DefinitionContext)_localctx).id.start,((DefinitionContext)_localctx).id.stop):null), (((DefinitionContext)_localctx).type!=null?_input.getText(((DefinitionContext)_localctx).type.start,((DefinitionContext)_localctx).type.stop):null), thisScope);
									}
								}
								else {
									success = this.symbolTable.putEntry((((DefinitionContext)_localctx).arrayElem!=null?_input.getText(((DefinitionContext)_localctx).arrayElem.start,((DefinitionContext)_localctx).arrayElem.stop):null), (((DefinitionContext)_localctx).type!=null?_input.getText(((DefinitionContext)_localctx).type.start,((DefinitionContext)_localctx).type.stop):null), thisScope);
								}
								if (!success) {
									throw new SemanticANTLRException(this, "This variable already exists");
								}
							}
						
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public DeclTypeContext declType;
		public DeclTypeContext declType() {
			return getRuleContext(DeclTypeContext.class,0);
		}
		public List<PtrContext> ptr() {
			return getRuleContexts(PtrContext.class);
		}
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public ArrayDeclContext arrayDecl(int i) {
			return getRuleContext(ArrayDeclContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(smallCParser.COMMA); }
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public PtrContext ptr(int i) {
			return getRuleContext(PtrContext.class,i);
		}
		public List<ArrayDeclContext> arrayDecl() {
			return getRuleContexts(ArrayDeclContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(smallCParser.COMMA, i);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitDeclaration(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_declaration);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(256); ((DeclarationContext)_localctx).declType = declType();
			setState(258);
			_la = _input.LA(1);
			if (_la==MULT) {
				{
				setState(257); ptr();
				}
			}

			setState(262);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				{
				setState(260); id();
				}
				break;

			case 2:
				{
				setState(261); arrayDecl();
				}
				break;
			}
			setState(274);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(264); match(COMMA);
					setState(266);
					_la = _input.LA(1);
					if (_la==MULT) {
						{
						setState(265); ptr();
						}
					}

					setState(270);
					switch ( getInterpreter().adaptivePredict(_input,17,_ctx) ) {
					case 1:
						{
						setState(268); id();
						}
						break;

					case 2:
						{
						setState(269); arrayDecl();
						}
						break;
					}
					}
					} 
				}
				setState(276);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}

							//Add to symbolTable
							Integer thisScope;
							useMaxScopeQueue.add(useMaxScope);
							if (useMaxScope) {
								thisScope = maxScope + 1;
							}
							else {
								thisScope = this.scopes.peek();
							}
							Boolean isPtr = false;
							int i = 0;
							//Add all declarations to symboltable
							while(true && !(_localctx.getParent().getParent() instanceof FunctionDeclContext)) {
								i++;
								Boolean success;
								if(_localctx.getChild(i).getText().equals("*")) {
									isPtr = true;
									//is pointer
									i++;
									success = this.symbolTable.putEntry(_localctx.getChild(i).getText(), (((DeclarationContext)_localctx).declType!=null?_input.getText(((DeclarationContext)_localctx).declType.start,((DeclarationContext)_localctx).declType.stop):null) + "*", thisScope);
								}
								else {
									if(_localctx.getChild(i).getClass().toString().substring(26).equals("ArrayDeclContext")) {
										//is array
										//Check for multidimensional arrays
										int size = 1;
										int index = 2;
										List<Integer> dimensions = new ArrayList<Integer>();
										while (_localctx.getChild(i).getChild(index) != null) {
											size *= Integer.parseInt(_localctx.getChild(i).getChild(index).getText());
											dimensions.add(Integer.parseInt(_localctx.getChild(i).getChild(index).getText()));
											index += 3; // skip over the brackets
										}
										success = this.symbolTable.putEntry(_localctx.getChild(i).getChild(0).getText(), (((DeclarationContext)_localctx).declType!=null?_input.getText(((DeclarationContext)_localctx).declType.start,((DeclarationContext)_localctx).declType.stop):null), thisScope, 
											size, dimensions);
									}
									else {
										success = this.symbolTable.putEntry(_localctx.getChild(i).getText(), (((DeclarationContext)_localctx).declType!=null?_input.getText(((DeclarationContext)_localctx).declType.start,((DeclarationContext)_localctx).declType.stop):null), thisScope);
									}
								}
								if (!success) {
									throw new SemanticANTLRException(this, "This variable already exists");
								}
								i++;
								if (!(_localctx.getChild(i) != null && _localctx.getChild(i).getText().equals(","))) {
									break;
								}
							}
							//Will be set if this is a child of a FunctionArgsNode
							if (setFunctionArgs) {
								if (isPtr) {
									functionArgs.add(_localctx.getChild(0).getText() + "*");
								}
								else {
									functionArgs.add(_localctx.getChild(0).getText());
								}
							}
											
						
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(smallCParser.CONST, 0); }
		public TerminalNode TYPECHAR() { return getToken(smallCParser.TYPECHAR, 0); }
		public TerminalNode TYPEINT() { return getToken(smallCParser.TYPEINT, 0); }
		public TypedefTypeContext typedefType() {
			return getRuleContext(TypedefTypeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_type);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(280);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(279); match(CONST);
				}
			}

			setState(285);
			switch (_input.LA(1)) {
			case TYPECHAR:
				{
				setState(282); match(TYPECHAR);
				}
				break;
			case TYPEINT:
				{
				setState(283); match(TYPEINT);
				}
				break;
			case ID:
				{
				setState(284); typedefType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclTypeContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(smallCParser.CONST, 0); }
		public TerminalNode TYPECHAR() { return getToken(smallCParser.TYPECHAR, 0); }
		public TerminalNode TYPEINT() { return getToken(smallCParser.TYPEINT, 0); }
		public TypedefTypeContext typedefType() {
			return getRuleContext(TypedefTypeContext.class,0);
		}
		public DeclTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterDeclType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitDeclType(this);
		}
	}

	public final DeclTypeContext declType() throws RecognitionException {
		DeclTypeContext _localctx = new DeclTypeContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_declType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(288);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(287); match(CONST);
				}
			}

			setState(293);
			switch (_input.LA(1)) {
			case TYPECHAR:
				{
				setState(290); match(TYPECHAR);
				}
				break;
			case TYPEINT:
				{
				setState(291); match(TYPEINT);
				}
				break;
			case ID:
				{
				setState(292); typedefType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PtrContext extends ParserRuleContext {
		public TerminalNode MULT() { return getToken(smallCParser.MULT, 0); }
		public PtrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ptr; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterPtr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitPtr(this);
		}
	}

	public final PtrContext ptr() throws RecognitionException {
		PtrContext _localctx = new PtrContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_ptr);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(295); match(MULT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionTypeContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(smallCParser.CONST, 0); }
		public TerminalNode TYPEVOID() { return getToken(smallCParser.TYPEVOID, 0); }
		public TerminalNode TYPECHAR() { return getToken(smallCParser.TYPECHAR, 0); }
		public PtrContext ptr() {
			return getRuleContext(PtrContext.class,0);
		}
		public TerminalNode TYPEINT() { return getToken(smallCParser.TYPEINT, 0); }
		public FunctionTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterFunctionType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitFunctionType(this);
		}
	}

	public final FunctionTypeContext functionType() throws RecognitionException {
		FunctionTypeContext _localctx = new FunctionTypeContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_functionType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(298);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(297); match(CONST);
				}
			}

			setState(300);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TYPEINT) | (1L << TYPECHAR) | (1L << TYPEVOID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(302);
			_la = _input.LA(1);
			if (_la==MULT) {
				{
				setState(301); ptr();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgTypeContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(smallCParser.CONST, 0); }
		public TerminalNode TYPEVOID() { return getToken(smallCParser.TYPEVOID, 0); }
		public TerminalNode TYPECHAR() { return getToken(smallCParser.TYPECHAR, 0); }
		public PtrContext ptr() {
			return getRuleContext(PtrContext.class,0);
		}
		public TerminalNode TYPEINT() { return getToken(smallCParser.TYPEINT, 0); }
		public ArgTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterArgType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitArgType(this);
		}
	}

	public final ArgTypeContext argType() throws RecognitionException {
		ArgTypeContext _localctx = new ArgTypeContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_argType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(305);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(304); match(CONST);
				}
			}

			setState(307);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TYPEINT) | (1L << TYPECHAR) | (1L << TYPEVOID))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(309);
			_la = _input.LA(1);
			if (_la==MULT) {
				{
				setState(308); ptr();
				}
			}

			if (setFunctionArgs) {functionArgs.add(_ctx.getText());}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedefTypeContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(smallCParser.ID, 0); }
		public TypedefTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedefType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterTypedefType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitTypedefType(this);
		}
	}

	public final TypedefTypeContext typedefType() throws RecognitionException {
		TypedefTypeContext _localctx = new TypedefTypeContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_typedefType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(313); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(smallCParser.ID, 0); }
		public IdContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_id; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitId(this);
		}
	}

	public final IdContext id() throws RecognitionException {
		IdContext _localctx = new IdContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_id);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(315); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AmpersandContext extends ParserRuleContext {
		public TerminalNode AMPERSAND() { return getToken(smallCParser.AMPERSAND, 0); }
		public AmpersandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ampersand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterAmpersand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitAmpersand(this);
		}
	}

	public final AmpersandContext ampersand() throws RecognitionException {
		AmpersandContext _localctx = new AmpersandContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_ampersand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(317); match(AMPERSAND);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionNameContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(smallCParser.ID, 0); }
		public FunctionNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterFunctionName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitFunctionName(this);
		}
	}

	public final FunctionNameContext functionName() throws RecognitionException {
		FunctionNameContext _localctx = new FunctionNameContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_functionName);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(319); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompoundContext extends ParserRuleContext {
		public TerminalNode RBRACE() { return getToken(smallCParser.RBRACE, 0); }
		public TerminalNode LBRACE() { return getToken(smallCParser.LBRACE, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public CompoundContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compound; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterCompound(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitCompound(this);
		}
	}

	public final CompoundContext compound() throws RecognitionException {
		CompoundContext _localctx = new CompoundContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_compound);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			this.scopes.push(++maxScope);
			setState(322); match(LBRACE);
			setState(326);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__0) | (1L << WHILE) | (1L << IF) | (1L << FOR) | (1L << BREAK) | (1L << CONTINUE) | (1L << INCLUDE) | (1L << TYPEINT) | (1L << TYPECHAR) | (1L << TYPEVOID) | (1L << CONST) | (1L << TRUE) | (1L << FALSE) | (1L << RETURN) | (1L << TYPEDEF) | (1L << GOTO) | (1L << INCREMENT) | (1L << DECREMENT) | (1L << MINUS) | (1L << MULT) | (1L << LPAREN) | (1L << LBRACE) | (1L << SEMICOLON) | (1L << AMPERSAND) | (1L << NEGATE) | (1L << SWITCH) | (1L << INT) | (1L << ID) | (1L << LABEL) | (1L << CHAR))) != 0)) {
				{
				{
				setState(323); stat();
				}
				}
				setState(328);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(329); match(RBRACE);
			this.scopes.pop();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CompoundNoScopeContext extends ParserRuleContext {
		public TerminalNode RBRACE() { return getToken(smallCParser.RBRACE, 0); }
		public TerminalNode LBRACE() { return getToken(smallCParser.LBRACE, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public CompoundNoScopeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compoundNoScope; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterCompoundNoScope(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitCompoundNoScope(this);
		}
	}

	public final CompoundNoScopeContext compoundNoScope() throws RecognitionException {
		CompoundNoScopeContext _localctx = new CompoundNoScopeContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_compoundNoScope);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(332); match(LBRACE);
			setState(336);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__0) | (1L << WHILE) | (1L << IF) | (1L << FOR) | (1L << BREAK) | (1L << CONTINUE) | (1L << INCLUDE) | (1L << TYPEINT) | (1L << TYPECHAR) | (1L << TYPEVOID) | (1L << CONST) | (1L << TRUE) | (1L << FALSE) | (1L << RETURN) | (1L << TYPEDEF) | (1L << GOTO) | (1L << INCREMENT) | (1L << DECREMENT) | (1L << MINUS) | (1L << MULT) | (1L << LPAREN) | (1L << LBRACE) | (1L << SEMICOLON) | (1L << AMPERSAND) | (1L << NEGATE) | (1L << SWITCH) | (1L << INT) | (1L << ID) | (1L << LABEL) | (1L << CHAR))) != 0)) {
				{
				{
				setState(333); stat();
				}
				}
				setState(338);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(339); match(RBRACE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseSwitchContext extends ParserRuleContext {
		public TerminalNode DEFAULTCASE() { return getToken(smallCParser.DEFAULTCASE, 0); }
		public TerminalNode CASE() { return getToken(smallCParser.CASE, 0); }
		public TerminalNode COLON() { return getToken(smallCParser.COLON, 0); }
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public BreakStatContext breakStat() {
			return getRuleContext(BreakStatContext.class,0);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public AtomContext atom() {
			return getRuleContext(AtomContext.class,0);
		}
		public CaseSwitchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseSwitch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterCaseSwitch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitCaseSwitch(this);
		}
	}

	public final CaseSwitchContext caseSwitch() throws RecognitionException {
		CaseSwitchContext _localctx = new CaseSwitchContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_caseSwitch);
		int _la;
		try {
			int _alt;
			setState(360);
			switch (_input.LA(1)) {
			case DEFAULTCASE:
				enterOuterAlt(_localctx, 1);
				{
				setState(341); match(DEFAULTCASE);
				setState(345);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__0) | (1L << WHILE) | (1L << IF) | (1L << FOR) | (1L << BREAK) | (1L << CONTINUE) | (1L << INCLUDE) | (1L << TYPEINT) | (1L << TYPECHAR) | (1L << TYPEVOID) | (1L << CONST) | (1L << TRUE) | (1L << FALSE) | (1L << RETURN) | (1L << TYPEDEF) | (1L << GOTO) | (1L << INCREMENT) | (1L << DECREMENT) | (1L << MINUS) | (1L << MULT) | (1L << LPAREN) | (1L << LBRACE) | (1L << SEMICOLON) | (1L << AMPERSAND) | (1L << NEGATE) | (1L << SWITCH) | (1L << INT) | (1L << ID) | (1L << LABEL) | (1L << CHAR))) != 0)) {
					{
					{
					setState(342); stat();
					}
					}
					setState(347);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
				break;
			case CASE:
				enterOuterAlt(_localctx, 2);
				{
				setState(348); match(CASE);
				setState(349); atom();
				setState(350); match(COLON);
				setState(354);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
					if ( _alt==1 ) {
						{
						{
						setState(351); stat();
						}
						} 
					}
					setState(356);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
				}
				setState(358);
				_la = _input.LA(1);
				if (_la==BREAK) {
					{
					setState(357); breakStat();
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SwitchStatContext extends ParserRuleContext {
		public List<CaseSwitchContext> caseSwitch() {
			return getRuleContexts(CaseSwitchContext.class);
		}
		public TerminalNode RBRACE() { return getToken(smallCParser.RBRACE, 0); }
		public TerminalNode SWITCH() { return getToken(smallCParser.SWITCH, 0); }
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public TerminalNode LBRACE() { return getToken(smallCParser.LBRACE, 0); }
		public CaseSwitchContext caseSwitch(int i) {
			return getRuleContext(CaseSwitchContext.class,i);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public SwitchStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_switchStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterSwitchStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitSwitchStat(this);
		}
	}

	public final SwitchStatContext switchStat() throws RecognitionException {
		SwitchStatContext _localctx = new SwitchStatContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_switchStat);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(362); match(SWITCH);
			setState(363); match(LPAREN);
			setState(364); id();
			setState(365); match(RPAREN);
			this.scopes.push(++maxScope);
			setState(367); match(LBRACE);
			setState(371);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==CASE || _la==DEFAULTCASE) {
				{
				{
				setState(368); caseSwitch();
				}
				}
				setState(373);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(374); match(RBRACE);
			this.scopes.pop();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(smallCParser.IF, 0); }
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public ScopedBodyContext scopedBody() {
			return getRuleContext(ScopedBodyContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public ElseStatContext elseStat() {
			return getRuleContext(ElseStatContext.class,0);
		}
		public ArithContext arith() {
			return getRuleContext(ArithContext.class,0);
		}
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public IfStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterIfStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitIfStat(this);
		}
	}

	public final IfStatContext ifStat() throws RecognitionException {
		IfStatContext _localctx = new IfStatContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_ifStat);
		try {
			setState(400);
			switch ( getInterpreter().adaptivePredict(_input,36,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				useMaxScope = true;
				setState(378); match(IF);
				setState(379); match(LPAREN);
				setState(382);
				switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
				case 1:
					{
					setState(380); arith(0);
					}
					break;

				case 2:
					{
					setState(381); definition();
					}
					break;
				}
				setState(384); match(RPAREN);
				useMaxScope = false;
				setState(386); scopedBody();
				setState(387); elseStat();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				useMaxScope = true;
				setState(390); match(IF);
				setState(391); match(LPAREN);
				setState(394);
				switch ( getInterpreter().adaptivePredict(_input,35,_ctx) ) {
				case 1:
					{
					setState(392); arith(0);
					}
					break;

				case 2:
					{
					setState(393); definition();
					}
					break;
				}
				setState(396); match(RPAREN);
				useMaxScope = false;
				setState(398); scopedBody();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElseStatContext extends ParserRuleContext {
		public TerminalNode ELSE() { return getToken(smallCParser.ELSE, 0); }
		public ScopedBodyContext scopedBody() {
			return getRuleContext(ScopedBodyContext.class,0);
		}
		public ElseStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_elseStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterElseStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitElseStat(this);
		}
	}

	public final ElseStatContext elseStat() throws RecognitionException {
		ElseStatContext _localctx = new ElseStatContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_elseStat);
		try {
			setState(405);
			switch ( getInterpreter().adaptivePredict(_input,37,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(402); match(ELSE);
				setState(403); scopedBody();
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStatContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public ScopedBodyContext scopedBody() {
			return getRuleContext(ScopedBodyContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public TerminalNode WHILE() { return getToken(smallCParser.WHILE, 0); }
		public ArithContext arith() {
			return getRuleContext(ArithContext.class,0);
		}
		public DefinitionContext definition() {
			return getRuleContext(DefinitionContext.class,0);
		}
		public WhileStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterWhileStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitWhileStat(this);
		}
	}

	public final WhileStatContext whileStat() throws RecognitionException {
		WhileStatContext _localctx = new WhileStatContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_whileStat);
		try {
			enterOuterAlt(_localctx, 1);
			{
			useMaxScope = true;
			setState(408); match(WHILE);
			setState(409); match(LPAREN);
			setState(412);
			switch ( getInterpreter().adaptivePredict(_input,38,_ctx) ) {
			case 1:
				{
				setState(410); arith(0);
				}
				break;

			case 2:
				{
				setState(411); definition();
				}
				break;
			}
			setState(414); match(RPAREN);
			useMaxScope = false;
			setState(416); scopedBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStatContext extends ParserRuleContext {
		public List<TerminalNode> SEMICOLON() { return getTokens(smallCParser.SEMICOLON); }
		public ArithContext arith(int i) {
			return getRuleContext(ArithContext.class,i);
		}
		public List<EmptyContext> empty() {
			return getRuleContexts(EmptyContext.class);
		}
		public TerminalNode FOR() { return getToken(smallCParser.FOR, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public EmptyContext empty(int i) {
			return getRuleContext(EmptyContext.class,i);
		}
		public ScopedBodyContext scopedBody() {
			return getRuleContext(ScopedBodyContext.class,0);
		}
		public TerminalNode SEMICOLON(int i) {
			return getToken(smallCParser.SEMICOLON, i);
		}
		public DefinitionContext definition(int i) {
			return getRuleContext(DefinitionContext.class,i);
		}
		public List<DefinitionContext> definition() {
			return getRuleContexts(DefinitionContext.class);
		}
		public List<ArithContext> arith() {
			return getRuleContexts(ArithContext.class);
		}
		public ForStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterForStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitForStat(this);
		}
	}

	public final ForStatContext forStat() throws RecognitionException {
		ForStatContext _localctx = new ForStatContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_forStat);
		try {
			enterOuterAlt(_localctx, 1);
			{
			useMaxScope = true;
			setState(419); match(FOR);
			setState(420); match(LPAREN);
			setState(424);
			switch ( getInterpreter().adaptivePredict(_input,39,_ctx) ) {
			case 1:
				{
				setState(421); definition();
				}
				break;

			case 2:
				{
				setState(422); id();
				}
				break;

			case 3:
				{
				setState(423); empty();
				}
				break;
			}
			setState(426); match(SEMICOLON);
			setState(429);
			switch (_input.LA(1)) {
			case TRUE:
			case FALSE:
			case INCREMENT:
			case DECREMENT:
			case MINUS:
			case MULT:
			case LPAREN:
			case AMPERSAND:
			case NEGATE:
			case INT:
			case ID:
			case CHAR:
				{
				setState(427); arith(0);
				}
				break;
			case SEMICOLON:
				{
				setState(428); empty();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(431); match(SEMICOLON);
			setState(435);
			switch ( getInterpreter().adaptivePredict(_input,41,_ctx) ) {
			case 1:
				{
				setState(432); arith(0);
				}
				break;

			case 2:
				{
				setState(433); definition();
				}
				break;

			case 3:
				{
				setState(434); empty();
				}
				break;
			}
			setState(437); match(RPAREN);
			useMaxScope = false;
			setState(439); scopedBody();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BreakStatContext extends ParserRuleContext {
		public TerminalNode BREAK() { return getToken(smallCParser.BREAK, 0); }
		public BreakStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_breakStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterBreakStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitBreakStat(this);
		}
	}

	public final BreakStatContext breakStat() throws RecognitionException {
		BreakStatContext _localctx = new BreakStatContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_breakStat);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(441); match(BREAK);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ContinueStatContext extends ParserRuleContext {
		public TerminalNode CONTINUE() { return getToken(smallCParser.CONTINUE, 0); }
		public ContinueStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_continueStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterContinueStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitContinueStat(this);
		}
	}

	public final ContinueStatContext continueStat() throws RecognitionException {
		ContinueStatContext _localctx = new ContinueStatContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_continueStat);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(443); match(CONTINUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefContext extends ParserRuleContext {
		public FunctionTypeContext functionType;
		public FunctionNameContext functionName;
		public FunctionArgumentsContext functionArguments;
		public FunctionArgumentsContext functionArguments() {
			return getRuleContext(FunctionArgumentsContext.class,0);
		}
		public FunctionNameContext functionName() {
			return getRuleContext(FunctionNameContext.class,0);
		}
		public FunctionTypeContext functionType() {
			return getRuleContext(FunctionTypeContext.class,0);
		}
		public CompoundContext compound() {
			return getRuleContext(CompoundContext.class,0);
		}
		public FunctionDefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterFunctionDef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitFunctionDef(this);
		}
	}

	public final FunctionDefContext functionDef() throws RecognitionException {
		FunctionDefContext _localctx = new FunctionDefContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_functionDef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(445); ((FunctionDefContext)_localctx).functionType = functionType();
			setState(446); ((FunctionDefContext)_localctx).functionName = functionName();
			setState(447); ((FunctionDefContext)_localctx).functionArguments = functionArguments();
			setState(448); compound();

			 			 	if (scopes.size() != 1) {
			 			 		//Not at global scope, no defs here
			 			 		throw new SemanticANTLRException(this, "Functions can only be defined in the global scope");
			 			 	}
			 			 	ArrayList functionArgsCopy = (ArrayList) functionArgs.clone();
			 			 	Boolean success = this.symbolTable.putEntryFunction((((FunctionDefContext)_localctx).functionName!=null?_input.getText(((FunctionDefContext)_localctx).functionName.start,((FunctionDefContext)_localctx).functionName.stop):null), (((FunctionDefContext)_localctx).functionType!=null?_input.getText(((FunctionDefContext)_localctx).functionType.start,((FunctionDefContext)_localctx).functionType.stop):null) + " func" + (((FunctionDefContext)_localctx).functionArguments!=null?_input.getText(((FunctionDefContext)_localctx).functionArguments.start,((FunctionDefContext)_localctx).functionArguments.stop):null),
			 			 	(((FunctionDefContext)_localctx).functionType!=null?_input.getText(((FunctionDefContext)_localctx).functionType.start,((FunctionDefContext)_localctx).functionType.stop):null), functionArgsCopy, false);
			 			 	if (!success) {throw new SemanticANTLRException(this, "This function already exists");}
			 			 	functionArgs = new ArrayList<String>();
			 			  
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionArgumentsContext extends ParserRuleContext {
		public List<TerminalNode> TYPEVOID() { return getTokens(smallCParser.TYPEVOID); }
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(smallCParser.COMMA); }
		public TerminalNode TYPEVOID(int i) {
			return getToken(smallCParser.TYPEVOID, i);
		}
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public List<ArgTypeContext> argType() {
			return getRuleContexts(ArgTypeContext.class);
		}
		public ArgTypeContext argType(int i) {
			return getRuleContext(ArgTypeContext.class,i);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(smallCParser.COMMA, i);
		}
		public FunctionArgumentsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionArguments; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterFunctionArguments(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitFunctionArguments(this);
		}
	}

	public final FunctionArgumentsContext functionArguments() throws RecognitionException {
		FunctionArgumentsContext _localctx = new FunctionArgumentsContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_functionArguments);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			useMaxScope = true;
								setFunctionArgs = true;
			setState(452); match(LPAREN);
			setState(469);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TYPEINT) | (1L << TYPECHAR) | (1L << TYPEVOID) | (1L << CONST) | (1L << ID))) != 0)) {
				{
				setState(456);
				switch ( getInterpreter().adaptivePredict(_input,42,_ctx) ) {
				case 1:
					{
					setState(453); match(TYPEVOID);
					}
					break;

				case 2:
					{
					setState(454); declaration();
					}
					break;

				case 3:
					{
					setState(455); argType();
					}
					break;
				}
				setState(466);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(458); match(COMMA);
					setState(462);
					switch ( getInterpreter().adaptivePredict(_input,43,_ctx) ) {
					case 1:
						{
						setState(459); match(TYPEVOID);
						}
						break;

					case 2:
						{
						setState(460); declaration();
						}
						break;

					case 3:
						{
						setState(461); argType();
						}
						break;
					}
					}
					}
					setState(468);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(471); match(RPAREN);
			useMaxScope = false;
							   	setFunctionArgs = false;
							   
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDeclContext extends ParserRuleContext {
		public FunctionTypeContext functionType;
		public FunctionNameContext functionName;
		public FunctionArgumentsContext functionArguments;
		public FunctionArgumentsContext functionArguments() {
			return getRuleContext(FunctionArgumentsContext.class,0);
		}
		public FunctionNameContext functionName() {
			return getRuleContext(FunctionNameContext.class,0);
		}
		public FunctionTypeContext functionType() {
			return getRuleContext(FunctionTypeContext.class,0);
		}
		public FunctionDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterFunctionDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitFunctionDecl(this);
		}
	}

	public final FunctionDeclContext functionDecl() throws RecognitionException {
		FunctionDeclContext _localctx = new FunctionDeclContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_functionDecl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(474); ((FunctionDeclContext)_localctx).functionType = functionType();
			setState(475); ((FunctionDeclContext)_localctx).functionName = functionName();
			setState(476); ((FunctionDeclContext)_localctx).functionArguments = functionArguments();

							if (scopes.size() != 1) {
			 			 		//Not at global scope, no defs here
			 			 		throw new SemanticANTLRException(this, "Functions can only be declared in the global scope");
			 			 	}
			 			 	//Copy strictly necessary?
			 			 	ArrayList functionArgsCopy = (ArrayList) functionArgs.clone();
			 			 	Boolean success = this.symbolTable.putEntryFunction((((FunctionDeclContext)_localctx).functionName!=null?_input.getText(((FunctionDeclContext)_localctx).functionName.start,((FunctionDeclContext)_localctx).functionName.stop):null), (((FunctionDeclContext)_localctx).functionType!=null?_input.getText(((FunctionDeclContext)_localctx).functionType.start,((FunctionDeclContext)_localctx).functionType.stop):null) + " func" + (((FunctionDeclContext)_localctx).functionArguments!=null?_input.getText(((FunctionDeclContext)_localctx).functionArguments.start,((FunctionDeclContext)_localctx).functionArguments.stop):null),
			 			 	(((FunctionDeclContext)_localctx).functionType!=null?_input.getText(((FunctionDeclContext)_localctx).functionType.start,((FunctionDeclContext)_localctx).functionType.stop):null), functionArgsCopy, true);
			 			 	if (!success) {throw new SemanticANTLRException(this, "This function is already defined");}
			 			 	functionArgs = new ArrayList<String>();
						
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReturnStatContext extends ParserRuleContext {
		public TerminalNode RETURN() { return getToken(smallCParser.RETURN, 0); }
		public ArithContext arith() {
			return getRuleContext(ArithContext.class,0);
		}
		public ReturnStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_returnStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterReturnStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitReturnStat(this);
		}
	}

	public final ReturnStatContext returnStat() throws RecognitionException {
		ReturnStatContext _localctx = new ReturnStatContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_returnStat);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(479); match(RETURN);
			setState(481);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TRUE) | (1L << FALSE) | (1L << INCREMENT) | (1L << DECREMENT) | (1L << MINUS) | (1L << MULT) | (1L << LPAREN) | (1L << AMPERSAND) | (1L << NEGATE) | (1L << INT) | (1L << ID) | (1L << CHAR))) != 0)) {
				{
				setState(480); arith(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrintfCallContext extends ParserRuleContext {
		public ArithContext arith(int i) {
			return getRuleContext(ArithContext.class,i);
		}
		public StringContext string(int i) {
			return getRuleContext(StringContext.class,i);
		}
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public List<StringContext> string() {
			return getRuleContexts(StringContext.class);
		}
		public List<TerminalNode> COMMA() { return getTokens(smallCParser.COMMA); }
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public TerminalNode STRING() { return getToken(smallCParser.STRING, 0); }
		public TerminalNode EMPTYSTRING() { return getToken(smallCParser.EMPTYSTRING, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(smallCParser.COMMA, i);
		}
		public List<ArithContext> arith() {
			return getRuleContexts(ArithContext.class);
		}
		public PrintfCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_printfCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterPrintfCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitPrintfCall(this);
		}
	}

	public final PrintfCallContext printfCall() throws RecognitionException {
		PrintfCallContext _localctx = new PrintfCallContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_printfCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(483); match(T__1);
			setState(484); match(LPAREN);
			setState(485);
			_la = _input.LA(1);
			if ( !(_la==EMPTYSTRING || _la==STRING) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(493);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(486); match(COMMA);
				setState(489);
				switch (_input.LA(1)) {
				case TRUE:
				case FALSE:
				case INCREMENT:
				case DECREMENT:
				case MINUS:
				case MULT:
				case LPAREN:
				case AMPERSAND:
				case NEGATE:
				case INT:
				case ID:
				case CHAR:
					{
					setState(487); arith(0);
					}
					break;
				case STRING:
					{
					setState(488); string();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				}
				setState(495);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(496); match(RPAREN);
			if(!stdioInclude) {throw new NoViableAltException(this);}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScanfCallContext extends ParserRuleContext {
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public IdContext id(int i) {
			return getRuleContext(IdContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(smallCParser.COMMA); }
		public List<IdContext> id() {
			return getRuleContexts(IdContext.class);
		}
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public TerminalNode STRING() { return getToken(smallCParser.STRING, 0); }
		public List<AmpersandContext> ampersand() {
			return getRuleContexts(AmpersandContext.class);
		}
		public TerminalNode EMPTYSTRING() { return getToken(smallCParser.EMPTYSTRING, 0); }
		public AmpersandContext ampersand(int i) {
			return getRuleContext(AmpersandContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(smallCParser.COMMA, i);
		}
		public ScanfCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scanfCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterScanfCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitScanfCall(this);
		}
	}

	public final ScanfCallContext scanfCall() throws RecognitionException {
		ScanfCallContext _localctx = new ScanfCallContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_scanfCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(499); match(T__0);
			setState(500); match(LPAREN);
			setState(501);
			_la = _input.LA(1);
			if ( !(_la==EMPTYSTRING || _la==STRING) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(509);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==COMMA) {
				{
				{
				setState(502); match(COMMA);
				setState(504);
				_la = _input.LA(1);
				if (_la==AMPERSAND) {
					{
					setState(503); ampersand();
					}
				}

				setState(506); id();
				}
				}
				setState(511);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(512); match(RPAREN);
			if(!stdioInclude) {throw new NoViableAltException(this);}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public FunctionNameContext functionName() {
			return getRuleContext(FunctionNameContext.class,0);
		}
		public ArithContext arith(int i) {
			return getRuleContext(ArithContext.class,i);
		}
		public TerminalNode LPAREN() { return getToken(smallCParser.LPAREN, 0); }
		public List<TerminalNode> COMMA() { return getTokens(smallCParser.COMMA); }
		public TerminalNode RPAREN() { return getToken(smallCParser.RPAREN, 0); }
		public TerminalNode COMMA(int i) {
			return getToken(smallCParser.COMMA, i);
		}
		public List<ArithContext> arith() {
			return getRuleContexts(ArithContext.class);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitFunctionCall(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_functionCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(515); functionName();
			setState(516); match(LPAREN);
			setState(525);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << TRUE) | (1L << FALSE) | (1L << INCREMENT) | (1L << DECREMENT) | (1L << MINUS) | (1L << MULT) | (1L << LPAREN) | (1L << AMPERSAND) | (1L << NEGATE) | (1L << INT) | (1L << ID) | (1L << CHAR))) != 0)) {
				{
				setState(517); arith(0);
				setState(522);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(518); match(COMMA);
					setState(519); arith(0);
					}
					}
					setState(524);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(527); match(RPAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayDeclContext extends ParserRuleContext {
		public List<TerminalNode> RBRACKET() { return getTokens(smallCParser.RBRACKET); }
		public TerminalNode LBRACKET(int i) {
			return getToken(smallCParser.LBRACKET, i);
		}
		public ArithContext arith(int i) {
			return getRuleContext(ArithContext.class,i);
		}
		public TerminalNode RBRACKET(int i) {
			return getToken(smallCParser.RBRACKET, i);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<TerminalNode> LBRACKET() { return getTokens(smallCParser.LBRACKET); }
		public List<ArithContext> arith() {
			return getRuleContexts(ArithContext.class);
		}
		public ArrayDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterArrayDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitArrayDecl(this);
		}
	}

	public final ArrayDeclContext arrayDecl() throws RecognitionException {
		ArrayDeclContext _localctx = new ArrayDeclContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_arrayDecl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(529); id();
			setState(534); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(530); match(LBRACKET);
				setState(531); arith(0);
				setState(532); match(RBRACKET);
				}
				}
				setState(536); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==LBRACKET );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayElemContext extends ParserRuleContext {
		public List<TerminalNode> RBRACKET() { return getTokens(smallCParser.RBRACKET); }
		public TerminalNode LBRACKET(int i) {
			return getToken(smallCParser.LBRACKET, i);
		}
		public ArithContext arith(int i) {
			return getRuleContext(ArithContext.class,i);
		}
		public TerminalNode RBRACKET(int i) {
			return getToken(smallCParser.RBRACKET, i);
		}
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public List<TerminalNode> LBRACKET() { return getTokens(smallCParser.LBRACKET); }
		public List<ArithContext> arith() {
			return getRuleContexts(ArithContext.class);
		}
		public ArrayElemContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayElem; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterArrayElem(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitArrayElem(this);
		}
	}

	public final ArrayElemContext arrayElem() throws RecognitionException {
		ArrayElemContext _localctx = new ArrayElemContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_arrayElem);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(538); id();
			setState(543); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(539); match(LBRACKET);
					setState(540); arith(0);
					setState(541); match(RBRACKET);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(545); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,54,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypedefContext extends ParserRuleContext {
		public TerminalNode TYPEDEF() { return getToken(smallCParser.TYPEDEF, 0); }
		public IdContext id() {
			return getRuleContext(IdContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypedefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typedef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterTypedef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitTypedef(this);
		}
	}

	public final TypedefContext typedef() throws RecognitionException {
		TypedefContext _localctx = new TypedefContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_typedef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(547); match(TYPEDEF);
			setState(548); type();
			setState(549); id();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScopedBodyContext extends ParserRuleContext {
		public CompoundNoScopeContext compoundNoScope() {
			return getRuleContext(CompoundNoScopeContext.class,0);
		}
		public StatContext stat() {
			return getRuleContext(StatContext.class,0);
		}
		public ScopedBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scopedBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterScopedBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitScopedBody(this);
		}
	}

	public final ScopedBodyContext scopedBody() throws RecognitionException {
		ScopedBodyContext _localctx = new ScopedBodyContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_scopedBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			this.scopes.push(++maxScope);
			setState(554);
			switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
			case 1:
				{
				setState(552); compoundNoScope();
				}
				break;

			case 2:
				{
				setState(553); stat();
				}
				break;
			}
			this.scopes.pop();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GotoStatContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(smallCParser.ID, 0); }
		public TerminalNode GOTO() { return getToken(smallCParser.GOTO, 0); }
		public GotoStatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gotoStat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterGotoStat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitGotoStat(this);
		}
	}

	public final GotoStatContext gotoStat() throws RecognitionException {
		GotoStatContext _localctx = new GotoStatContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_gotoStat);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(558); match(GOTO);
			setState(559); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class JumpLabelContext extends ParserRuleContext {
		public TerminalNode LABEL() { return getToken(smallCParser.LABEL, 0); }
		public JumpLabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_jumpLabel; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterJumpLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitJumpLabel(this);
		}
	}

	public final JumpLabelContext jumpLabel() throws RecognitionException {
		JumpLabelContext _localctx = new JumpLabelContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_jumpLabel);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(561); match(LABEL);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public TerminalNode STRING() { return getToken(smallCParser.STRING, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_string);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(563); match(STRING);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EmptyContext extends ParserRuleContext {
		public EmptyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_empty; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).enterEmpty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof smallCListener ) ((smallCListener)listener).exitEmpty(this);
		}
	}

	public final EmptyContext empty() throws RecognitionException {
		EmptyContext _localctx = new EmptyContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_empty);
		try {
			enterOuterAlt(_localctx, 1);
			{
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2: return arith_sempred((ArithContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean arith_sempred(ArithContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 8);

		case 1: return precpred(_ctx, 7);

		case 2: return precpred(_ctx, 6);

		case 3: return precpred(_ctx, 5);

		case 4: return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3>\u023a\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\3\2\3\2\6\2g\n\2"+
		"\r\2\16\2h\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\u0096\n\3\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\5\4\u00a0\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4\u00b6\n\4\f\4\16\4\u00b9"+
		"\13\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\n\3\n\5\n\u00c9"+
		"\n\n\3\n\3\n\3\n\3\n\5\n\u00cf\n\n\3\n\3\n\5\n\u00d3\n\n\3\n\3\n\3\n\3"+
		"\n\3\n\3\n\3\n\5\n\u00dc\n\n\3\n\3\n\5\n\u00e0\n\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\5\n\u00e9\n\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16\3"+
		"\17\3\17\5\17\u00f7\n\17\5\17\u00f9\n\17\3\17\3\17\5\17\u00fd\n\17\3\17"+
		"\3\17\3\17\3\17\3\20\3\20\5\20\u0105\n\20\3\20\3\20\5\20\u0109\n\20\3"+
		"\20\3\20\5\20\u010d\n\20\3\20\3\20\5\20\u0111\n\20\7\20\u0113\n\20\f\20"+
		"\16\20\u0116\13\20\3\20\3\20\3\21\5\21\u011b\n\21\3\21\3\21\3\21\5\21"+
		"\u0120\n\21\3\22\5\22\u0123\n\22\3\22\3\22\3\22\5\22\u0128\n\22\3\23\3"+
		"\23\3\24\5\24\u012d\n\24\3\24\3\24\5\24\u0131\n\24\3\25\5\25\u0134\n\25"+
		"\3\25\3\25\5\25\u0138\n\25\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31"+
		"\3\31\3\32\3\32\3\32\7\32\u0147\n\32\f\32\16\32\u014a\13\32\3\32\3\32"+
		"\3\32\3\33\3\33\7\33\u0151\n\33\f\33\16\33\u0154\13\33\3\33\3\33\3\34"+
		"\3\34\7\34\u015a\n\34\f\34\16\34\u015d\13\34\3\34\3\34\3\34\3\34\7\34"+
		"\u0163\n\34\f\34\16\34\u0166\13\34\3\34\5\34\u0169\n\34\5\34\u016b\n\34"+
		"\3\35\3\35\3\35\3\35\3\35\3\35\3\35\7\35\u0174\n\35\f\35\16\35\u0177\13"+
		"\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\5\36\u0181\n\36\3\36\3\36"+
		"\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\5\36\u018d\n\36\3\36\3\36\3\36"+
		"\3\36\5\36\u0193\n\36\3\37\3\37\3\37\5\37\u0198\n\37\3 \3 \3 \3 \3 \5"+
		" \u019f\n \3 \3 \3 \3 \3!\3!\3!\3!\3!\3!\5!\u01ab\n!\3!\3!\3!\5!\u01b0"+
		"\n!\3!\3!\3!\3!\5!\u01b6\n!\3!\3!\3!\3!\3\"\3\"\3#\3#\3$\3$\3$\3$\3$\3"+
		"$\3%\3%\3%\3%\3%\5%\u01cb\n%\3%\3%\3%\3%\5%\u01d1\n%\7%\u01d3\n%\f%\16"+
		"%\u01d6\13%\5%\u01d8\n%\3%\3%\3%\3&\3&\3&\3&\3&\3\'\3\'\5\'\u01e4\n\'"+
		"\3(\3(\3(\3(\3(\3(\5(\u01ec\n(\7(\u01ee\n(\f(\16(\u01f1\13(\3(\3(\3(\3"+
		")\3)\3)\3)\3)\5)\u01fb\n)\3)\7)\u01fe\n)\f)\16)\u0201\13)\3)\3)\3)\3*"+
		"\3*\3*\3*\3*\7*\u020b\n*\f*\16*\u020e\13*\5*\u0210\n*\3*\3*\3+\3+\3+\3"+
		"+\3+\6+\u0219\n+\r+\16+\u021a\3,\3,\3,\3,\3,\6,\u0222\n,\r,\16,\u0223"+
		"\3-\3-\3-\3-\3.\3.\3.\5.\u022d\n.\3.\3.\3/\3/\3/\3\60\3\60\3\61\3\61\3"+
		"\62\3\62\3\62\2\3\6\63\2\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,"+
		".\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`b\2\n\3\2\32\34\3\2\30\31\3\2%(\3\2"+
		"#$\3\2)*\3\2\26\27\3\2\r\17\3\2:;\u0269\2d\3\2\2\2\4\u0095\3\2\2\2\6\u009f"+
		"\3\2\2\2\b\u00ba\3\2\2\2\n\u00bc\3\2\2\2\f\u00be\3\2\2\2\16\u00c0\3\2"+
		"\2\2\20\u00c2\3\2\2\2\22\u00e8\3\2\2\2\24\u00ea\3\2\2\2\26\u00ec\3\2\2"+
		"\2\30\u00ee\3\2\2\2\32\u00f0\3\2\2\2\34\u00f8\3\2\2\2\36\u0102\3\2\2\2"+
		" \u011a\3\2\2\2\"\u0122\3\2\2\2$\u0129\3\2\2\2&\u012c\3\2\2\2(\u0133\3"+
		"\2\2\2*\u013b\3\2\2\2,\u013d\3\2\2\2.\u013f\3\2\2\2\60\u0141\3\2\2\2\62"+
		"\u0143\3\2\2\2\64\u014e\3\2\2\2\66\u016a\3\2\2\28\u016c\3\2\2\2:\u0192"+
		"\3\2\2\2<\u0197\3\2\2\2>\u0199\3\2\2\2@\u01a4\3\2\2\2B\u01bb\3\2\2\2D"+
		"\u01bd\3\2\2\2F\u01bf\3\2\2\2H\u01c5\3\2\2\2J\u01dc\3\2\2\2L\u01e1\3\2"+
		"\2\2N\u01e5\3\2\2\2P\u01f5\3\2\2\2R\u0205\3\2\2\2T\u0213\3\2\2\2V\u021c"+
		"\3\2\2\2X\u0225\3\2\2\2Z\u0229\3\2\2\2\\\u0230\3\2\2\2^\u0233\3\2\2\2"+
		"`\u0235\3\2\2\2b\u0237\3\2\2\2df\b\2\1\2eg\5\4\3\2fe\3\2\2\2gh\3\2\2\2"+
		"hf\3\2\2\2hi\3\2\2\2ij\3\2\2\2jk\7\2\2\3k\3\3\2\2\2lm\5\6\4\2mn\7.\2\2"+
		"n\u0096\3\2\2\2o\u0096\5\32\16\2pq\5\34\17\2qr\7.\2\2r\u0096\3\2\2\2s"+
		"t\5\36\20\2tu\7.\2\2u\u0096\3\2\2\2v\u0096\5\62\32\2w\u0096\5:\36\2x\u0096"+
		"\5> \2y\u0096\5@!\2z{\5B\"\2{|\7.\2\2|\u0096\3\2\2\2}~\5D#\2~\177\7.\2"+
		"\2\177\u0096\3\2\2\2\u0080\u0096\5F$\2\u0081\u0096\5J&\2\u0082\u0083\5"+
		"L\'\2\u0083\u0084\7.\2\2\u0084\u0096\3\2\2\2\u0085\u0096\5R*\2\u0086\u0087"+
		"\5N(\2\u0087\u0088\7.\2\2\u0088\u0096\3\2\2\2\u0089\u008a\5P)\2\u008a"+
		"\u008b\7.\2\2\u008b\u0096\3\2\2\2\u008c\u008d\5X-\2\u008d\u008e\7.\2\2"+
		"\u008e\u0096\3\2\2\2\u008f\u0090\5\\/\2\u0090\u0091\7.\2\2\u0091\u0096"+
		"\3\2\2\2\u0092\u0096\5^\60\2\u0093\u0096\58\35\2\u0094\u0096\7.\2\2\u0095"+
		"l\3\2\2\2\u0095o\3\2\2\2\u0095p\3\2\2\2\u0095s\3\2\2\2\u0095v\3\2\2\2"+
		"\u0095w\3\2\2\2\u0095x\3\2\2\2\u0095y\3\2\2\2\u0095z\3\2\2\2\u0095}\3"+
		"\2\2\2\u0095\u0080\3\2\2\2\u0095\u0081\3\2\2\2\u0095\u0082\3\2\2\2\u0095"+
		"\u0085\3\2\2\2\u0095\u0086\3\2\2\2\u0095\u0089\3\2\2\2\u0095\u008c\3\2"+
		"\2\2\u0095\u008f\3\2\2\2\u0095\u0092\3\2\2\2\u0095\u0093\3\2\2\2\u0095"+
		"\u0094\3\2\2\2\u0096\5\3\2\2\2\u0097\u0098\b\4\1\2\u0098\u0099\7\62\2"+
		"\2\u0099\u00a0\5\6\4\4\u009a\u009b\7\35\2\2\u009b\u009c\5\6\4\2\u009c"+
		"\u009d\7\36\2\2\u009d\u00a0\3\2\2\2\u009e\u00a0\5\22\n\2\u009f\u0097\3"+
		"\2\2\2\u009f\u009a\3\2\2\2\u009f\u009e\3\2\2\2\u00a0\u00b7\3\2\2\2\u00a1"+
		"\u00a2\f\n\2\2\u00a2\u00a3\5\b\5\2\u00a3\u00a4\5\6\4\13\u00a4\u00b6\3"+
		"\2\2\2\u00a5\u00a6\f\t\2\2\u00a6\u00a7\5\n\6\2\u00a7\u00a8\5\6\4\n\u00a8"+
		"\u00b6\3\2\2\2\u00a9\u00aa\f\b\2\2\u00aa\u00ab\5\f\7\2\u00ab\u00ac\5\6"+
		"\4\t\u00ac\u00b6\3\2\2\2\u00ad\u00ae\f\7\2\2\u00ae\u00af\5\16\b\2\u00af"+
		"\u00b0\5\6\4\b\u00b0\u00b6\3\2\2\2\u00b1\u00b2\f\6\2\2\u00b2\u00b3\5\20"+
		"\t\2\u00b3\u00b4\5\6\4\7\u00b4\u00b6\3\2\2\2\u00b5\u00a1\3\2\2\2\u00b5"+
		"\u00a5\3\2\2\2\u00b5\u00a9\3\2\2\2\u00b5\u00ad\3\2\2\2\u00b5\u00b1\3\2"+
		"\2\2\u00b6\u00b9\3\2\2\2\u00b7\u00b5\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8"+
		"\7\3\2\2\2\u00b9\u00b7\3\2\2\2\u00ba\u00bb\t\2\2\2\u00bb\t\3\2\2\2\u00bc"+
		"\u00bd\t\3\2\2\u00bd\13\3\2\2\2\u00be\u00bf\t\4\2\2\u00bf\r\3\2\2\2\u00c0"+
		"\u00c1\t\5\2\2\u00c1\17\3\2\2\2\u00c2\u00c3\t\6\2\2\u00c3\21\3\2\2\2\u00c4"+
		"\u00c8\5\24\13\2\u00c5\u00c9\7\66\2\2\u00c6\u00c9\5,\27\2\u00c7\u00c9"+
		"\5\6\4\2\u00c8\u00c5\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c8\u00c7\3\2\2\2\u00c9"+
		"\u00e9\3\2\2\2\u00ca\u00ce\5\26\f\2\u00cb\u00cf\7\66\2\2\u00cc\u00cf\5"+
		",\27\2\u00cd\u00cf\5\6\4\2\u00ce\u00cb\3\2\2\2\u00ce\u00cc\3\2\2\2\u00ce"+
		"\u00cd\3\2\2\2\u00cf\u00e9\3\2\2\2\u00d0\u00d3\7\66\2\2\u00d1\u00d3\5"+
		",\27\2\u00d2\u00d0\3\2\2\2\u00d2\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4"+
		"\u00e9\5\30\r\2\u00d5\u00e9\7\66\2\2\u00d6\u00e9\79\2\2\u00d7\u00e9\7"+
		"\21\2\2\u00d8\u00e9\7\22\2\2\u00d9\u00e9\5R*\2\u00da\u00dc\7\61\2\2\u00db"+
		"\u00da\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00e9\5V"+
		",\2\u00de\u00e0\7\61\2\2\u00df\u00de\3\2\2\2\u00df\u00e0\3\2\2\2\u00e0"+
		"\u00e1\3\2\2\2\u00e1\u00e9\5,\27\2\u00e2\u00e3\5$\23\2\u00e3\u00e4\5,"+
		"\27\2\u00e4\u00e9\3\2\2\2\u00e5\u00e6\5$\23\2\u00e6\u00e7\5\6\4\2\u00e7"+
		"\u00e9\3\2\2\2\u00e8\u00c4\3\2\2\2\u00e8\u00ca\3\2\2\2\u00e8\u00d2\3\2"+
		"\2\2\u00e8\u00d5\3\2\2\2\u00e8\u00d6\3\2\2\2\u00e8\u00d7\3\2\2\2\u00e8"+
		"\u00d8\3\2\2\2\u00e8\u00d9\3\2\2\2\u00e8\u00db\3\2\2\2\u00e8\u00df\3\2"+
		"\2\2\u00e8\u00e2\3\2\2\2\u00e8\u00e5\3\2\2\2\u00e9\23\3\2\2\2\u00ea\u00eb"+
		"\7\31\2\2\u00eb\25\3\2\2\2\u00ec\u00ed\t\7\2\2\u00ed\27\3\2\2\2\u00ee"+
		"\u00ef\t\7\2\2\u00ef\31\3\2\2\2\u00f0\u00f1\7\13\2\2\u00f1\u00f2\7\f\2"+
		"\2\u00f2\u00f3\b\16\1\2\u00f3\33\3\2\2\2\u00f4\u00f6\5 \21\2\u00f5\u00f7"+
		"\5$\23\2\u00f6\u00f5\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00f9\3\2\2\2\u00f8"+
		"\u00f4\3\2\2\2\u00f8\u00f9\3\2\2\2\u00f9\u00fc\3\2\2\2\u00fa\u00fd\5,"+
		"\27\2\u00fb\u00fd\5V,\2\u00fc\u00fa\3\2\2\2\u00fc\u00fb\3\2\2\2\u00fd"+
		"\u00fe\3\2\2\2\u00fe\u00ff\7\60\2\2\u00ff\u0100\5\6\4\2\u0100\u0101\b"+
		"\17\1\2\u0101\35\3\2\2\2\u0102\u0104\5\"\22\2\u0103\u0105\5$\23\2\u0104"+
		"\u0103\3\2\2\2\u0104\u0105\3\2\2\2\u0105\u0108\3\2\2\2\u0106\u0109\5,"+
		"\27\2\u0107\u0109\5T+\2\u0108\u0106\3\2\2\2\u0108\u0107\3\2\2\2\u0109"+
		"\u0114\3\2\2\2\u010a\u010c\7+\2\2\u010b\u010d\5$\23\2\u010c\u010b\3\2"+
		"\2\2\u010c\u010d\3\2\2\2\u010d\u0110\3\2\2\2\u010e\u0111\5,\27\2\u010f"+
		"\u0111\5T+\2\u0110\u010e\3\2\2\2\u0110\u010f\3\2\2\2\u0111\u0113\3\2\2"+
		"\2\u0112\u010a\3\2\2\2\u0113\u0116\3\2\2\2\u0114\u0112\3\2\2\2\u0114\u0115"+
		"\3\2\2\2\u0115\u0117\3\2\2\2\u0116\u0114\3\2\2\2\u0117\u0118\b\20\1\2"+
		"\u0118\37\3\2\2\2\u0119\u011b\7\20\2\2\u011a\u0119\3\2\2\2\u011a\u011b"+
		"\3\2\2\2\u011b\u011f\3\2\2\2\u011c\u0120\7\16\2\2\u011d\u0120\7\r\2\2"+
		"\u011e\u0120\5*\26\2\u011f\u011c\3\2\2\2\u011f\u011d\3\2\2\2\u011f\u011e"+
		"\3\2\2\2\u0120!\3\2\2\2\u0121\u0123\7\20\2\2\u0122\u0121\3\2\2\2\u0122"+
		"\u0123\3\2\2\2\u0123\u0127\3\2\2\2\u0124\u0128\7\16\2\2\u0125\u0128\7"+
		"\r\2\2\u0126\u0128\5*\26\2\u0127\u0124\3\2\2\2\u0127\u0125\3\2\2\2\u0127"+
		"\u0126\3\2\2\2\u0128#\3\2\2\2\u0129\u012a\7\32\2\2\u012a%\3\2\2\2\u012b"+
		"\u012d\7\20\2\2\u012c\u012b\3\2\2\2\u012c\u012d\3\2\2\2\u012d\u012e\3"+
		"\2\2\2\u012e\u0130\t\b\2\2\u012f\u0131\5$\23\2\u0130\u012f\3\2\2\2\u0130"+
		"\u0131\3\2\2\2\u0131\'\3\2\2\2\u0132\u0134\7\20\2\2\u0133\u0132\3\2\2"+
		"\2\u0133\u0134\3\2\2\2\u0134\u0135\3\2\2\2\u0135\u0137\t\b\2\2\u0136\u0138"+
		"\5$\23\2\u0137\u0136\3\2\2\2\u0137\u0138\3\2\2\2\u0138\u0139\3\2\2\2\u0139"+
		"\u013a\b\25\1\2\u013a)\3\2\2\2\u013b\u013c\7\67\2\2\u013c+\3\2\2\2\u013d"+
		"\u013e\7\67\2\2\u013e-\3\2\2\2\u013f\u0140\7\61\2\2\u0140/\3\2\2\2\u0141"+
		"\u0142\7\67\2\2\u0142\61\3\2\2\2\u0143\u0144\b\32\1\2\u0144\u0148\7\37"+
		"\2\2\u0145\u0147\5\4\3\2\u0146\u0145\3\2\2\2\u0147\u014a\3\2\2\2\u0148"+
		"\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149\u014b\3\2\2\2\u014a\u0148\3\2"+
		"\2\2\u014b\u014c\7 \2\2\u014c\u014d\b\32\1\2\u014d\63\3\2\2\2\u014e\u0152"+
		"\7\37\2\2\u014f\u0151\5\4\3\2\u0150\u014f\3\2\2\2\u0151\u0154\3\2\2\2"+
		"\u0152\u0150\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0155\3\2\2\2\u0154\u0152"+
		"\3\2\2\2\u0155\u0156\7 \2\2\u0156\65\3\2\2\2\u0157\u015b\7\65\2\2\u0158"+
		"\u015a\5\4\3\2\u0159\u0158\3\2\2\2\u015a\u015d\3\2\2\2\u015b\u0159\3\2"+
		"\2\2\u015b\u015c\3\2\2\2\u015c\u016b\3\2\2\2\u015d\u015b\3\2\2\2\u015e"+
		"\u015f\7\64\2\2\u015f\u0160\5\22\n\2\u0160\u0164\7/\2\2\u0161\u0163\5"+
		"\4\3\2\u0162\u0161\3\2\2\2\u0163\u0166\3\2\2\2\u0164\u0162\3\2\2\2\u0164"+
		"\u0165\3\2\2\2\u0165\u0168\3\2\2\2\u0166\u0164\3\2\2\2\u0167\u0169\5B"+
		"\"\2\u0168\u0167\3\2\2\2\u0168\u0169\3\2\2\2\u0169\u016b\3\2\2\2\u016a"+
		"\u0157\3\2\2\2\u016a\u015e\3\2\2\2\u016b\67\3\2\2\2\u016c\u016d\7\63\2"+
		"\2\u016d\u016e\7\35\2\2\u016e\u016f\5,\27\2\u016f\u0170\7\36\2\2\u0170"+
		"\u0171\b\35\1\2\u0171\u0175\7\37\2\2\u0172\u0174\5\66\34\2\u0173\u0172"+
		"\3\2\2\2\u0174\u0177\3\2\2\2\u0175\u0173\3\2\2\2\u0175\u0176\3\2\2\2\u0176"+
		"\u0178\3\2\2\2\u0177\u0175\3\2\2\2\u0178\u0179\7 \2\2\u0179\u017a\b\35"+
		"\1\2\u017a9\3\2\2\2\u017b\u017c\b\36\1\2\u017c\u017d\7\6\2\2\u017d\u0180"+
		"\7\35\2\2\u017e\u0181\5\6\4\2\u017f\u0181\5\34\17\2\u0180\u017e\3\2\2"+
		"\2\u0180\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182\u0183\7\36\2\2\u0183"+
		"\u0184\b\36\1\2\u0184\u0185\5Z.\2\u0185\u0186\5<\37\2\u0186\u0193\3\2"+
		"\2\2\u0187\u0188\b\36\1\2\u0188\u0189\7\6\2\2\u0189\u018c\7\35\2\2\u018a"+
		"\u018d\5\6\4\2\u018b\u018d\5\34\17\2\u018c\u018a\3\2\2\2\u018c\u018b\3"+
		"\2\2\2\u018d\u018e\3\2\2\2\u018e\u018f\7\36\2\2\u018f\u0190\b\36\1\2\u0190"+
		"\u0191\5Z.\2\u0191\u0193\3\2\2\2\u0192\u017b\3\2\2\2\u0192\u0187\3\2\2"+
		"\2\u0193;\3\2\2\2\u0194\u0195\7\7\2\2\u0195\u0198\5Z.\2\u0196\u0198\3"+
		"\2\2\2\u0197\u0194\3\2\2\2\u0197\u0196\3\2\2\2\u0198=\3\2\2\2\u0199\u019a"+
		"\b \1\2\u019a\u019b\7\5\2\2\u019b\u019e\7\35\2\2\u019c\u019f\5\6\4\2\u019d"+
		"\u019f\5\34\17\2\u019e\u019c\3\2\2\2\u019e\u019d\3\2\2\2\u019f\u01a0\3"+
		"\2\2\2\u01a0\u01a1\7\36\2\2\u01a1\u01a2\b \1\2\u01a2\u01a3\5Z.\2\u01a3"+
		"?\3\2\2\2\u01a4\u01a5\b!\1\2\u01a5\u01a6\7\b\2\2\u01a6\u01aa\7\35\2\2"+
		"\u01a7\u01ab\5\34\17\2\u01a8\u01ab\5,\27\2\u01a9\u01ab\5b\62\2\u01aa\u01a7"+
		"\3\2\2\2\u01aa\u01a8\3\2\2\2\u01aa\u01a9\3\2\2\2\u01ab\u01ac\3\2\2\2\u01ac"+
		"\u01af\7.\2\2\u01ad\u01b0\5\6\4\2\u01ae\u01b0\5b\62\2\u01af\u01ad\3\2"+
		"\2\2\u01af\u01ae\3\2\2\2\u01b0\u01b1\3\2\2\2\u01b1\u01b5\7.\2\2\u01b2"+
		"\u01b6\5\6\4\2\u01b3\u01b6\5\34\17\2\u01b4\u01b6\5b\62\2\u01b5\u01b2\3"+
		"\2\2\2\u01b5\u01b3\3\2\2\2\u01b5\u01b4\3\2\2\2\u01b6\u01b7\3\2\2\2\u01b7"+
		"\u01b8\7\36\2\2\u01b8\u01b9\b!\1\2\u01b9\u01ba\5Z.\2\u01baA\3\2\2\2\u01bb"+
		"\u01bc\7\t\2\2\u01bcC\3\2\2\2\u01bd\u01be\7\n\2\2\u01beE\3\2\2\2\u01bf"+
		"\u01c0\5&\24\2\u01c0\u01c1\5\60\31\2\u01c1\u01c2\5H%\2\u01c2\u01c3\5\62"+
		"\32\2\u01c3\u01c4\b$\1\2\u01c4G\3\2\2\2\u01c5\u01c6\b%\1\2\u01c6\u01d7"+
		"\7\35\2\2\u01c7\u01cb\7\17\2\2\u01c8\u01cb\5\36\20\2\u01c9\u01cb\5(\25"+
		"\2\u01ca\u01c7\3\2\2\2\u01ca\u01c8\3\2\2\2\u01ca\u01c9\3\2\2\2\u01cb\u01d4"+
		"\3\2\2\2\u01cc\u01d0\7+\2\2\u01cd\u01d1\7\17\2\2\u01ce\u01d1\5\36\20\2"+
		"\u01cf\u01d1\5(\25\2\u01d0\u01cd\3\2\2\2\u01d0\u01ce\3\2\2\2\u01d0\u01cf"+
		"\3\2\2\2\u01d1\u01d3\3\2\2\2\u01d2\u01cc\3\2\2\2\u01d3\u01d6\3\2\2\2\u01d4"+
		"\u01d2\3\2\2\2\u01d4\u01d5\3\2\2\2\u01d5\u01d8\3\2\2\2\u01d6\u01d4\3\2"+
		"\2\2\u01d7\u01ca\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8\u01d9\3\2\2\2\u01d9"+
		"\u01da\7\36\2\2\u01da\u01db\b%\1\2\u01dbI\3\2\2\2\u01dc\u01dd\5&\24\2"+
		"\u01dd\u01de\5\60\31\2\u01de\u01df\5H%\2\u01df\u01e0\b&\1\2\u01e0K\3\2"+
		"\2\2\u01e1\u01e3\7\23\2\2\u01e2\u01e4\5\6\4\2\u01e3\u01e2\3\2\2\2\u01e3"+
		"\u01e4\3\2\2\2\u01e4M\3\2\2\2\u01e5\u01e6\7\3\2\2\u01e6\u01e7\7\35\2\2"+
		"\u01e7\u01ef\t\t\2\2\u01e8\u01eb\7+\2\2\u01e9\u01ec\5\6\4\2\u01ea\u01ec"+
		"\5`\61\2\u01eb\u01e9\3\2\2\2\u01eb\u01ea\3\2\2\2\u01ec\u01ee\3\2\2\2\u01ed"+
		"\u01e8\3\2\2\2\u01ee\u01f1\3\2\2\2\u01ef\u01ed\3\2\2\2\u01ef\u01f0\3\2"+
		"\2\2\u01f0\u01f2\3\2\2\2\u01f1\u01ef\3\2\2\2\u01f2\u01f3\7\36\2\2\u01f3"+
		"\u01f4\b(\1\2\u01f4O\3\2\2\2\u01f5\u01f6\7\4\2\2\u01f6\u01f7\7\35\2\2"+
		"\u01f7\u01ff\t\t\2\2\u01f8\u01fa\7+\2\2\u01f9\u01fb\5.\30\2\u01fa\u01f9"+
		"\3\2\2\2\u01fa\u01fb\3\2\2\2\u01fb\u01fc\3\2\2\2\u01fc\u01fe\5,\27\2\u01fd"+
		"\u01f8\3\2\2\2\u01fe\u0201\3\2\2\2\u01ff\u01fd\3\2\2\2\u01ff\u0200\3\2"+
		"\2\2\u0200\u0202\3\2\2\2\u0201\u01ff\3\2\2\2\u0202\u0203\7\36\2\2\u0203"+
		"\u0204\b)\1\2\u0204Q\3\2\2\2\u0205\u0206\5\60\31\2\u0206\u020f\7\35\2"+
		"\2\u0207\u020c\5\6\4\2\u0208\u0209\7+\2\2\u0209\u020b\5\6\4\2\u020a\u0208"+
		"\3\2\2\2\u020b\u020e\3\2\2\2\u020c\u020a\3\2\2\2\u020c\u020d\3\2\2\2\u020d"+
		"\u0210\3\2\2\2\u020e\u020c\3\2\2\2\u020f\u0207\3\2\2\2\u020f\u0210\3\2"+
		"\2\2\u0210\u0211\3\2\2\2\u0211\u0212\7\36\2\2\u0212S\3\2\2\2\u0213\u0218"+
		"\5,\27\2\u0214\u0215\7!\2\2\u0215\u0216\5\6\4\2\u0216\u0217\7\"\2\2\u0217"+
		"\u0219\3\2\2\2\u0218\u0214\3\2\2\2\u0219\u021a\3\2\2\2\u021a\u0218\3\2"+
		"\2\2\u021a\u021b\3\2\2\2\u021bU\3\2\2\2\u021c\u0221\5,\27\2\u021d\u021e"+
		"\7!\2\2\u021e\u021f\5\6\4\2\u021f\u0220\7\"\2\2\u0220\u0222\3\2\2\2\u0221"+
		"\u021d\3\2\2\2\u0222\u0223\3\2\2\2\u0223\u0221\3\2\2\2\u0223\u0224\3\2"+
		"\2\2\u0224W\3\2\2\2\u0225\u0226\7\24\2\2\u0226\u0227\5 \21\2\u0227\u0228"+
		"\5,\27\2\u0228Y\3\2\2\2\u0229\u022c\b.\1\2\u022a\u022d\5\64\33\2\u022b"+
		"\u022d\5\4\3\2\u022c\u022a\3\2\2\2\u022c\u022b\3\2\2\2\u022d\u022e\3\2"+
		"\2\2\u022e\u022f\b.\1\2\u022f[\3\2\2\2\u0230\u0231\7\25\2\2\u0231\u0232"+
		"\7\67\2\2\u0232]\3\2\2\2\u0233\u0234\78\2\2\u0234_\3\2\2\2\u0235\u0236"+
		"\7;\2\2\u0236a\3\2\2\2\u0237\u0238\3\2\2\2\u0238c\3\2\2\2:h\u0095\u009f"+
		"\u00b5\u00b7\u00c8\u00ce\u00d2\u00db\u00df\u00e8\u00f6\u00f8\u00fc\u0104"+
		"\u0108\u010c\u0110\u0114\u011a\u011f\u0122\u0127\u012c\u0130\u0133\u0137"+
		"\u0148\u0152\u015b\u0164\u0168\u016a\u0175\u0180\u018c\u0192\u0197\u019e"+
		"\u01aa\u01af\u01b5\u01ca\u01d0\u01d4\u01d7\u01e3\u01eb\u01ef\u01fa\u01ff"+
		"\u020c\u020f\u021a\u0223\u022c";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}