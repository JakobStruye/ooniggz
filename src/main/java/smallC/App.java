package smallC;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import symbolTable.FunctionEntry;
import symbolTable.SymbolTable;
import tree.Node;


public class App {
	public static void main(String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("Usage: $ ./c2p [C source] <[P source]> <-d> <-g <[name]>> <-saveast <[name]>> <-s>"
					+ "\n \t reads [C source], compiles to P, writes to [P source] (default 'p.out')"
					+ "\n\n\t -d: debug mode (default ANTLR errors)"
					+ "\n\t -g: generates GraphViz file *name*.gv of AST for GraphViz DOT"
					+ "\n\t\t Default name is 'AST' \n\t\t To generate visualization: $ dot -Tps *name*.gv -o *name*.ps"
					+ "\n\t -saveast: saves serialized AST to a binary file *name*.bin"
					+ "\n\t\t Default name is 'AST'"
					+ "\n\t\t This is a binary, non human-readable file, \n\t\t meant to check if two ASTs are equal"
					+ "\n\t\t For a readable format use the visualization with -g"
					+ "\n\t -s: prints the symbol table in human-readable format"
					+ "\n\t -r: turns error for missing return in some branch into warning");
					//-t is reserved for internal testing purposes!
			return;
		}
		
		compile(args);

	}
	
	/**
	 * Does the actual compiling and additional work.
	 */
	static Boolean compile(String[] args) throws Exception{
		ANTLRFileStream input = new ANTLRFileStream(args[0]);
		
		smallCLexer lexer = new smallCLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		
		smallCParser parser = new smallCParser(tokens);
		
		List<String> argList = Arrays.asList(args);
		
		//Add custom errorListener unless specified otherwise by user
		MyErrorListener listener = new MyErrorListener();
		
		//This is only used for tests; only then should a syntactical error not halt parser
		if (argList.contains("-t")) {
			listener.noCrash();
		}
		if (!argList.contains("-d")) { 
			parser.removeErrorListeners();
			parser.addErrorListener(listener);
		}
		if (argList.contains("-r")) {
			Node.noReturnError = true; //disable crashing on missing return statements
		}
						
		ParserRuleContext tree = parser.prog(); // parse the code
		
		//if noCrash wasn't set, an exception will already have been thrown
		if(listener.hasErrored()) {
			return false;
		}
		
		//Grab the symbol table from the parser
		SymbolTable symbolTable = parser.symbolTable;
		Node.symbolTable = symbolTable;
		if (argList.contains("-s")) {
			symbolTable.print();
		}

		
		/**
		 * Note that we do use a walker and listener here. However these are only used for AST generation.
		 * The only two functions in the listener are enterEveryRule and exitEveryRule.
		 * Our intention was to do this in grammar actions, however a quirk of ANTLR undermined this.
		 * For ANTLR to be able to handle left-recursion (in our arith rule), the left recursion must be 
		 * the very first thing in the rule. This means not even an action can precede it. For our AST
		 * generation to work, we need to generate the AST node before accessing the subrules of the current
		 * rule (== its children), which was thus impossible in actions. By moving this to a listener, we do
		 * essentially the same thing, only after parsing instead of during.
		 */
		
		ParseTreeWalker walker = new ParseTreeWalker(); // create standard walker		
		ASTgenerator extractor = new ASTgenerator();
		walker.walk(extractor, tree); // generate AST
		Integer maxLocation = symbolTable.getLocation() + 1;// (+1 extra for return backup at store[0])
		
		//Grab the root of the AST (rest of tree reachable from root)
		Node root = extractor.root;
		Stack<Integer> scopes = new Stack<Integer>();
		scopes.add(0); //0 is global scope, set beforehand
		
		//Get outfile name from argumens if provided, else default name
		String outFileName = "p.out";
		if (args.length > 1 && !args[1].startsWith("-")) {
			outFileName = args[1];
		}
		
//		//Write to P file
		PrintWriter writer = new PrintWriter(outFileName);
		
		PrintWriter dummy = new PrintWriter("temp.p"); //Will temporarily contain global scope code, clear file first
		dummy.close();
		
		try {
			Node.assigningID = ""; //Make sure to reset this to avoid mvn errors
			FunctionEntry.overloadMap = new HashMap<String, Integer>(); //Make sure to reset this to avoid mvn errors
			
			//Check if main exists
			if (symbolTable.getFunctionEntries("main").size() != 1) {
				writer.close();
				throw new SemanticException("Exactly one main should be defined!", 0);
			}
			
			//Set some node members including scoping 
			root.setInfo(scopes, 0, parser.useMaxScopeQueue, false);
			
			//Abstractifying the root will add information to some nodes and mark some nodes as to be pruned
			//pruneTree will then do the actual pruning
			root.abstractify(symbolTable);
			root.pruneTree();
			
			//Note that, apart from pruning being done only by abstractify, the split between what is done in setInfo
			//and what is done in abstractify is somewhat arbitrary.
			
			//Remove access to symbol table to show code generation doens't use it.
			symbolTable = null;
			Node.symbolTable = null;
			parser.symbolTable = null;
			
			//Start code generation
			writer.println("ssp " + maxLocation); // Reserve store space
			for (Integer i = 0; i < maxLocation-1; i++) { //Initialise to 0
				writer.println("ldc i 0");
				writer.println("sro i " + i);
			}

			writer.println("ujp global"); //Execute global scope code first (will jump to main afterwards)
			root.visitNode(writer);
			writer.println("hlt");
			writer.println("global:"); //Put the global scope code here
			try (BufferedReader br = new BufferedReader(new FileReader("temp.p"))) {
				//All global scope code contained in file, append to outfile
			    String line;
			    while ((line = br.readLine()) != null) {
			       writer.println(line);
			    }
			}
			writer.println("ujp func_main"); //jump to main after global scope
			
    		Files.delete(Paths.get("temp.p")); //Remove temp file


		}
		catch (SemanticException e) {
			System.out.println("A semantical error was detected at line "
					+ e.line + ".\n" + e.getMessage()
					+ ". \nThe compiler will now terminate.");

			Files.delete(Paths.get("temp.p")); //Clean up after ourselves in case of exception
			Files.delete(Paths.get(outFileName));

			return false;
		}
		
		//Remove output if testing
		if (argList.contains("-t")) {
			File file = new File("p.out");
			file.delete();
		}
		
		//For DOT file
		if (argList.contains("-g")) {
			int index = argList.indexOf("-g");
			String name = "AST";
			//Only set custom name if supplied (check if not followed by other flag!)
			if (index != argList.size()-1 && !(argList.get(index+1).startsWith("-"))) {
				name = argList.get(index+1);
			}
			extractor.root.dotTree(0, 0, name + ".gv");
		}
		
		//For binary serialization
		if (argList.contains("-saveast")) {
			int index = argList.indexOf("-saveast");
			String name = "AST";
			//Only set custom name if supplied (check if not followed by other flag!)
			if (index != argList.size()-1 && !(argList.get(index+1).startsWith("-"))) {
				name = argList.get(index+1);
			}
			extractor.root.serialize(name);
		}
		
		writer.close();	

		if (argList.contains("-d")) {
			return true;
		}
		else {
			return !(listener.hasErrored());
		}
	}
}
