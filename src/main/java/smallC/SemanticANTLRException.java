package smallC;

import org.antlr.v4.runtime.NoViableAltException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.atn.ATNConfigSet;

public class SemanticANTLRException extends NoViableAltException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2064344842008630848L;
	
	String message;
	
	public SemanticANTLRException(Parser recognizer) {
		super(recognizer);
		this.message = "";
	}

	public SemanticANTLRException(Parser recognizer, String message) {
		super(recognizer);
		this.message = message;
	}

	public SemanticANTLRException(Parser recognizer, TokenStream input,
			Token startToken, Token offendingToken,
			ATNConfigSet deadEndConfigs, ParserRuleContext ctx) {
		super(recognizer, input, startToken, offendingToken, deadEndConfigs,
				ctx);
	}
	
	public String getMessage() {
		return message;
	}

}
