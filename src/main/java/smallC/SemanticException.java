package smallC;

public class SemanticException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2133825564639535492L;
	
	public Integer line;

	public SemanticException(Integer line) {
		this.line = line;
	}

	public SemanticException(String message, Integer line) {
		super(message);
		this.line = line;
	}
}
