// Generated from smallC.g4 by ANTLR 4.3

	package smallC;
	import tree.*;
	import symbolTable.*;
	import java.util.Stack;
	import java.util.Queue;
	import java.util.LinkedList;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link smallCParser}.
 */
public interface smallCListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link smallCParser#arrayElem}.
	 * @param ctx the parse tree
	 */
	void enterArrayElem(@NotNull smallCParser.ArrayElemContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#arrayElem}.
	 * @param ctx the parse tree
	 */
	void exitArrayElem(@NotNull smallCParser.ArrayElemContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#compoundNoScope}.
	 * @param ctx the parse tree
	 */
	void enterCompoundNoScope(@NotNull smallCParser.CompoundNoScopeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#compoundNoScope}.
	 * @param ctx the parse tree
	 */
	void exitCompoundNoScope(@NotNull smallCParser.CompoundNoScopeContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#scanfCall}.
	 * @param ctx the parse tree
	 */
	void enterScanfCall(@NotNull smallCParser.ScanfCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#scanfCall}.
	 * @param ctx the parse tree
	 */
	void exitScanfCall(@NotNull smallCParser.ScanfCallContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#continueStat}.
	 * @param ctx the parse tree
	 */
	void enterContinueStat(@NotNull smallCParser.ContinueStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#continueStat}.
	 * @param ctx the parse tree
	 */
	void exitContinueStat(@NotNull smallCParser.ContinueStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(@NotNull smallCParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(@NotNull smallCParser.TypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#empty}.
	 * @param ctx the parse tree
	 */
	void enterEmpty(@NotNull smallCParser.EmptyContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#empty}.
	 * @param ctx the parse tree
	 */
	void exitEmpty(@NotNull smallCParser.EmptyContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#lowop}.
	 * @param ctx the parse tree
	 */
	void enterLowop(@NotNull smallCParser.LowopContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#lowop}.
	 * @param ctx the parse tree
	 */
	void exitLowop(@NotNull smallCParser.LowopContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#postcrement}.
	 * @param ctx the parse tree
	 */
	void enterPostcrement(@NotNull smallCParser.PostcrementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#postcrement}.
	 * @param ctx the parse tree
	 */
	void exitPostcrement(@NotNull smallCParser.PostcrementContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#returnStat}.
	 * @param ctx the parse tree
	 */
	void enterReturnStat(@NotNull smallCParser.ReturnStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#returnStat}.
	 * @param ctx the parse tree
	 */
	void exitReturnStat(@NotNull smallCParser.ReturnStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#scopedBody}.
	 * @param ctx the parse tree
	 */
	void enterScopedBody(@NotNull smallCParser.ScopedBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#scopedBody}.
	 * @param ctx the parse tree
	 */
	void exitScopedBody(@NotNull smallCParser.ScopedBodyContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#id}.
	 * @param ctx the parse tree
	 */
	void enterId(@NotNull smallCParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#id}.
	 * @param ctx the parse tree
	 */
	void exitId(@NotNull smallCParser.IdContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#whileStat}.
	 * @param ctx the parse tree
	 */
	void enterWhileStat(@NotNull smallCParser.WhileStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#whileStat}.
	 * @param ctx the parse tree
	 */
	void exitWhileStat(@NotNull smallCParser.WhileStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#invert}.
	 * @param ctx the parse tree
	 */
	void enterInvert(@NotNull smallCParser.InvertContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#invert}.
	 * @param ctx the parse tree
	 */
	void exitInvert(@NotNull smallCParser.InvertContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#jumpLabel}.
	 * @param ctx the parse tree
	 */
	void enterJumpLabel(@NotNull smallCParser.JumpLabelContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#jumpLabel}.
	 * @param ctx the parse tree
	 */
	void exitJumpLabel(@NotNull smallCParser.JumpLabelContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#argType}.
	 * @param ctx the parse tree
	 */
	void enterArgType(@NotNull smallCParser.ArgTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#argType}.
	 * @param ctx the parse tree
	 */
	void exitArgType(@NotNull smallCParser.ArgTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#ampersand}.
	 * @param ctx the parse tree
	 */
	void enterAmpersand(@NotNull smallCParser.AmpersandContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#ampersand}.
	 * @param ctx the parse tree
	 */
	void exitAmpersand(@NotNull smallCParser.AmpersandContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#functionArguments}.
	 * @param ctx the parse tree
	 */
	void enterFunctionArguments(@NotNull smallCParser.FunctionArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#functionArguments}.
	 * @param ctx the parse tree
	 */
	void exitFunctionArguments(@NotNull smallCParser.FunctionArgumentsContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#arrayDecl}.
	 * @param ctx the parse tree
	 */
	void enterArrayDecl(@NotNull smallCParser.ArrayDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#arrayDecl}.
	 * @param ctx the parse tree
	 */
	void exitArrayDecl(@NotNull smallCParser.ArrayDeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#breakStat}.
	 * @param ctx the parse tree
	 */
	void enterBreakStat(@NotNull smallCParser.BreakStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#breakStat}.
	 * @param ctx the parse tree
	 */
	void exitBreakStat(@NotNull smallCParser.BreakStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#typedef}.
	 * @param ctx the parse tree
	 */
	void enterTypedef(@NotNull smallCParser.TypedefContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#typedef}.
	 * @param ctx the parse tree
	 */
	void exitTypedef(@NotNull smallCParser.TypedefContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#forStat}.
	 * @param ctx the parse tree
	 */
	void enterForStat(@NotNull smallCParser.ForStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#forStat}.
	 * @param ctx the parse tree
	 */
	void exitForStat(@NotNull smallCParser.ForStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#caseSwitch}.
	 * @param ctx the parse tree
	 */
	void enterCaseSwitch(@NotNull smallCParser.CaseSwitchContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#caseSwitch}.
	 * @param ctx the parse tree
	 */
	void exitCaseSwitch(@NotNull smallCParser.CaseSwitchContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(@NotNull smallCParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(@NotNull smallCParser.FunctionCallContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#lowrelop}.
	 * @param ctx the parse tree
	 */
	void enterLowrelop(@NotNull smallCParser.LowrelopContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#lowrelop}.
	 * @param ctx the parse tree
	 */
	void exitLowrelop(@NotNull smallCParser.LowrelopContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#arith}.
	 * @param ctx the parse tree
	 */
	void enterArith(@NotNull smallCParser.ArithContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#arith}.
	 * @param ctx the parse tree
	 */
	void exitArith(@NotNull smallCParser.ArithContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDecl(@NotNull smallCParser.FunctionDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#functionDecl}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDecl(@NotNull smallCParser.FunctionDeclContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#printfCall}.
	 * @param ctx the parse tree
	 */
	void enterPrintfCall(@NotNull smallCParser.PrintfCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#printfCall}.
	 * @param ctx the parse tree
	 */
	void exitPrintfCall(@NotNull smallCParser.PrintfCallContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(@NotNull smallCParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(@NotNull smallCParser.StringContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#elseStat}.
	 * @param ctx the parse tree
	 */
	void enterElseStat(@NotNull smallCParser.ElseStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#elseStat}.
	 * @param ctx the parse tree
	 */
	void exitElseStat(@NotNull smallCParser.ElseStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#typedefType}.
	 * @param ctx the parse tree
	 */
	void enterTypedefType(@NotNull smallCParser.TypedefTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#typedefType}.
	 * @param ctx the parse tree
	 */
	void exitTypedefType(@NotNull smallCParser.TypedefTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#logicalandor}.
	 * @param ctx the parse tree
	 */
	void enterLogicalandor(@NotNull smallCParser.LogicalandorContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#logicalandor}.
	 * @param ctx the parse tree
	 */
	void exitLogicalandor(@NotNull smallCParser.LogicalandorContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#declType}.
	 * @param ctx the parse tree
	 */
	void enterDeclType(@NotNull smallCParser.DeclTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#declType}.
	 * @param ctx the parse tree
	 */
	void exitDeclType(@NotNull smallCParser.DeclTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterDefinition(@NotNull smallCParser.DefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitDefinition(@NotNull smallCParser.DefinitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#functionType}.
	 * @param ctx the parse tree
	 */
	void enterFunctionType(@NotNull smallCParser.FunctionTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#functionType}.
	 * @param ctx the parse tree
	 */
	void exitFunctionType(@NotNull smallCParser.FunctionTypeContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#highrelop}.
	 * @param ctx the parse tree
	 */
	void enterHighrelop(@NotNull smallCParser.HighrelopContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#highrelop}.
	 * @param ctx the parse tree
	 */
	void exitHighrelop(@NotNull smallCParser.HighrelopContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void enterIfStat(@NotNull smallCParser.IfStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#ifStat}.
	 * @param ctx the parse tree
	 */
	void exitIfStat(@NotNull smallCParser.IfStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#include}.
	 * @param ctx the parse tree
	 */
	void enterInclude(@NotNull smallCParser.IncludeContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#include}.
	 * @param ctx the parse tree
	 */
	void exitInclude(@NotNull smallCParser.IncludeContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterStat(@NotNull smallCParser.StatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitStat(@NotNull smallCParser.StatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#switchStat}.
	 * @param ctx the parse tree
	 */
	void enterSwitchStat(@NotNull smallCParser.SwitchStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#switchStat}.
	 * @param ctx the parse tree
	 */
	void exitSwitchStat(@NotNull smallCParser.SwitchStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(@NotNull smallCParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(@NotNull smallCParser.FunctionNameContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(@NotNull smallCParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(@NotNull smallCParser.DeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#compound}.
	 * @param ctx the parse tree
	 */
	void enterCompound(@NotNull smallCParser.CompoundContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#compound}.
	 * @param ctx the parse tree
	 */
	void exitCompound(@NotNull smallCParser.CompoundContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(@NotNull smallCParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(@NotNull smallCParser.ProgContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#ptr}.
	 * @param ctx the parse tree
	 */
	void enterPtr(@NotNull smallCParser.PtrContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#ptr}.
	 * @param ctx the parse tree
	 */
	void exitPtr(@NotNull smallCParser.PtrContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#highop}.
	 * @param ctx the parse tree
	 */
	void enterHighop(@NotNull smallCParser.HighopContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#highop}.
	 * @param ctx the parse tree
	 */
	void exitHighop(@NotNull smallCParser.HighopContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#precrement}.
	 * @param ctx the parse tree
	 */
	void enterPrecrement(@NotNull smallCParser.PrecrementContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#precrement}.
	 * @param ctx the parse tree
	 */
	void exitPrecrement(@NotNull smallCParser.PrecrementContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#gotoStat}.
	 * @param ctx the parse tree
	 */
	void enterGotoStat(@NotNull smallCParser.GotoStatContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#gotoStat}.
	 * @param ctx the parse tree
	 */
	void exitGotoStat(@NotNull smallCParser.GotoStatContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#functionDef}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDef(@NotNull smallCParser.FunctionDefContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#functionDef}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDef(@NotNull smallCParser.FunctionDefContext ctx);

	/**
	 * Enter a parse tree produced by {@link smallCParser#atom}.
	 * @param ctx the parse tree
	 */
	void enterAtom(@NotNull smallCParser.AtomContext ctx);
	/**
	 * Exit a parse tree produced by {@link smallCParser#atom}.
	 * @param ctx the parse tree
	 */
	void exitAtom(@NotNull smallCParser.AtomContext ctx);
}