package smallC;

import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;

import tree.*;

public class ASTgenerator extends smallCBaseListener {
	
	Node sibling = null;
	Node parent = null;
	Node root = null;
	
	Stack<Node> nodes = new Stack<Node>();

	@Override
	/**
	 * Generate AST node and set pointers that can be set at this point
	 */
	public void enterEveryRule(ParserRuleContext ctx) {
		Node node = NodeGenerator.generateNode(ctx);
		if (root == null) {
			// root case
			root = node;
		}
		nodes.push(node);
		//Null nodes are returned instead of nodes that would be pruned instead.
		//Don't set any pointers if a null node is detected, just add to stack (for exit method below)
		//and move on
		if (node == null) {
			return;
		}
		//Leftmost child (has no left sibling)
		if (sibling == null && parent != null) {
			node.parent = parent; //global parent pointer is this node's parent
			parent.leftmostChild = node; //parent's leftmost child is this node
			node.leftmostSibling = node; //node is its own leftmost sibling
		}
		//Not a leftmost child (has a left sibling)
		else if (sibling != null) {
			node.leftmostSibling = sibling.leftmostSibling; //same leftmost sibling as left sibling
			sibling.rightSibling = node; //node is right sibling of its left sibling
			node.parent = sibling.parent; //same parent as left sibling
		}
		sibling = null; //set back to 0, as the value has been used here (and should only be used once)
		parent = node; //set to node, for potential leftmost child of this node	
	}

	@Override
	public void exitEveryRule(ParserRuleContext ctx) {
		//Grab the node for the rule being exited from the stack
		//Set the sibling pointer to this node UNLESS it is null
		//As null nodes won't be added to the AST
		Node topStack = nodes.pop();
		if (topStack != null) {
			sibling = topStack;
		}
	}
}
