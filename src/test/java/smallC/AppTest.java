package smallC;


/**
 * This will try to compile a number of source files, and check if only valid ones are accepted.
 *
 */
public class AppTest {

	public AppTest() {
	}

	// SYNTAX TESTS
	
	public void testBad1() throws Exception {
		assert(!compile("syn_bad1.c"));
	}

	public void testBad2() throws Exception {
		assert(!compile("syn_bad2.c"));
	}

	public void testBad3() throws Exception {
		assert(!compile("syn_bad3.c"));
	}
	
	public void testDeclareDefineBad1() throws Exception {
		assert(!compile("syn_declareDefineBad1.c"));
	}

	public void testDeclareDefineGood() throws Exception {
		assert(compile("syn_declareDefineGood.c"));
	}

	public void testCommentsGood() throws Exception {
		assert(compile("syn_commentsGood.c"));
	}
	
	public void testCommentsBad() throws Exception {
		assert(!compile("syn_commentsBad.c"));
	}

	public void testCallGood() throws Exception {
		assert(compile("syn_callGood.c"));
	}

	public void testCallBad1() throws Exception {
		assert(!compile("syn_callBad1.c"));
	}

	public void testCallBad2() throws Exception {
		assert(!compile("syn_callBad2.c"));
	}

	public void testFunctionGood() throws Exception {
		assert(compile("syn_functionGood.c"));
	}

	public void testFunctionBad1() throws Exception {
		assert(!compile("syn_functionBad1.c"));
	}


	public void testStdioGood() throws Exception {
		assert(compile("syn_stdioGood.c"));
	}

	public void testStdioBad1() throws Exception {
		assert(!compile("syn_stdioBad1.c"));
	}
	
	public void testIncludeBad1() throws Exception {
		assert(!compile("syn_includeBad.c"));
	}
	
	public void testReservedWordsBad1() throws Exception {
		assert(!compile("syn_reservedWordsBad1.c"));
	}
	
	public void testReservedWordsBad2() throws Exception {
		assert(!compile("syn_reservedWordsBad2.c"));
	}
	
	public void testReservedWordsBad3() throws Exception {
		assert(!compile("syn_reservedWordsBad3.c"));
	}
	
	public void testReservedWordsBad4() throws Exception {
		assert(!compile("syn_reservedWordsBad4.c"));
	}
	
	public void testReservedWordsBad5() throws Exception {
		assert(!compile("syn_reservedWordsBad5.c"));
	}

	// SEMANTIC TESTS
	
	public void testDivByZero() throws Exception {
		assert(!compile("sem_divByZeroBad.c"));
	}

	public void testParamMismatch() throws Exception {
		assert(!compile("sem_paramMismatchBad.c"));
	}
	
	public void testReturnGood() throws Exception {
		assert(compile("sem_returnGood.c"));
	}

	public void testReturnBad1() throws Exception {
		assert(!compile("sem_returnBad1.c"));
	}
	
	public void testScanfPrintf() throws Exception {
		assert(compile("sem_scanfPrintf.c"));
	}
	
	public void testUndefFunc() throws Exception {
		assert(!compile("sem_undefFuncBad.c"));
	}
	
	public void testWrongVar1() throws Exception {
		assert(!compile("sem_wrongVar1.c"));
	}
	
	public void testRedecl1() throws Exception {
		assert(!compile("sem_varRedeclBad1.c"));
	}
	
	public void testRedecl2() throws Exception {
		assert(!compile("sem_varRedeclBad2.c"));
	}
	
	public void testRedecl3() throws Exception {
		assert(!compile("sem_varRedeclBad3.c"));
	}
	
	public void testRedecl4() throws Exception {
		assert(!compile("sem_varRedeclBad4.c"));
	}
	
	
	public void testReinit1() throws Exception {
		assert(!compile("sem_varReinitBad1.c"));
	}
	
	public void testMissingVarDecl1() throws Exception {
		assert(!compile("sem_missingVarDeclBad1.c"));
	}
	
	public void testGoodFctRedecl() throws Exception {
		assert(compile("sem_fctRedeclGood.c"));
	}
	
	public void testBadFctRedecl1() throws Exception {
		assert(!compile("sem_fctRedeclBad1.c"));
	}
	
	public void testBadScopedFctR() throws Exception {
		assert(!compile("sem_scopedFctBad.c"));
	}
	
	public void testConstBad1() throws Exception {
		assert(!compile("sem_constBad1.c"));
	}
	
	public void testConstBad2() throws Exception {
		assert(!compile("sem_constBad2.c"));
	}
	
	public void testConstGood() throws Exception {
		assert(compile("sem_constGood.c"));
	}
	
	public void testPrintfGood1() throws Exception {
		assert(compile("sem_printfGood1.c"));
	}
	
	public void testPrintfBad1() throws Exception {
		assert(!compile("sem_printfBad1.c"));
	}
	
	public void testPrintfBad2() throws Exception {
		assert(!compile("sem_printfBad2.c"));
	}
	
	public void testPrintfBad3() throws Exception {
		assert(!compile("sem_printfBad3.c"));
	}
	
	public void testPrintfBad4() throws Exception {
		assert(!compile("sem_printfBad4.c"));
	}
	
	public void testPrintfBad5() throws Exception {
		assert(!compile("sem_printfBad5.c"));
	}
	
	public void testForwardDeclGood() throws Exception {
		assert(compile("sem_forwardDeclGood.c"));
	}
	
	public void testForwardDeclBad1() throws Exception {
		assert(!compile("sem_forwardDeclBad1.c"));
	}
	
	public void testForwardDeclBad2() throws Exception {
		assert(!compile("sem_forwardDeclBad2.c"));
	}
	
	public void testForwardDeclBad3() throws Exception {
		assert(!compile("sem_forwardDeclBad3.c"));
	}
	
	public void testForwardDeclBad4() throws Exception {
		assert(!compile("sem_forwardDeclBad4.c"));
	}
	
	public void testArrayGood() throws Exception {
		assert(compile("sem_arrayGood.c"));
	}
	
	public void testArrayBad1() throws Exception {
		assert(!compile("sem_arrayBad1.c"));
	}
	
	public void testArrayBad2() throws Exception {
		assert(!compile("sem_arrayBad2.c"));
	}
	
	public void testForGood() throws Exception {
		assert(compile("sem_forGood.c"));
	}
	
	public void testBreakGood() throws Exception {
		assert(compile("sem_breakGood.c"));
	}
	
	public void testBreakBad() throws Exception {
		assert(!compile("sem_breakBad.c"));
	}

	public void testContinueGood() throws Exception {
		assert(compile("sem_continueGood.c"));
	}
	
	public void testContinueBad() throws Exception {
		assert(!compile("sem_continueBad.c"));
	}
	
	public void testSelfAssignGood() throws Exception {
		assert(compile("sem_selfAssignGood.c"));
	}
		
	public void testSelfAssignBad1() throws Exception {
		assert(!compile("sem_selfAssignBad1.c"));
	}
		
	public void testSelfAssignBad2() throws Exception {
		assert(!compile("sem_selfAssignBad2.c"));
	}
	
	public void testUseBeforeDecl() throws Exception {
		assert(!compile("sem_useBeforeDeclBad1.c"));
	}
	
	public void testUseBeforeDecl2() throws Exception {
		assert(!compile("sem_useBeforeDeclBad2.c"));
	}
	
	public void testDoubleMain() throws Exception {
		assert(!compile("sem_doubleMainBad.c"));
	}

	//CODE TESTS
	

	public void testGood() throws Exception {
		assert(compile("code_good.c"));
	}

	public void testArith() throws Exception {
		assert(compile("code_arith.c"));
	}
	
	public void testCrement() throws Exception {
		assert(compile("code_crement.c"));
	}
	
	public void testPointersGood() throws Exception {
		assert(compile("code_pointers.c"));
	}
	
	public void testIfElse() throws Exception {
		assert(compile("code_ifelse.c"));
	}

	public void testNegate() throws Exception {
		assert(compile("code_negate.c"));
	}
	
	public void testRecursion() throws Exception {
		assert(compile("code_recursion.c"));
	}
	
	public void testWhile() throws Exception {
		assert(compile("code_while.c"));
	}


	public void testSubLabel() throws Exception {
		assert(compile("code_sublabel.c"));
	}

	public void testPrintfWidth() throws Exception {
		assert(compile("code_printfPadding.c"));
	}

	public void testScanPrintArray() throws Exception {
		assert(compile("code_scanprintarray.c"));
	}
	
	public void testPtrAsArgument() throws Exception {
		assert(compile("code_ptrAsArgument.c"));
	}
	
	//BONUS FEATURE TESTS
	
	public void testGoto() throws Exception {
		assert(compile("bonus_goto.c"));
	}
	
	public void testMultidim() throws Exception {
		assert(compile("bonus_multidim.c"));
	}
	
	public void testMultidimBadDims() throws Exception {
		assert(!compile("bonus_multidimBadDims.c"));
	}
	
	public void testSwitchGood() throws Exception {
		assert(compile("bonus_switchGood.c"));
	}
	
	public void testSwitchBad1() throws Exception {
		assert(!compile("bonus_switchBad1.c"));
	}
	
	public void testSwitchBad2() throws Exception {
		assert(!compile("bonus_switchBad2.c"));
	}
	
	public void testParameterOverload() throws Exception {
		assert(compile("bonus_parameterOverload.c"));
	}
	
	public void testAutoInit() throws Exception {
		assert(compile("bonus_autoInit.c"));
	}
	
	public void testUndecl1() throws Exception {
		assert(!compile("bonus_undecl1.c"));
	}
	
	public void testUndecl2() throws Exception {
		assert(!compile("bonus_undecl2.c"));
	}
	
	public void testUndecl3() throws Exception {
		assert(!compile("bonus_undecl3.c"));
	}
	
	public void testReturnAllBranchesGood() throws Exception {
		assert(compile("bonus_returnAllBranchesGood.c"));
	}
	
	public void testReturnAllBranchesBad1() throws Exception {
		assert(!compile("bonus_returnAllBranchesBad1.c"));
	}
	
	public void testReturnAllBranchesBad2() throws Exception {
		assert(!compile("bonus_returnAllBranchesBad2.c"));
	}
	
	public void testReturnAllBranchesBad3() throws Exception {
		assert(!compile("bonus_returnAllBranchesBad3.c"));
	}
	
	public void testPointerArith() throws Exception {
		assert(compile("bonus_pointerArith.c"));
	}
	
	public void testMultiline() throws Exception {
		assert(compile("bonus_multilineComments.c"));
	}
	

	
	/**
	 * Tries to compile filename (in src/test/resources/sourceFiles/), returns true if successful
	 */
	private Boolean compile(String filename) throws Exception {
		String[] args;
		args = new String[2];
		args[0] = "src/test/resources/sourceFiles/" + filename;
		args[1] = "-t";
		return App.compile(args);
	}

	
	/**
	 * We ended up not using the following code to check if an AST had the same structure as a verified one
	 * It ended up being too much of a hassle to maintain and had limited use
	 * 
	 * It's still included for reference
	 */
//	/**
//	 * Compiles filename, returns true if AST generated during compilation equal to a verified one
//	 *
//	 * It's up to the tester to ensure the file actually compiles!
//	 */
//	private Boolean compileWithAST(String filename) throws Exception {
//		String[] args;
//		args = new String[4];
//		args[0] = "src/test/resources/sourceFiles/" + filename;
//		args[1] = "-t";
//		args[2] = "-saveast";
//		args[3] = "src/test/resources/output/" + filename;
//
//		App.compile(args);
//
//		String checksum1 = checkSum("src/test/resources/output/" + filename + ".bin");
//		String checksum2 = checkSum("src/test/resources/verified/" + filename + ".bin");
//
//		Path outputPath = Paths.get("src/test/resources/output/" + filename + ".bin");
//
//		Files.delete(outputPath);
//
//		return (checksum1.equals(checksum2));
//	}
//
//
//	/**
//	 * found at  http://javarevisited.blogspot.com/2013/06/how-to-generate-md5-checksum-for-files.html
//	 */
//    private static String checkSum(String path){
//        String checksum = null;
//        try {
//	        FileInputStream fis = new FileInputStream(path);
//	        MessageDigest md = MessageDigest.getInstance("MD5");
//
//	        //Using MessageDigest update() method to provide input
//	        byte[] buffer = new byte[8192];
//	        int numOfBytesRead;
//	        while( (numOfBytesRead = fis.read(buffer)) > 0){
//	            md.update(buffer, 0, numOfBytesRead);
//	        }
//	        byte[] hash = md.digest();
//	        checksum = new BigInteger(1, hash).toString(16); //don't use this, truncates leading zero
//	        fis.close();
//        }
//        catch(Exception e){System.out.println("COULD NOT CALCULATE CHECKSUM (missing file?)");}
//
//       return checksum;
//    }


}
