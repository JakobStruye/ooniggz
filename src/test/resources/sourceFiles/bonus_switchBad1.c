#include <stdio.h>

int main() {
	int a = 42;
	switch(a) {
	case 1:
		printf("Case! 1");
		break;
	case 42:
		printf("Case! 42");
		break;
	case 'c':
		printf("Case! 666");
		break;
	default:
		printf("default!");
	}

	return 0;
}
