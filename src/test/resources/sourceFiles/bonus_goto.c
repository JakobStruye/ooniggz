#include <stdio.h>

int main() {
    int i = 5;
    back:
    i++;
    if (i == 9) {
        goto skip;
    }
    goto back;
    skip:
    printf("%i", i);
    return 0;
}
