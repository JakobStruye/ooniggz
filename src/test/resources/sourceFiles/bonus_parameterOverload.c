#include <stdio.h>

int foo(int a, int b) {
    return 1;
}
int foo(int a) {
    return 2;
}
int foo(char a) {
    return 3;
}
int foo(char a, int x) {
    return 4;
}

int main() {
    int int1 = 1;
    int int2 = 2;
    char char1 = 'a';
    printf("first foo: %i\n", foo(int1, int2));
    printf("second foo: %i\n", foo(int1));
    printf("third foo: %i\n", foo(char1));
    printf("fourth foo: %i\n", foo(char1, int2));
    return;
}
