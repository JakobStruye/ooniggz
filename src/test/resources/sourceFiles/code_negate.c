#include <stdio.h>

int main(){
	if(!(5 == 4)) {
		printf("working negate\n");
	}
	else {
		printf("faulty negate\n");
	}

	if(!1) {
		printf("!1 shouldn't print\n");
	}

	if((!((5+5) == 10)) && (5+5)) {
		printf("this shouldn't print\n");
	}

	if((5+5) == 10) {
		printf("5+5 == 10");
	}
	return;
}
