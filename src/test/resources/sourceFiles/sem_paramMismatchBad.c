/* 
 * Increments its parameter
 */ 
int increment(int input)
{ return input + 1; }

int main(void)
{
	int z = increment(10, 'a');
	int k = increment('k');
    return 0;
}
