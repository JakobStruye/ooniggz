#include <stdio.h>

int main() {
	int quit = 0;
	int x = 0;
	while(!quit && 1) {
		if(x == 9) {
			quit = 1;
		}
		x++;
		printf("%i\n", x);
	}

	return 0;
}
