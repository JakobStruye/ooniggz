#include <stdio.h>

int main() {
    for (int i = 0; i > -1; i++) {
        if (i > 10) {
            break; //should loop indefinitely without break
        }
    }
    printf("We're past the for loop \n");
    int j = 5;
    while(1) {
        j--;
        if (j == 0) {
            break; //should loop indefinitely without break
        }
    }
    printf("We're past the while loop");

    return 0;


}
