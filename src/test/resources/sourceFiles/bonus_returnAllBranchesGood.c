int foo() {
    return 1;
}

int bar(int a) {
    if (a < 0) {
        return -1;
    }
    else if (a == 0) {
        return 0;
    }
    else {
        return 1;
    }
}

void baz(int a) {
    if (a < 0) {
        return;
    }
}

void biznaz(int a) {
    a++;
}

int main() {
    return 0;
}
