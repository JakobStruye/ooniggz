#include <stdio.h>

int main() {
    int j = 0;
    for (int i = 0; i == -1; i++) {
        j++;
        if (7 == j) {
            continue; //should never reach break without continue
        }
        j++;
        if (j == 11) {
            break;
        }
    }
    printf("We're past the for loop \n");
    int k = 6;
    while(1) {
        k++;
        if (7 == k) {
            continue; //should never reach break without continue
        }
        k++;
        if (k == 11) {
            break;
        }
    }
    printf("We're past the while loop");

    return 0;


}
