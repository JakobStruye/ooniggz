#include <stdio.h>

int foo(int* a) {
return *a;
}


int main() {
  int a = 5;
  int* ptr = &a;
  int b = foo(ptr);
  printf("%i", b);
  return;
}
