#include <stdio.h>

void recurse(int a) {
    if (a > 10) {
        return;
    }
    a++;
    int i = 2;
    int j = 5;
    int k;
    recurse(a);

    i++;
    printf("%i", i);
}

int main() {
    recurse(0);
    return 0;
}
