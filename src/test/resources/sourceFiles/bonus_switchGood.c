#include <stdio.h>

int main() {
	int a;
	printf("Enter an integer: ");
	scanf("%i", &a);

	switch(a) {
	case 1:
		printf("Case! 1\n");
		break;
	case 42:
		printf("Case! 42\n");
	case 666:
		printf("Case! 666\n");
		break;
	default:
		printf("default!\n");
	}



	//Check if switch didn't break scoping
	int b = 960;
	printf("%i\n", b);

	printf("Counting from 0!\n");
	for(int x = 0; x < 10; x++) {
		printf("%i\n", x);
	}

	printf("Demonstrating char switch!\n");
	char c = 'k';
	switch(c) {
	case 'a':
		printf("a");
		break;
	case 'b':
		printf("b");
		break;
	case 'k':
		printf("k");
		break;
	}

	return 0;
}
