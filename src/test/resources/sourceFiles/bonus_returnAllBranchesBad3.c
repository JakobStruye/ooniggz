int bar(int a) {
    for (int i = 0; i < a; i++) {
        if (i > 5) {
            return 0;
        }
    }
    int j = 10;
    while(j > a) {
        j--;
        return 1;
    }
}

int main() {
    return 0;
}
