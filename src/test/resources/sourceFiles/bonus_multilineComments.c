#include <stdio.h>

int main() {
	// This is a comment.
	int i = 5; // comment! // lalala
  /**
    this is a 
    multiline comment
  */ //another single line
	/*
	 * multiline containing bad code
	 * int a ='c';
	 * +a+;
	 *
	 */ printf("Code reached");
	return 0;
}
