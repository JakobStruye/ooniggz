#include <stdio.h>

int main() {
    int a = 5; //memory position 1
    char c = 'c'; //2
    int someInt = 27; //3
    char arr[20]; //4-23
    int arrMult[5][4]; //24-43

    arr[6] = 'x'; //10

    arrMult[2][3] = 42; //35

    int* ptrInt = &a;
    int* ptrChar = &c;

    printf("%i, %c, %i, %c, %i", *ptrInt, *ptrChar, *(ptrInt+2), *(ptrChar+8), *(ptrInt+34));

    return 0;
}
