//FOUND ON, ADAPTED SLIGHTLY FROM http://www.programmingsimplified.com/c-program-generate-fibonacci-series
//CODE LICENSED UNDER http://creativecommons.org/licenses/by-nc-nd/3.0/ BY PROGRAMMING SIMPLIFIED

#include<stdio.h>
 
int Fibonacci(int);
 
int main()
{
   int n = 15;
   int i = 0;
   int c;
 
   
 
   printf("Fibonacci series\n");
 
   for ( c = 1 ; c <= n ; c++ )
   {
      printf("%d\n", Fibonacci(i));
      i++; 
   }
   return 0;
}
 
int Fibonacci(int n)
{
   if ( n == 0 )
      return 0;
   if ( n == 1 )
      return 1;
   return ( Fibonacci(n-1) + Fibonacci(n-2) );
} 



