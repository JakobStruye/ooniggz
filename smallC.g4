/**
 * Define a grammar called RSubset
 */
grammar smallC;



@lexer::header {package smallC;}

@parser::header {
	package smallC;
	import tree.*;
	import symbolTable.*;
	import java.util.Stack;
	import java.util.Queue;
	import java.util.LinkedList;
}

@parser::members {
	SymbolTable symbolTable = new SymbolTable();
	ProgramNode root = null;
	Node parent = null;
	Node sibling = null;
	//scopes maintains a stack of all (unique!) scope numbers currently in use
	Stack<Integer> scopes = new Stack<Integer>();
	//maxScope indicates the highest scope number used so far
	Integer maxScope = 0;
	//useMaxScope indicates maxScope should be used instead of top of scopes (e.g. for function params as those are parsed before adding a scope)
	Boolean useMaxScope = false; 
	Queue<Boolean> useMaxScopeQueue = new LinkedList<Boolean>();
	Boolean stdioInclude = false;
	
	FunctionEntry latestFunction = null;
	Boolean setFunctionArgs = false;
	ArrayList functionArgs = new ArrayList<String>();
}

//Add global scope to scopes
prog: {this.scopes.push(0);} stat+ EOF;

//Enforce semicolon where one should be
stat: arith SEMICOLON
	| include
	| definition SEMICOLON
	| declaration SEMICOLON
	| compound
	| ifStat
	| whileStat
	| forStat
	| breakStat SEMICOLON
	| continueStat SEMICOLON
	| functionDef
	| functionDecl
	| returnStat SEMICOLON
	| functionCall
	| printfCall SEMICOLON
	| scanfCall SEMICOLON
	| typedef SEMICOLON
	| gotoStat SEMICOLON
	| jumpLabel
	| switchStat
	| SEMICOLON;
	
/*
 * As this grammar is to be used with ANTLR4, left-recursive rules as below are allowed.
 * ANTLR4 can handle this as long as the left-recursion is direct.
 * ANTLR4 will also give precedence to rules defined higher up.
 * This means the following rules lead to left-associative math with multiplication, division
 * and modulo having precedence over addition and subtraction. 
 * Those have precedence over all relational operators, with <, ≤, > and ≥ having precedence
 * over = and ≠. All the aforementioned have precedence over the logical and (&&), or (||)
 */
arith: arith highop arith
	 | arith lowop arith
	 | arith highrelop arith
	 | arith lowrelop arith
	 | arith logicalandor arith
	 | LPAREN arith RPAREN
	 | NEGATE arith
	 | atom
	 ;

highop: MULT | DIV | MOD;

lowop: PLUS | MINUS;

highrelop: GT | LT | GEQ | LEQ;

lowrelop: EQ | NEQ;

logicalandor: AND | OR;

atom: invert (INT|id|arith)
	| precrement (INT|id|arith)
	| (INT|id) postcrement
	| INT
	| CHAR
	| TRUE
	| FALSE
	| functionCall
	| AMPERSAND? arrayElem
	| AMPERSAND? id
	| ptr id
	| ptr arith
	;

invert: MINUS;

precrement: INCREMENT | DECREMENT;

postcrement: INCREMENT | DECREMENT;

include: INCLUDE STDIO {stdioInclude = true;};

definition: (type ptr?)? (id|arrayElem) ASSIGN arith
			{
				//Add to symbolTable
				Integer thisScope;
				useMaxScopeQueue.add(useMaxScope);
				if (useMaxScope) {
					thisScope = maxScope + 1;
				}
				else {
					 thisScope = this.scopes.peek();
				}
				if ($type.text != null) {
					Boolean success;
					if ($id.text != null) {
						if($ptr.text != null) {
							success = this.symbolTable.putEntry($id.text, $type.text + "*", thisScope);
						}
						else {
							success = this.symbolTable.putEntry($id.text, $type.text, thisScope);
						}
					}
					else {
						success = this.symbolTable.putEntry($arrayElem.text, $type.text, thisScope);
					}
					if (!success) {
						throw new SemanticANTLRException(this, "This variable already exists");
					}
				}
			}
	      ;

declaration:declType ptr? (id|arrayDecl)  (COMMA ptr? (id|arrayDecl))*
			{
				//Add to symbolTable
				Integer thisScope;
				useMaxScopeQueue.add(useMaxScope);
				if (useMaxScope) {
					thisScope = maxScope + 1;
				}
				else {
					thisScope = this.scopes.peek();
				}
				Boolean isPtr = false;
				int i = 0;
				//Add all declarations to symboltable
				while(true && !(_localctx.getParent().getParent() instanceof FunctionDeclContext)) {
					i++;
					Boolean success;
					if(_localctx.getChild(i).getText().equals("*")) {
						isPtr = true;
						//is pointer
						i++;
						success = this.symbolTable.putEntry(_localctx.getChild(i).getText(), $declType.text + "*", thisScope);
					}
					else {
						if(_localctx.getChild(i).getClass().toString().substring(26).equals("ArrayDeclContext")) {
							//is array
							//Check for multidimensional arrays
							int size = 1;
							int index = 2;
							List<Integer> dimensions = new ArrayList<Integer>();
							while (_localctx.getChild(i).getChild(index) != null) {
								size *= Integer.parseInt(_localctx.getChild(i).getChild(index).getText());
								dimensions.add(Integer.parseInt(_localctx.getChild(i).getChild(index).getText()));
								index += 3; // skip over the brackets
							}
							success = this.symbolTable.putEntry(_localctx.getChild(i).getChild(0).getText(), $declType.text, thisScope, 
								size, dimensions);
						}
						else {
							success = this.symbolTable.putEntry(_localctx.getChild(i).getText(), $declType.text, thisScope);
						}
					}
					if (!success) {
						throw new SemanticANTLRException(this, "This variable already exists");
					}
					i++;
					if (!(_localctx.getChild(i) != null && _localctx.getChild(i).getText().equals(","))) {
						break;
					}
				}
				//Will be set if this is a child of a FunctionArgsNode
				if (setFunctionArgs) {
					if (isPtr) {
						functionArgs.add(_localctx.getChild(0).getText() + "*");
					}
					else {
						functionArgs.add(_localctx.getChild(0).getText());
					}
				}
								
			}
		   ; 

type: CONST? (TYPECHAR | TYPEINT | typedefType);

declType: CONST? (TYPECHAR | TYPEINT | typedefType);


ptr: MULT;

functionType: CONST? (TYPECHAR | TYPEINT | TYPEVOID) ptr?;

argType: CONST? (TYPECHAR | TYPEINT | TYPEVOID) ptr?
		{if (setFunctionArgs) {functionArgs.add(_ctx.getText());}}
		;

typedefType: ID;

id: ID;

ampersand: AMPERSAND;

functionName: ID;

//Create new scope
compound:{this.scopes.push(++maxScope);} LBRACE stat* RBRACE {this.scopes.pop();}
	    ;

compoundNoScope: LBRACE stat* RBRACE;

caseSwitch: DEFAULTCASE stat*
		  | CASE atom COLON stat* breakStat?;

switchStat: SWITCH LPAREN id RPAREN {this.scopes.push(++maxScope);} LBRACE caseSwitch* RBRACE {this.scopes.pop();};

//having 'stat' as conditional body allows for both compound bodies (with {})
//having scopedBody as conditional body allows for both compound bodies (with {})

//and single-line bodies
//Conditionals set useMaxScope to true while parsing condition: this puts condition in same scope as the body
ifStat: {useMaxScope = true;}  IF LPAREN (arith|definition) RPAREN {useMaxScope = false;} scopedBody elseStat
	  | {useMaxScope = true;} IF LPAREN (arith|definition) RPAREN {useMaxScope = false;} scopedBody
	  ;
	  
elseStat : ELSE scopedBody
	     |;
	  

whileStat: {useMaxScope = true;}  WHILE LPAREN (arith|definition) RPAREN {useMaxScope = false;} scopedBody;
		   

forStat: {useMaxScope = true;} FOR LPAREN (definition | id|empty) SEMICOLON (arith|empty) SEMICOLON (arith | definition|empty) RPAREN {useMaxScope = false;} scopedBody;

breakStat: BREAK;

continueStat: CONTINUE;

functionDef: functionType functionName functionArguments compound
 			 {
 			 	if (scopes.size() != 1) {
 			 		//Not at global scope, no defs here
 			 		throw new SemanticANTLRException(this, "Functions can only be defined in the global scope");
 			 	}
 			 	ArrayList functionArgsCopy = (ArrayList) functionArgs.clone();
 			 	Boolean success = this.symbolTable.putEntryFunction($functionName.text, $functionType.text + " func" + $functionArguments.text,
 			 	$functionType.text, functionArgsCopy, false);
 			 	if (!success) {throw new SemanticANTLRException(this, "This function already exists");}
 			 	functionArgs = new ArrayList<String>();
 			  }
	       ;
	       
//set useMaxScope to true while parsing function args: this puts args in same scope as the body
//setFunctionArgs will signal the declaration that it's a function argument
functionArguments: {useMaxScope = true;
					setFunctionArgs = true;} 
				   LPAREN ((TYPEVOID|declaration|argType) (COMMA (TYPEVOID|declaration|argType))*)? RPAREN
				   {useMaxScope = false;
				   	setFunctionArgs = false;
				   }
				   ;

//functionDecl: functionType functionName LPAREN ((declaration|functionType) (COMMA (declaration|functionType))*)? RPAREN;
functionDecl: functionType functionName functionArguments
			{
				if (scopes.size() != 1) {
 			 		//Not at global scope, no defs here
 			 		throw new SemanticANTLRException(this, "Functions can only be declared in the global scope");
 			 	}
 			 	//Copy strictly necessary?
 			 	ArrayList functionArgsCopy = (ArrayList) functionArgs.clone();
 			 	Boolean success = this.symbolTable.putEntryFunction($functionName.text, $functionType.text + " func" + $functionArguments.text,
 			 	$functionType.text, functionArgsCopy, true);
 			 	if (!success) {throw new SemanticANTLRException(this, "This function is already defined");}
 			 	functionArgs = new ArrayList<String>();
			};



returnStat: RETURN (arith)?;

printfCall: 'printf' LPAREN (EMPTYSTRING|STRING) (COMMA (arith|string))* RPAREN {if(!stdioInclude) {throw new NoViableAltException(this);}};
scanfCall: 'scanf' LPAREN (EMPTYSTRING|STRING) (COMMA (ampersand)? id)* RPAREN  {if(!stdioInclude) {throw new NoViableAltException(this);}};

functionCall: functionName LPAREN (arith (COMMA arith)*)? RPAREN;

arrayDecl: id (LBRACKET arith RBRACKET)+;

arrayElem: id (LBRACKET arith RBRACKET)+;

typedef: TYPEDEF type id;

scopedBody: {this.scopes.push(++maxScope);} (compoundNoScope | stat) {this.scopes.pop();} ;

gotoStat: GOTO ID;

jumpLabel: LABEL;

string: STRING;

empty: ;

WHILE: 'while';
IF: 'if';
ELSE: 'else';
FOR: 'for';
BREAK: 'break';
CONTINUE: 'continue';
INCLUDE: '#include';
STDIO: '<stdio.h>';
TYPEINT: 'int';
TYPECHAR: 'char';
TYPEVOID: 'void';
CONST: 'const';
TRUE: 'true';
FALSE: 'false';
RETURN: 'return';
TYPEDEF: 'typedef';
GOTO: 'goto';
INCREMENT: '++';
DECREMENT: '--';
PLUS: '+';
MINUS: '-';	
MULT: '*';
DIV: '/';
MOD: '%';
LPAREN: '(';
RPAREN: ')';
LBRACE: '{';
RBRACE: '}';
LBRACKET: '[';
RBRACKET: ']';
EQ: '==';
NEQ: '!=';
GEQ: '>=';
LEQ: '<=';
GT: '>';
LT: '<';
AND: '&&';
OR: '||';
COMMA: ',';
QUOTE: '\'';
DOUBLEQUOTE: '"';
SEMICOLON: ';';
COLON: ':';
ASSIGN: '=';
AMPERSAND: '&';
NEGATE: '!';
SWITCH: 'switch';
CASE: 'case';
DEFAULTCASE: 'default:';

INT: '0'..'9'+;
ID: ('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;
LABEL: ID':';
CHAR: QUOTE . QUOTE;
EMPTYSTRING: DOUBLEQUOTE DOUBLEQUOTE;
STRING: DOUBLEQUOTE .*? ~('\\') DOUBLEQUOTE;


/* Checking for comments in lexer makes it possible to detect them before removing newlines,
 * which is necessary because they signify the end of a single line comment.
 * Non-greedy multiline comments makes sure they don't go past the first comment end
 */
SINGLECOMMENT: '//' ~('\r' | '\n')* -> channel(HIDDEN);
MULTICOMMENT: '/*' .*? '*/' -> channel(HIDDEN);

WS:(' '|'\t'|'\r'|'\n')+ ->channel(HIDDEN);
